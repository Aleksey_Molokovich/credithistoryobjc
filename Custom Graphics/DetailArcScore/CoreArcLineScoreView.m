//
//  CoreArcLineScoreView.m
//  ArcScore
//
//  Created by Алексей Молокович on 31.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "CoreArcLineScoreView.h"
#import "UIView+ImageContext.h"
#import "UIColor+CoreCustomColors.h"

static  double radians (double degrees) { return degrees * M_PI/180; }
void __path_applay2(void *info,const CGPathElement *element);


@interface CoreArcLineScoreView()
@property (nonatomic, assign) CGFloat radiusArcPoint;
@property (nonatomic, assign) CGFloat radiusArc;

@property (nonatomic, assign) NSInteger spliteLineWidth;
@property (nonatomic, assign) NSInteger startAngle;
@property (nonatomic) CAShapeLayer *maskArc;
@property (nonatomic) CAShapeLayer *maskPoint;
@property (nonatomic) CAShapeLayer *pointLayer;

@property (nonatomic) UIView *pointView;
@property (nonatomic) UIView *arcView;

@property (nonatomic, assign) NSInteger score;
@property (nonatomic, strong) NSString *scoreDescription;
@property (nonatomic,assign) BOOL isUp;

@property (nonatomic,assign) CGPoint pointArcCenter;
@property (nonatomic,assign) CGPoint arcCenter;

@property (nonatomic) UILabel *labelScore;
@property (nonatomic) UILabel *labelDifScore;
@property (nonatomic) UILabel *labelMarkScore;
@property (nonatomic) UIColor *labelMarkScoreColor;

@property (nonatomic, assign) BOOL isAnimated;
@end


@implementation CoreArcLineScoreView
void __path_applay2(void *info,const CGPathElement *element)
{
    
    NSMutableArray *bezierPoints = (__bridge NSMutableArray*) info;
    
    CGPoint *points = element->points;
    CGPathElementType type = element->type;
    
    switch(type) {
        case kCGPathElementMoveToPoint: // contains 1 point
            //            [bezierPoints addObject:[NSValue valueWithCGPoint:points[0]]];
            break;
            
        case kCGPathElementAddLineToPoint: // contains 1 point
            [bezierPoints addObject:[NSValue valueWithCGPoint:points[0]]];
            break;
            
        case kCGPathElementAddQuadCurveToPoint: // contains 2 points
            [bezierPoints addObject:[NSValue valueWithCGPoint:points[0]]];
            [bezierPoints addObject:[NSValue valueWithCGPoint:points[1]]];
            break;
            
        case kCGPathElementAddCurveToPoint: // contains 3 points
            //            [bezierPoints addObject:[NSValue valueWithCGPoint:points[0]]];
            //            [bezierPoints addObject:[NSValue valueWithCGPoint:points[1]]];
            [bezierPoints addObject:[NSValue valueWithCGPoint:points[2]]];
            break;
            
        case kCGPathElementCloseSubpath: // contains no point
            break;
    }
    
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ( (self = [super initWithCoder:aDecoder]) == nil )
    {
        return nil;
    }
    
    
    [self commonInit];
    
    return self;
}

-(void)drawRect:(CGRect)rect
{
    if (_score>0 && !_isAnimated)
    {
        _isAnimated = YES;
        [self animateScore];
    }
}


- (void)commonInit
{
    
    if (_isAnimated)
    {
        return;
    }
    
    
    
    _startAngle = 117;
    _spliteLineWidth = 6;
    _radiusArc = self.bounds.size.height < self.bounds.size.width
                                        ? self.bounds.size.height/2
                                        : self.bounds.size.width/2;
    _radiusArc -=_spliteLineWidth;
    _arcCenter = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);

    NSArray *arcs = @[@156,@23,@23,@23,@57];
    CGFloat splitArcLenght = 6.5;
    CGFloat startNextAngle = 0;
    
    //---------Gray segments
    
    UIView *arcGrayView = [[UIView alloc] initWithFrame:self.bounds];
    CAShapeLayer *maskGray = [CAShapeLayer new];
    for (NSNumber *value in arcs)
    {
        if (startNextAngle == 0)
        {
            [maskGray addSublayer:[self getArcStart:startNextAngle value:[value floatValue] center:_arcCenter radius:_radiusArc]];
        } else {
            [maskGray addSublayer:[self getArcStart:startNextAngle value:[value floatValue] center:_arcCenter radius:_radiusArc]];
        }
        startNextAngle +=([value floatValue]+splitArcLenght);
    }
    
    arcGrayView.layer.mask = maskGray;
    arcGrayView.backgroundColor = [UIColor colorWithHex:@"#E2E7E8" alpha:1];
    [self addSubview:arcGrayView];
    //----------
    startNextAngle = 0;
    //-------Color segments
    _arcView = [[UIView alloc] initWithFrame:self.bounds];
    for (NSNumber *value in arcs) {
        if (startNextAngle == 0) {
            [_arcView.layer addSublayer:[self getArcStart:startNextAngle value:[value floatValue] center:_arcCenter radius:_radiusArc]];
        } else {
            [_arcView.layer addSublayer:[self gradientLayerWithMask:[self getArcStart:startNextAngle value:[value floatValue] center:_arcCenter radius:_radiusArc]]];
        }
        startNextAngle +=([value floatValue]+splitArcLenght);
    }
    
    
    _maskArc = [self getArcStart:0 value:310 center:_arcCenter radius:_radiusArc];
    _maskArc.strokeStart = 0.0;
    _maskArc.strokeEnd = 0.0;
    _arcView.layer.mask = _maskArc;
    
    [self addSubview:_arcView];
    
    
    NSInteger offset = 12;
    _spliteLineWidth = 50;
    _pointView = [[UIView alloc] initWithFrame:self.bounds];
    
    _pointArcCenter =_pointView.center;
    
    _radiusArcPoint= _radiusArc - offset;
    [self addSubview:_pointView];
    
    
    [_pointView.layer addSublayer:[self getArcStart:-20 value:180 center:_pointArcCenter radius:_radiusArcPoint]];
    [_pointView.layer addSublayer:[self gradientLayerWithMask:[self getArcStart:160 value:160 center:_pointArcCenter radius:_radiusArcPoint]]];
    
    _pointLayer = [CAShapeLayer layer];
    _pointLayer.fillColor = [[UIColor blackColor] CGColor];
    UIBezierPath *circle = [UIBezierPath bezierPathWithArcCenter:CGPointZero radius:4 startAngle:0 endAngle:(2 * M_PI) clockwise:YES];
    _pointLayer.path = circle.CGPath;
    _pointView.layer.mask = _pointLayer;
    
    UIView *whiteView = [[UIView alloc] initWithFrame:self.bounds];
    CAShapeLayer *maskLayer = [CAShapeLayer new];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithArcCenter:_arcCenter
                                                            radius:90
                                                        startAngle:radians(0)
                                                          endAngle:radians(360)
                                                         clockwise:NO];
    maskLayer.path = [maskPath CGPath];
    maskLayer.fillColor = [[UIColor whiteColor] CGColor];
    
    [whiteView.layer addSublayer:maskLayer];
    whiteView.layer.shadowColor = [UIColor blackColor].CGColor;
    whiteView.layer.shadowOffset = CGSizeMake(0, 0);
    whiteView.layer.shadowOpacity = 0.3;
    whiteView.layer.shadowRadius = 10.0;
    
    [self addSubview:whiteView];
    
    
    UILabel *zero = [UILabel new];
    zero.translatesAutoresizingMaskIntoConstraints = NO;
    zero.text = [@(0) stringValue];
    zero.textAlignment = NSTextAlignmentRight;
    zero.textColor = [UIColor colorWithHex:@"#96A6A7" alpha:1.0];
    zero.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    [zero sizeToFit];
    [self addSubview:zero];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:zero attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:zero attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:-70]];
    
    UILabel *maxLabel = [UILabel new];
    maxLabel.translatesAutoresizingMaskIntoConstraints = NO;
    maxLabel.text = [@(1000) stringValue];
    maxLabel.textAlignment = NSTextAlignmentLeft;
    maxLabel.textColor = [UIColor colorWithHex:@"#96A6A7" alpha:1.0];
    maxLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    [maxLabel sizeToFit];
    [self addSubview:maxLabel];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:maxLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:maxLabel attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:80]];
    
}

-(void)showMarkScore
{
    if (!_labelMarkScoreColor)
        return;
    
    if (_labelMarkScore)
        [_labelMarkScore removeFromSuperview];
    
     NSArray* desriptionScore = @[@"oчень плохо",@"плохо", @"хорошо",@"очень хорошо",@"отлично"];
    
    _labelMarkScore = [UILabel new];
    _labelMarkScore.alpha = 0;
    _labelMarkScore.backgroundColor = _labelMarkScoreColor;
    
    NSInteger index=0;
    
    if (_score>500 && _score<=600) {
        index=1;
    }else if (_score>600 && _score<=700) {
        index=2;
    }else if (_score>700 && _score<=800) {
        index=3;
    }else if (_score>800) {
        index=4;
    }
    
    if (!_scoreDescription)
    {
        _scoreDescription = desriptionScore[index];
    }
    
    _labelMarkScore.text = _scoreDescription;
    _labelMarkScore.font = [UIFont systemFontOfSize:14];
    _labelMarkScore.translatesAutoresizingMaskIntoConstraints = NO;
    _labelMarkScore.textAlignment = NSTextAlignmentCenter;
    _labelMarkScore.textColor = [UIColor whiteColor];
    
    [self addSubview:_labelMarkScore];
    [_labelMarkScore sizeToFit];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_labelMarkScore attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_labelMarkScore attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:45]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_labelMarkScore attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:_labelMarkScore.frame.size.width+30]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_labelMarkScore attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:28]];
    
    
    _labelMarkScore.layer.cornerRadius = 14;
    _labelMarkScore.clipsToBounds = YES;
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3
                     animations:^{
                         __strong typeof(weakSelf) blockSelf = weakSelf;
                         blockSelf.labelMarkScore.alpha = 1;
                     }];
    if (_labelScore)
        [_labelScore removeFromSuperview];
    
    _labelScore = [UILabel new];
    _labelScore.translatesAutoresizingMaskIntoConstraints = NO;
    _labelScore.text = [@(_score) stringValue];
    _labelScore.textAlignment = NSTextAlignmentCenter;
    _labelScore.textColor = [UIColor colorWithHex:@"#2C3E50" alpha:1.0];
    [_labelScore sizeToFit];
    
    [self addSubview:_labelScore];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_labelScore attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_labelScore attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:-10]];
    _labelScore.font = [UIFont systemFontOfSize:72 weight:UIFontWeightThin];
    
    
    
    
    

}



-(CAShapeLayer *)getArcStart:(CGFloat)start value:(CGFloat)value center:(CGPoint)center radius:(CGFloat)radius
{
    CAShapeLayer *redLayer = [CAShapeLayer new];
    UIBezierPath *redArcPath = [UIBezierPath bezierPathWithArcCenter:center
                                                              radius:radius
                                                          startAngle:radians(_startAngle+start)
                                                            endAngle:radians(_startAngle+start+value)
                                                           clockwise:YES];
    redLayer.path = [redArcPath CGPath];
    redLayer.fillColor = [[UIColor clearColor] CGColor];
    redLayer.strokeColor = [[UIColor redColor] CGColor];
    redLayer.lineWidth = _spliteLineWidth;
    redLayer.strokeStart =0;
    redLayer.lineCap = kCALineCapRound;
    
    return redLayer;
}

- (CAGradientLayer*) gradientLayerWithMask:(CALayer*)mask
{
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    
    gradientLayer.frame = self.bounds;
    gradientLayer.colors = @[(id)[[UIColor colorWithHex:@"#EE3C47" alpha:1] CGColor],
                             (id)[[UIColor colorWithHex:@"#F68400" alpha:1] CGColor],
                             (id)[[UIColor colorWithHex:@"#F4DC00" alpha:1] CGColor],
                             (id)[[UIColor colorWithHex:@"#30BF52" alpha:1] CGColor]];
    gradientLayer.startPoint = CGPointMake(0.5,0);
    gradientLayer.endPoint = CGPointMake(1,0.5);
    gradientLayer.mask = mask;
    
    return gradientLayer;
}



-(void)updateScore:(NSInteger)score withDescription:(NSString *)description
{
    _score = score;
    _scoreDescription = description;
}

-(void)updateDiff:(NSInteger)diff
{
    if (diff == 0) {
        return;
    }
    
    if (diff<0)
    {
        _isUp = NO;
    }
    else
    {
        _isUp = YES;
    }
    
    if (_labelDifScore)
        [_labelDifScore removeFromSuperview];
    
    _labelDifScore = [UILabel new];
    _labelDifScore.translatesAutoresizingMaskIntoConstraints = NO;
    _labelDifScore.text = [@(labs(diff)) stringValue];
    _labelDifScore.textAlignment = NSTextAlignmentLeft;
    _labelDifScore.textColor = [UIColor colorWithHex:@"#2C3E50" alpha:1.0];
    _labelDifScore.font = [UIFont systemFontOfSize:14 weight:UIFontWeightThin];
    [_labelDifScore sizeToFit];
    
    [self addSubview:_labelDifScore];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_labelDifScore attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:5]];
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:_labelDifScore attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:-55]];
    
    CAShapeLayer *arrow = [CAShapeLayer new];
    [self.layer addSublayer:arrow];
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathMoveToPoint(path, NULL, 108, 60);
    CGPathAddLineToPoint(path, NULL,108, 70);
    
    if (_isUp) {
        CGPathMoveToPoint(path, NULL, 108, 60);
        CGPathAddLineToPoint(path, NULL,112, 64);
        CGPathMoveToPoint(path, NULL, 108, 60);
        CGPathAddLineToPoint(path, NULL,104, 64);
        arrow.strokeColor = [[UIColor colorWithHex:@"#30BF52" alpha:1] CGColor];
    }else{
        CGPathMoveToPoint(path, NULL, 108, 70);
        CGPathAddLineToPoint(path, NULL,112, 66);
        CGPathMoveToPoint(path, NULL, 108, 70);
        CGPathAddLineToPoint(path, NULL,104, 66);
        arrow.strokeColor = [[UIColor redColor] CGColor];
    }
    
    CGPathCloseSubpath(path);
    arrow.path = path;
    arrow.lineWidth =1;
    
    CGPathRelease(path);
}

- (void)animateScore
{
    _maskPoint =[self getArcStart:0 value:310.0*_score/1000.0 center:_pointArcCenter radius:_radiusArcPoint];
    _maskArc = [self getArcStart:0 value:310.0*_score/1000.0 center:_arcCenter radius:_radiusArc];
    
    NSMutableArray *pathPoints = [[NSMutableArray alloc] init];
    
    CGPathApply(_maskArc.path, (__bridge void*)pathPoints, &__path_applay2);
    
    CGPoint lastPoint = [[pathPoints lastObject] CGPointValue];
    lastPoint = CGPointMake(lastPoint.x, lastPoint.y);
    
    _labelMarkScoreColor = [self.pointView getPixelColorAtLocation:lastPoint];
    [self showMarkScore];
    
    CAMediaTimingFunction *func = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionDefault];
    
    CABasicAnimation *arcAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    arcAnimation.fillMode = kCAFillModeForwards;
    arcAnimation.removedOnCompletion = NO;;
    arcAnimation.fromValue = @0;
    arcAnimation.toValue = @(_score/1000.0);
    arcAnimation.duration = 2.0;
    arcAnimation.repeatCount = 1;
    arcAnimation.timingFunction = func;
    
    CAKeyframeAnimation *pointnimation=[CAKeyframeAnimation animationWithKeyPath:@"position"];
    pointnimation.duration=2.0 ;
    pointnimation.calculationMode = kCAAnimationPaced;
    pointnimation.path=_maskPoint.path;
    pointnimation.fillMode = kCAFillModeForwards;
    pointnimation.removedOnCompletion = NO;
    pointnimation.repeatCount = 1;
    pointnimation.timingFunction = func;
    
    
    [_arcView.layer.mask addAnimation:arcAnimation forKey:@"strokeEnd"];
    
    [_pointView.layer.mask addAnimation:pointnimation forKey:@"position"];
}


@end


