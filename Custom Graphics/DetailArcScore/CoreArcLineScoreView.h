//
//  CoreArcLineScoreView.h
//  ArcScore
//
//  Created by Алексей Молокович on 31.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CoreArcLineScoreView : UIView

-(void)updateDiff:(NSInteger)diff;

-(void)updateScore:(NSInteger)score withDescription:(NSString *)description;


@end
