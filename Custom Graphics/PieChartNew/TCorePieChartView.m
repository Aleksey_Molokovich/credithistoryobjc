//
//  TCorePieChartView.m
//  Chart
//
//  Created by Алексей Молокович on 25.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import "TCorePieChartView.h"
#import "NSLayoutConstraint+CoreUtils.h"
#import "UIColor+CoreCustomColors.h"

static  double radians (double degrees) { return degrees * M_PI/180; }

@interface TCorePieChartView()<UIGestureRecognizerDelegate>

@property (nonatomic, assign) CGPoint centerOfBounds;
@property (nonatomic) NSArray<CALayer*> *layers;
@property (nonatomic, assign) CGFloat radius;
@property (nonatomic, assign) NSInteger spliteLineWidth;
@end

@implementation TCorePieChartView


- (void)drawRect:(CGRect)rect
{
    _spliteLineWidth = 3;
    _centerOfBounds = CGPointMake(rect.size.width/2, rect.size.height/2);
    _radius = rect.size.height<rect.size.width?rect.size.height/2:rect.size.width/2;
    _radius +=_spliteLineWidth;
    
    CGFloat startPosition = 0;
    NSInteger index = 0;

    self.layer.cornerRadius = rect.size.height/2;
    self.backgroundColor = [UIColor colorWithHex:@"#E2E7E8" alpha:1];
    self.clipsToBounds = YES;
    

    for (NSNumber *val in _pieces) {
        [self addPiece:[val floatValue] startPosition:startPosition index:index];
        startPosition += [val floatValue];
        index++;
    }
    
    
    UIView *whiteView = [[UIView alloc] initWithFrame:rect];
    CAShapeLayer *maskLayer = [CAShapeLayer new];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithArcCenter:_centerOfBounds
                                                            radius:_radius*0.55
                                                        startAngle:radians(0)
                                                          endAngle:radians(360)
                                                         clockwise:NO];
    maskLayer.path = [maskPath CGPath];
    maskLayer.fillColor = [[UIColor whiteColor] CGColor];
    
    [whiteView.layer addSublayer:maskLayer];
    whiteView.layer.shadowColor = [UIColor blackColor].CGColor;
    whiteView.layer.shadowOffset = CGSizeMake(0, 0);
    whiteView.layer.shadowOpacity = 0.5;
    whiteView.layer.shadowRadius = 10.0;
    
    [self addSubview:whiteView];
    
    UILabel *title = [[UILabel alloc] initWithFrame:whiteView.bounds ];
    title.numberOfLines = 2;
    title.text = _title;
    title.textAlignment = NSTextAlignmentCenter;
    [title sizeToFit];
    title.backgroundColor = [UIColor clearColor];
    title.textColor = [UIColor colorWithHex:@"#96A6A7" alpha:1];
    title.font = [UIFont fontWithName:@"SFProText-Regular" size:14];
    [whiteView addSubview: title];
    
    
    title.translatesAutoresizingMaskIntoConstraints = NO;
    [whiteView addConstraint:[NSLayoutConstraint constraintWithItem:whiteView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:title attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:(title.frame.size.height*0.6)]];
    
    [whiteView addConstraint:[NSLayoutConstraint constraintWithItem:whiteView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:title attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    [title addConstraint:[NSLayoutConstraint constraintWithItem:title attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:whiteView.bounds.size.width/3]];
    
    
    UILabel *titlePercent = [[UILabel alloc] initWithFrame:whiteView.bounds ];
    titlePercent.numberOfLines = 1;
    titlePercent.textAlignment = NSTextAlignmentCenter;
    titlePercent.text = [NSString stringWithFormat:@"%@%%",_value];
    [titlePercent sizeToFit];
    titlePercent.backgroundColor = [UIColor clearColor];
    titlePercent.textColor = [UIColor colorWithHex:@"#2C3E50" alpha:1];
    titlePercent.font = [UIFont fontWithName:@"SFProText-Regular" size:18];
    [whiteView addSubview: titlePercent];
    
    
    titlePercent.translatesAutoresizingMaskIntoConstraints = NO;
    [whiteView addConstraint:[NSLayoutConstraint constraintWithItem:whiteView attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:titlePercent attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:-(titlePercent.frame.size.height)]];
    
    [whiteView addConstraint:[NSLayoutConstraint constraintWithItem:whiteView attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:titlePercent attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0]];
    
    [titlePercent addConstraint:[NSLayoutConstraint constraintWithItem:titlePercent attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:whiteView.bounds.size.width/3]];
    
    
    
}

- (CAShapeLayer*)addPiece:(CGFloat)value startPosition:(CGFloat)startPosition index:(NSInteger)index
{
    
    UIColor *color = _colorsPieces[index];
    
    CAShapeLayer *maskLayer = [CAShapeLayer new];
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithArcCenter:_centerOfBounds
                                                            radius:_radius
                                                    startAngle:radians(360-startPosition)
                                                      endAngle:radians(360-(startPosition+value))
                                                     clockwise:NO];
    
    maskLayer.strokeColor = [color CGColor];
    maskLayer.path = [maskPath CGPath];
    maskLayer.lineWidth = _radius*2;
    
    CGMutablePathRef path = CGPathCreateMutableCopyByTransformingPath(maskLayer.path, nil);
    CGPathAddLineToPoint(path, NULL, _centerOfBounds.x, _centerOfBounds.y);
    CGPathCloseSubpath(path);

    
    CAShapeLayer *borderLayer = [CAShapeLayer new];
    borderLayer.path = path;
    borderLayer.lineWidth = _spliteLineWidth;
    borderLayer.strokeColor = [[UIColor colorWithHex:@"#E2E7E8" alpha:1] CGColor];
    borderLayer.fillColor = [color CGColor];
    borderLayer.fillRule = kCAFillRuleEvenOdd;
    maskLayer.mask = borderLayer;
    
    UIView *view = [[UIView alloc] initWithFrame:self.bounds];
    [self addSubview:view];
    view.clipsToBounds = YES;
    [view.layer addSublayer:borderLayer];

    CGPathRelease(path);
    
    return maskLayer;
}

@end
