//
//  TCorePieChartView.h
//  Chart
//
//  Created by Алексей Молокович on 25.01.18.
//  Copyright © 2018 Aleksey Molokovich. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TCorePieChartView : UIView
@property (nonatomic) NSArray *pieces;
@property (nonatomic) NSArray<UIColor*> *colorsPieces;
@property (nonatomic) NSString *title;
@property (nonatomic) NSString *value;
@end
