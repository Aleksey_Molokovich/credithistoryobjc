//
//  RainbowChartView.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 30.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "RainbowChartView.h"
#import "RainbowView.h"
#import "ArrowView.h"

@interface RainbowChartView()
@property (weak, nonatomic) IBOutlet UIImageView *arrow;
@property (weak, nonatomic) IBOutlet UIImageView *rainbow;
@property (weak, nonatomic) IBOutlet RainbowView *rainbowView;
@property (strong, nonatomic) ArrowView *arrowView;
@property (assign, nonatomic) BOOL isDidChangeAncherPointDraw;
@property (assign, nonatomic) CGFloat score;
@end

@implementation RainbowChartView
@synthesize enableAnimation;

- (id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self commonInit];
    }
    return self;
}


- (void)commonInit
{
    _score = 0;
    _isDidChangeAncherPointDraw = NO;
    [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil];
    [self addSubview:self.view];
    self.view.translatesAutoresizingMaskIntoConstraints = NO;
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
    [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|" options:0 metrics:nil views:@{@"view":self.view}]];
}

-(void)drawRect:(CGRect)rect
{
#pragma unused (rect)
    ArrowView *arrowView = [[ArrowView alloc] initWithFrame:CGRectMake(0, 0, 1/12.f * CGRectGetWidth(self.rainbowView.bounds), 0.9f * CGRectGetHeight(self.rainbowView.bounds))];
    [self addSubview:arrowView];
    arrowView.translatesAutoresizingMaskIntoConstraints = NO;
    arrowView.layer.position = CGPointMake(CGRectGetMidX(self.rainbowView.frame), CGRectGetMaxY(self.rainbowView.frame));
    self.arrowView = arrowView;
    
    _isDidChangeAncherPointDraw = YES;
    [self setAnchorPoint:CGPointMake(0.5, 0.9) forView:_arrow];
    CGFloat radians = (M_PI/180) * (-89);
    self.arrowView.transform = CGAffineTransformRotate(self.arrowView.transform, radians);
    self.arrow.hidden = YES;
    
    NSArray *texts = @[@"oчень\nплохо",@"плохо", @"хорошо",@"очень\nхорошо",@"отлично"];
    UIFont *font = [UIFont fontWithName:@"HelveticaNeue-Medium" size:12];

    int i = 0;
    for (NSString *text in texts)
    {
        UILabel *veryBadLabel = [UILabel new];
        CGRect mFrame = _rainbow.frame;
        mFrame.origin.y += _rainbow.frame.size.height*0.025;
        UIView *view = [[UIView alloc] initWithFrame:mFrame];
        NSAttributedString *attributedstring = [[NSAttributedString alloc] initWithString:text attributes:@{ NSFontAttributeName : font }];
        veryBadLabel.attributedText = attributedstring ;
        veryBadLabel.numberOfLines = 2;
        veryBadLabel.textAlignment = NSTextAlignmentCenter;
        CGRect boundingRect = [attributedstring boundingRectWithSize:self.frame.size
                                                             options:(NSStringDrawingUsesLineFragmentOrigin|NSStringDrawingUsesFontLeading)
                                                             context:nil];
        boundingRect.origin.x = view.frame.size.width/2-boundingRect.size.width/2;
        boundingRect.origin.y = view.frame.size.height*0.15 - boundingRect.size.height;
        
        veryBadLabel.frame =boundingRect;
        veryBadLabel.textColor = [UIColor whiteColor];
        [view addSubview:veryBadLabel];
        [self addSubview:view];
        [self setAnchorPoint:CGPointMake(0.5, 1) forView:view];
        
        [view setTransform:CGAffineTransformMakeRotation((M_PI/180) * (-90+18+36*i))];
        i++;
    }
    
    [self reloadData];
}

#pragma mark -  chart protocol

- (void) reloadData
{
    
    if ( !_isDidChangeAncherPointDraw)
    {
        return;
    }
    
    if (_score == [_delegate valueForRainbowChartView])
    {
        return;
    }
    else
    {
        CGFloat radians = (M_PI/180) * 180*_score/1000;
        self.arrowView.transform = CGAffineTransformRotate(self.arrowView.transform, radians);
    }
    
    _score = [_delegate valueForRainbowChartView];

    CGFloat radians = (M_PI/180) * 180*_score/1000;

    __weak typeof(self) weakSelf = self;


    [UIView animateWithDuration:1.0 animations:^{
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if (!blockSelf)
        {
            return ;
        }
        blockSelf.arrowView.transform = CGAffineTransformRotate(blockSelf.arrowView.transform, radians);
    }];
}

-(void)setAnchorPoint:(CGPoint)anchorPoint forView:(UIView *)view
{
    CGPoint newPoint = CGPointMake(view.bounds.size.width * anchorPoint.x,
                                   view.bounds.size.height * anchorPoint.y);
    CGPoint oldPoint = CGPointMake(view.bounds.size.width * view.layer.anchorPoint.x,
                                   view.bounds.size.height * view.layer.anchorPoint.y);
    
    newPoint = CGPointApplyAffineTransform(newPoint, view.transform);
    oldPoint = CGPointApplyAffineTransform(oldPoint, view.transform);
    
    CGPoint position = view.layer.position;
    
    position.x -= oldPoint.x;
    position.x += newPoint.x;
    
    position.y -= oldPoint.y;
    position.y += newPoint.y;
    
    view.layer.position = position;
    view.layer.anchorPoint = anchorPoint;
}

- (void) show
{
    [self setNeedsDisplay];
}

- (void) hide
{
    [self setNeedsDisplay];
}


@end
