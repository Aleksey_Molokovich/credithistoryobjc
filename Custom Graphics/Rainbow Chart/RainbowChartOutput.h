//
//  RainbowChartOutput.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 30.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#ifndef RainbowChartOutput_h
#define RainbowChartOutput_h

@protocol RainbowChartOutput<NSObject>

@required

- (CGFloat) valueForRainbowChartView;

@end
#endif /* RainbowChartOutput_h */
