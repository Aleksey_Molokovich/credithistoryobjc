//
//  ArrowView.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 12.12.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "ArrowView.h"

@implementation ArrowView

- (instancetype)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame]) != nil) {
        self.opaque = NO;
    }
    
    return self;
}

- (void)drawRect:(CGRect)rect {

    CGFloat height = CGRectGetHeight(rect);
    CGFloat radius = CGRectGetWidth(rect)/2.f;
    CGFloat innerRadius = radius * 0.4f;
    CGRect innerOvalRect = CGRectMake(radius - innerRadius, CGRectGetHeight(rect) - radius - innerRadius, innerRadius*2, innerRadius*2);
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(radius, 0)];
    [path addLineToPoint:CGPointMake(0, height - radius)];
    [path addArcWithCenter:CGPointMake(radius, height - radius)
                    radius:radius
                startAngle:M_PI endAngle:0 clockwise:NO];
    [path addLineToPoint:CGPointMake(radius, 0)];
    [path appendPath:[[UIBezierPath bezierPathWithOvalInRect:innerOvalRect] bezierPathByReversingPath]];
    [path setUsesEvenOddFillRule:YES];
    [path closePath];
    
    [[UIColor whiteColor] setFill];
    [path fill];
    
    self.layer.anchorPoint = CGPointMake(0.5, 1 - radius/height);
}

@end
