//
//  RainbowView.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 12.12.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "RainbowView.h"

#import "UIColor+CoreCustomColors.h"
#import "TCoreColorList.h"

@implementation RainbowView
{
    CGPoint centerBottom;
    CGFloat outerRadius;
    CGFloat innerRadius;
}

- (void)drawRect:(CGRect)rect
{
    NSArray<UIColor *> *sectorColors = @[[UIColor colorWithHex:@"#CE2021" alpha:1.0],
                                         [UIColor colorWithHex:@"#F76521" alpha:1.0],
                                         [UIColor colorWithHex:@"#FFBA10" alpha:1.0],
                                         [UIColor colorWithHex:@"#CEDF29" alpha:1.0],
                                         [UIColor colorWithHex:@"#42B64A" alpha:1.0]];
    
    CGFloat width = CGRectGetWidth(rect);
    centerBottom = CGPointMake(CGRectGetMidX(rect), CGRectGetMaxY(rect));
    
    outerRadius = width/2;
    innerRadius = 50.f/240.f * width/2;
    
    NSUInteger sectorCount = sectorColors.count;
    
    CGFloat spacingAngle = 0.f;//2.f/180.f * M_PI;
    CGFloat angleStep = (M_PI - spacingAngle*(sectorCount-1)) / sectorCount;
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextBeginTransparencyLayer(context, nil);
    for (int i = 0; i < sectorCount; i++)
    {
        CGFloat startAngle = M_PI + spacingAngle*i + angleStep*i;
        CGFloat endAngle = startAngle + angleStep;
        [sectorColors[i] setFill];
        
        UIBezierPath *sectorPath = [UIBezierPath bezierPathWithArcCenter:centerBottom radius:outerRadius startAngle:startAngle endAngle:endAngle clockwise:YES];
        [sectorPath addArcWithCenter:centerBottom radius:innerRadius startAngle:endAngle endAngle:startAngle clockwise:NO];
        [sectorPath closePath];
        
        CGContextSetBlendMode(context, kCGBlendModeNormal);
        [sectorPath fill];
        
        CGContextSetBlendMode(context, kCGBlendModeClear);
        if (i > 0)
        {
            [[self spacingForAngle:startAngle] fill];
        }
        
        if (i < sectorCount-1)
        {
            [[self spacingForAngle:endAngle] fill];
        }
    }
    CGContextEndTransparencyLayer(context);
}

- (UIBezierPath *)spacingForAngle:(CGFloat)angle
{
    CGFloat spacingWidthHalf = 1.f;
    CGFloat spacingHeight = outerRadius + 1.f;
    
    CGPoint topCenterPoint = CGPointMake(centerBottom.x + spacingHeight * cosf(angle),
                                         centerBottom.y + spacingHeight * sinf(angle));
    CGPoint topLeftPoint    = CGPointMake(topCenterPoint.x + spacingWidthHalf * cosf(angle - M_PI_2), topCenterPoint.y + spacingWidthHalf * sinf(angle - M_PI_2));
    CGPoint topRightPoint   = CGPointMake(topCenterPoint.x + spacingWidthHalf * cosf(angle + M_PI_2), topCenterPoint.y + spacingWidthHalf * sinf(angle + M_PI_2));
    CGPoint bottomLeftPoint = CGPointMake(centerBottom.x + spacingWidthHalf * cosf(angle - M_PI_2), centerBottom.y + spacingWidthHalf * sinf(angle - M_PI_2));
    CGPoint bottomRightPoint = CGPointMake(centerBottom.x + spacingWidthHalf * cosf(angle + M_PI_2), centerBottom.y + spacingWidthHalf * sinf(angle + M_PI_2));
    
    UIBezierPath *spacingPath = [UIBezierPath bezierPath];
    [spacingPath moveToPoint:topLeftPoint];
    [spacingPath addLineToPoint:topRightPoint];
    [spacingPath addLineToPoint:bottomRightPoint];
    [spacingPath addLineToPoint:bottomLeftPoint];
    [spacingPath closePath];
    
    return spacingPath;
}

@end
