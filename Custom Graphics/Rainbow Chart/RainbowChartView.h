//
//  RainbowChartView.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 30.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SoulChart.h"
#import "RainbowChartOutput.h"

@interface RainbowChartView : UIView<SoulChart>
@property (strong, nonatomic) IBOutlet UIView *view;

@property (nonatomic, weak) id<RainbowChartOutput> delegate;
@end
