//
//  ArcScore.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 23.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "ArcScore.h"
#import "ArcGradient.h"
#import "UIColor+CoreCustomColors.h"
#import "NSLayoutConstraint+CoreUtils.h"

void __path_applay(void *info,const CGPathElement *element);

static inline double radians (double degrees) { return degrees * M_PI/180; }
@interface ArcScore()<CAAnimationDelegate>

@property (nonatomic, strong) CAShapeLayer *arcColorLayer;
@property (nonatomic, strong) CAShapeLayer *pointColorLayer;


@property (nonatomic, assign) NSInteger score;

@property (nonatomic, strong) NSString *scoreDescription;

@property (nonatomic, assign) BOOL isAnimating;

@property (nonatomic, assign) NSInteger widthArc;

@property (nonatomic) UILabel *labelScore;
@property (nonatomic) UILabel *labelMarkScore;
@property (nonatomic) UIColor *labelMarkScoreColor;
@property (nonatomic) NSArray *desriptionScore;
@property (nonatomic) CALayer *gradientLayerForPoint;
@end


@implementation ArcScore


-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ( (self = [super initWithCoder:aDecoder]) == nil )
    {
        return nil;
    }
    
    
    return self;
}



- (void) drawRect:(CGRect)rect
{

    if (_gradientLayerForPoint) {
        [_gradientLayerForPoint removeFromSuperlayer];
    }
    
    _isAnimating = NO;
    
    if (_isFatChart) {
        _widthArc = 80;
    }
    else{
        _widthArc = 40;
        
        if (_labelScore)
            [_labelScore removeFromSuperview];
        
        _labelScore = [UILabel new];
        _labelScore.translatesAutoresizingMaskIntoConstraints = NO;
        _labelScore.text = @"0";
        _labelScore.textAlignment = NSTextAlignmentCenter;
        _labelScore.textColor = [UIColor colorWithHex:@"#2C3E50" alpha:1.0];
        [_labelScore sizeToFit];
        
        [self addSubview:_labelScore];
        
        [self addConstraint:[NSLayoutConstraint centerX:self to:_labelScore constant:0]];
        [self addConstraint:[NSLayoutConstraint bottomFrom:_labelScore to:self constant:0.0f]];
        _labelScore.font = [UIFont systemFontOfSize:64];
    }
    
    _desriptionScore = @[@"oчень плохо",@"плохо", @"хорошо",@"очень хорошо",@"отлично"];
    

    [self showMarkScore];
    
    _arcColorLayer = [[CAShapeLayer alloc] init];
    _arcColorLayer.path = [[self createArcPathFromBottomOfRect:self.bounds points:0] CGPath];
    _arcColorLayer.fillColor = [[UIColor clearColor] CGColor];
    _arcColorLayer.lineWidth = _widthArc;
    _arcColorLayer.strokeColor = [[UIColor grayColor] CGColor];
    
    CAShapeLayer *grayLayer = [[CAShapeLayer alloc] init];
    grayLayer.path = [[self createArcPathFromBottomOfRect:self.bounds points:1000] CGPath];
    grayLayer.fillColor = [[UIColor clearColor] CGColor];
    grayLayer.lineWidth = _widthArc;
    grayLayer.strokeColor = [[UIColor colorWithHex:@"#F6F8F9" alpha:1.0] CGColor];
    
    [self.layer addSublayer:grayLayer];
    
    
    CALayer *gradientLayer = [self gradientLayer];
    gradientLayer.mask = _arcColorLayer;
    [self.layer addSublayer:gradientLayer];
    
    UILabel *zero = [UILabel new];
    zero.translatesAutoresizingMaskIntoConstraints = NO;
    zero.text = [@(0) stringValue];
    zero.textAlignment = NSTextAlignmentRight;
    zero.textColor = [UIColor colorWithHex:@"#96A6A7" alpha:1.0];
    zero.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    [zero sizeToFit];
    [self addSubview:zero];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:zero attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:20]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:zero attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1.0 constant:15]];
    
    UILabel *maxLabel = [UILabel new];
    maxLabel.translatesAutoresizingMaskIntoConstraints = NO;
    maxLabel.text = [@(1000) stringValue];
    maxLabel.textAlignment = NSTextAlignmentLeft;
    maxLabel.textColor = [UIColor colorWithHex:@"#96A6A7" alpha:1.0];
    maxLabel.font = [UIFont systemFontOfSize:14 weight:UIFontWeightRegular];
    [maxLabel sizeToFit];
    [self addSubview:maxLabel];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:maxLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1.0 constant:20]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:maxLabel attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0 constant:-5]];
    
}

- (CALayer *)gradientLayer
{
    CALayer *gradientLayer = [CALayer layer];
    NSArray *colors = @[(__bridge id)[UIColor colorWithHex:@"#EA3552" alpha:1.0].CGColor,
                        (__bridge id)[UIColor colorWithHex:@"#F48422" alpha:1.0].CGColor,
                        (__bridge id)[UIColor colorWithHex:@"#FFB100" alpha:1.0].CGColor,
                        (__bridge id)[UIColor colorWithHex:@"#B1CE20" alpha:1.0].CGColor,
                        (__bridge id)[UIColor colorWithHex:@"#059D4B" alpha:1.0].CGColor];
    NSArray *locations = @[@0.45,@0.6,@0.75,@0.9,@1.0];
    CGImageRef image = [ArcGradient newImageGradientInRect:self.bounds colors:colors locations:locations];
    gradientLayer.masksToBounds = YES;
    CGRect rect = self.bounds;
    rect.size.height *=1.5;
    
    gradientLayer.frame = rect;
    gradientLayer.contents = (__bridge id)image;
    
    return gradientLayer;
}

- (void)addPointWithAnimation:(BOOL)animation
{
    
    
    CAShapeLayer *animPathPointLayer = [[CAShapeLayer alloc] init];
    CGRect rectForColorPoint =self.bounds;
    
    CGFloat aspect = self.bounds.size.height/self.bounds.size.width;
    
    CGFloat pointRadius = 5;
    CGFloat offset = 4*pointRadius+_widthArc;
    rectForColorPoint.size.width -= offset*aspect;
    rectForColorPoint.size.height -= offset*aspect;
    rectForColorPoint.origin.x +=(self.bounds.size.width -rectForColorPoint.size.width)/2;
    rectForColorPoint.origin.y +=offset/2;
    
    animPathPointLayer.path = [[self createArcPathFromBottomOfRect:rectForColorPoint points:_score] CGPath];
    animPathPointLayer.fillColor = [[UIColor clearColor] CGColor];
    animPathPointLayer.lineWidth = 1;
    animPathPointLayer.strokeColor = [[UIColor grayColor] CGColor];
    
    _pointColorLayer = [CAShapeLayer layer];
    _pointColorLayer.fillColor = [[UIColor blackColor] CGColor];
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointZero radius:pointRadius startAngle:0 endAngle:(2 * M_PI) clockwise:YES];
    _pointColorLayer.path = path.CGPath;
    
    
    
    _gradientLayerForPoint = [self gradientLayer];
    _gradientLayerForPoint.mask = _pointColorLayer;
    [self.layer addSublayer:_gradientLayerForPoint];
    
    CAKeyframeAnimation * theAnimation;
    theAnimation=[CAKeyframeAnimation animationWithKeyPath:@"position"];
    theAnimation.path=[[self createArcPathFromBottomOfRect:rectForColorPoint points:_score] CGPath];
    theAnimation.duration=animation?1:0;
    theAnimation.fillMode = kCAFillModeForwards;
    theAnimation.removedOnCompletion = NO;
    theAnimation.repeatCount = 0;
    theAnimation.calculationMode = kCAAnimationPaced;
//    [self.layer addSublayer:animPathPointLayer];
    
    [_pointColorLayer addAnimation:theAnimation forKey:@"position"];
    
}

-(void)showMarkScore
{
    if (!_labelMarkScoreColor)
        return;
    
    if (_labelMarkScore)
        [_labelMarkScore removeFromSuperview];
    
    
    
    _labelMarkScore = [UILabel new];
    _labelMarkScore.alpha = 0;
    _labelMarkScore.backgroundColor = _labelMarkScoreColor;
    
    NSInteger index=0;
    
    if (_score>500 && _score<=600) {
        index=1;
    }else if (_score>600 && _score<=700) {
        index=2;
    }else if (_score>700 && _score<=800) {
        index=3;
    }else if (_score>800) {
        index=4;
    }
    
    NSMutableParagraphStyle *style =  [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    style.alignment = NSTextAlignmentCenter;
    
    if (!_scoreDescription)
    {
        _scoreDescription = _desriptionScore[index];
    }
    
    NSAttributedString *attrText = [[NSAttributedString alloc]
                                    initWithString:_scoreDescription
                                    attributes:@{ NSParagraphStyleAttributeName : style,
                                                  NSFontAttributeName:[UIFont systemFontOfSize: 20]
                                                  }];

    _labelMarkScore.attributedText = attrText;
    
    _labelMarkScore.translatesAutoresizingMaskIntoConstraints = NO;
    _labelMarkScore.textAlignment = NSTextAlignmentCenter;
    _labelMarkScore.textColor = [UIColor whiteColor];
    
    
    [self addSubview:_labelMarkScore];
    [_labelMarkScore sizeToFit];
    _labelMarkScore.layer.cornerRadius = _labelMarkScore.frame.size.height/2;
    _labelMarkScore.clipsToBounds = YES;
    
    [self addConstraint:[NSLayoutConstraint widthFor:_labelMarkScore constant:_labelMarkScore.frame.size.width+30]];

    [self addConstraint:[NSLayoutConstraint centerX:self to:_labelMarkScore constant:0]];
    [self addConstraint:[NSLayoutConstraint bottomFrom:_labelMarkScore to:self constant:_labelMarkScore.frame.size.height*2]];
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.3
                     animations:^{
                         __strong typeof(weakSelf) blockSelf = weakSelf;
                         blockSelf.labelMarkScore.alpha = 1;
                     }];
}




- (UIBezierPath*) createArcPathFromBottomOfRect:(CGRect)rect  points:(NSInteger)points
{
    
    CGRect arcRect = CGRectMake(rect.origin.x+_widthArc/2, rect.origin.y, rect.size.width-_widthArc, rect.size.height);
    
    CGFloat arcRadius = (arcRect.size.height -_widthArc/2) ;
    arcRect.origin.y +=fabs(arcRect.size.height-arcRadius);
    
    CGPoint arcCenter = CGPointMake(arcRect.origin.x + arcRect.size.width/2, arcRect.origin.y + arcRadius);
    
    CGFloat angle = 180+180*points/1000.0;
    
    CGFloat startAngle = radians(180) ;
    CGFloat endAngle = radians(angle) ;
    
    UIBezierPath *bPath = [UIBezierPath bezierPathWithArcCenter:arcCenter
                                          radius:arcRadius
                                      startAngle:startAngle
                                        endAngle:endAngle
                                       clockwise:YES];
    
    return bPath;
}


-(void)updateScore:(NSInteger)score withDescription:(NSString *)description withAnimation:(BOOL)animation
{
    
    if (_score == score || _isAnimating) {
        return;
    }
    
    if (_labelMarkScore)
        [_labelMarkScore removeFromSuperview];
    
    if (_gradientLayerForPoint) {
        [_gradientLayerForPoint removeFromSuperlayer];
    }
    
    _isAnimating = YES;
    _score = score;
    _scoreDescription = description;
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:0.2
                     animations:^{
                         __strong typeof(weakSelf) blockSelf = weakSelf;
                         blockSelf.labelScore.alpha = 0;
                     } completion:^(BOOL finished) {
                         [UIView animateWithDuration:0.2
                                          animations:^{
                                              __strong typeof(weakSelf) blockSelf = weakSelf;
                                              blockSelf.labelScore.text = [@(blockSelf.score) stringValue];
                                              blockSelf.labelScore.alpha = 1;
                                          } completion:nil];
                     }];
    
    _arcColorLayer.path = [[self createArcPathFromBottomOfRect:self.bounds points:_score] CGPath];
    
    [self addPointWithAnimation:animation];
    
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.delegate = self;
    pathAnimation.duration =animation? 1.0 : 0;
    pathAnimation.fromValue = @(0.0f);
    pathAnimation.toValue = @(1.0f);
    pathAnimation.removedOnCompletion = NO;
    pathAnimation.fillMode = kCAFillModeForwards;
    [_arcColorLayer addAnimation:pathAnimation forKey:@"drawLineAnimation"];
    


}

-(UIColor *) colorOfPoint:(CGPoint)point
{
    unsigned char pixel[4] = {0};
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(pixel,
                                                 1, 1, 8, 4, colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedLast);
    
    CGContextTranslateCTM(context, -point.x, -point.y);
    
    [self.layer renderInContext:context];
    
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    UIColor *color = [UIColor colorWithRed:pixel[0]/255.0
                                     green:pixel[1]/255.0 blue:pixel[2]/255.0
                                     alpha:pixel[3]/255.0];
    return color;
}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    NSMutableArray *pathPoints = [[NSMutableArray alloc] init];
    
    CGPathApply(_arcColorLayer.path, (__bridge void*)pathPoints, &__path_applay);
    
    CGPoint lastPoint = [[pathPoints lastObject] CGPointValue];
    lastPoint = CGPointMake(lastPoint.x-2, lastPoint.y+2);
    if (_score>500) {
        lastPoint = CGPointMake(lastPoint.x-2, lastPoint.y-2);
    }
    
    _isAnimating = NO;
    _labelMarkScoreColor = [self colorOfPoint:lastPoint];
    [self showMarkScore];

}

@end

void __path_applay(void *info,const CGPathElement *element)
{
    
    NSMutableArray *bezierPoints = (__bridge NSMutableArray*) info;
    
    CGPoint *points = element->points;
    CGPathElementType type = element->type;
    
    switch(type) {
        case kCGPathElementMoveToPoint: // contains 1 point
//            [bezierPoints addObject:[NSValue valueWithCGPoint:points[0]]];
            break;
            
        case kCGPathElementAddLineToPoint: // contains 1 point
            [bezierPoints addObject:[NSValue valueWithCGPoint:points[0]]];
            break;
            
        case kCGPathElementAddQuadCurveToPoint: // contains 2 points
            [bezierPoints addObject:[NSValue valueWithCGPoint:points[0]]];
            [bezierPoints addObject:[NSValue valueWithCGPoint:points[1]]];
            break;
            
        case kCGPathElementAddCurveToPoint: // contains 3 points
//            [bezierPoints addObject:[NSValue valueWithCGPoint:points[0]]];
//            [bezierPoints addObject:[NSValue valueWithCGPoint:points[1]]];
            [bezierPoints addObject:[NSValue valueWithCGPoint:points[2]]];
            break;
            
        case kCGPathElementCloseSubpath: // contains no point
            break;
    }
 
}


