//
//  ArcScore.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 23.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ArcScoreDelegate <NSObject>

@end

@interface ArcScore : UIView

@property (nonatomic, assign) BOOL isFatChart;

- (void)updateScore:(NSInteger)score withDescription:(NSString *)description withAnimation:(BOOL)animation;

@end
