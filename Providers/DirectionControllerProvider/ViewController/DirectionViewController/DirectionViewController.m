    //
//  DirectionViewController.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 04.10.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <AVFoundation/AVFoundation.h>
#import <Availability.h>

#import "DirectionViewController.h"
#import "CoreNavigationController.h"
#import "CoreNavigationControllerView.h"

#import "TCoreImagePickerViewController.h"
#import "TCoreCircleLoadActivity.h"
#import "TCoreWebViewController.h"
#import "TCoreColorList.h"
#import "BaseCollectionViewCell.h"
#import "CollectionHeaderView.h"
#import "DirectionCollectionViewFlowLayout.h"

#import "DirectionViewModel.h"
#import "CollectionCellHeaderModel.h"
#import "BottomButtonWithArrow.h"

#import "UIColor+CoreCustomColors.h"
#import "NSObject+CoreUtils.h"
#import "NSLayoutConstraint+CoreUtils.h"
#import "UIVIew+CoreAnimations.h"
#import "UIImage+CoreUtils.h"
#import "CellManager.h"

#import "CollectionViewCellsDefinitions.h"

#import "TextFieldCollectionViewOutput.h"
#import "ButtonCollectionViewCellOutput.h"
#import "AddPhotoCollectionViewCellOutput.h"
#import "LabelCollectionViewCellOutput.h"

static NSString *const kImagePickerMementoKey = @"ImagePickerMemento";


@interface DirectionViewController () <    UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate, TextFieldCollectionViewOutput, ButtonCollectionViewCellOutput, AddPhotoCollectionViewCellOutput, LabelCollectionViewCellOutput>

@property (strong, nonatomic) DirectionViewModel *viewModel;

@property (strong, nonatomic) DirectionCollectionViewFlowLayout *collectionViewLayout;


@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UIView *viewHeader;

@property (strong, nonatomic) NSMutableArray *subviews;

@property (strong, nonatomic) UITextField *activeTextField;
//HEADER VIEW PROPERTIES

@property (weak, nonatomic) IBOutlet UIButton *buttonStepOne;
@property (weak, nonatomic) IBOutlet UIButton *buttonStepTwo;
@property (weak, nonatomic) IBOutlet UIView *viewDotOne;
@property (weak, nonatomic) IBOutlet UIView *viewDotTwo;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintCollectionViewBottom;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewCheck;

@property (nonatomic, strong) TCoreCircleLoadActivity *circleLoadActivity;

@property (nonatomic, assign) CGPoint contentOffset;

@property (nonatomic, assign) BOOL isUpdating;
@end

@implementation DirectionViewController

#pragma mark - init methods

- (instancetype) initWithViewModel:(DirectionViewModel *)viewModel
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    self.viewModel = viewModel;
    return self;
}

- (void) dealloc
{
    [self.circleLoadActivity removeFromSuperview];
    self.circleLoadActivity = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"%@ controller deallocated",NSStringFromClass(self.class));
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }

    self.subviews = [NSMutableArray array];
    
    _collectionView.collectionViewLayout =[DirectionCollectionViewFlowLayout new];
    _collectionViewLayout = (DirectionCollectionViewFlowLayout*)_collectionView.collectionViewLayout;
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    [CellManager registerCellsWithId:@[kCollectionViewWarningCell,
                                       kCollectionViewTextFieldCell,
                                       kCollectionViewSelectableCell,
                                       kAddPhotoCollectionViewCell,
                                       kBlueButtonCollectionViewCell,
                                       kLabelCollectionViewCell] colectionView:_collectionView];
    
    [_collectionView registerNib:[UINib nibWithNibName:kCollectionHeaderView bundle:nil] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kCollectionHeaderView];
    
    
    [self registerForKeyboardNotification];

    self.navigationController.title = @"Анкета";
    self.navigationItem.leftBarButtonItem = nil;
//    self.automaticallyAdjustsScrollViewInsets = YES;

    UIView *ncView = self.navigationController.view;
    TCoreCircleLoadActivity *circleLoadActivity = [[TCoreCircleLoadActivity alloc] init];
    circleLoadActivity.titleText = @"Ваша заявка отправляется…";
    circleLoadActivity.translatesAutoresizingMaskIntoConstraints = NO;
    circleLoadActivity.alpha = 0.0f;
    self.circleLoadActivity = circleLoadActivity;

    [ncView addSubview:circleLoadActivity];
    [ncView addConstraints:[NSLayoutConstraint standartBoxConstratinsFrom:ncView to:circleLoadActivity constant:0.0f]];
    [ncView bringSubviewToFront:circleLoadActivity];

    self.circleLoadActivity = circleLoadActivity;

    [self.collectionView.collectionViewLayout invalidateLayout];

    [self bindViewModel];
}


- (void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    CoreNavigationController *navController = [self.navigationController coreAsClass:[CoreNavigationController class]];
  //  navController.coreSwipeEnabled = YES;
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    if([[UIDevice currentDevice]userInterfaceIdiom]==UIUserInterfaceIdiomPhone) {
        
        switch ((int)[[UIScreen mainScreen] nativeBounds].size.height) {

            case 2436:
                [self.collectionView.collectionViewLayout invalidateLayout];
                [self.collectionView performBatchUpdates:^{
                    [self.collectionView reloadData];
                }  completion:nil];
                break;

        }
    }


}

- (void)registerForKeyboardNotification {
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShownNotification:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHiddenNotification:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShownNotification:(NSNotification *)aNotification {
    
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.constraintCollectionViewBottom.constant = kbSize.height;
    
}

- (void)keyboardWillBeHiddenNotification:(NSNotification *)aNotification {
#pragma unused(aNotification)
    self.constraintCollectionViewBottom.constant = 0.0f;
}

#pragma mark - UICollectionView data souce

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
#pragma unused(collectionView)
    return _viewModel.datasource.countSections;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
#pragma unused(collectionView)
    return [_viewModel.datasource countCellInSection:section];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    BaseCollectionViewCell *cell = [collectionView  dequeueReusableCellWithReuseIdentifier:[_viewModel.datasource cellIdAtIndexPath:indexPath] forIndexPath:indexPath];
    
    cell.delegate = self;
    NSString *key = [_viewModel.datasource cellConfigAtIndexPath:indexPath].key;
    
    [cell configureCell:[self.viewModel.datasource cellConfigAtIndexPath:indexPath]];
    [cell fillWithData:[self.viewModel dataForKey:key]];
    
    return cell;
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    CollectionHeaderView *headerView = nil;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader])
    {
        headerView = [collectionView dequeueReusableSupplementaryViewOfKind:
                      UICollectionElementKindSectionHeader withReuseIdentifier:kCollectionHeaderView forIndexPath:indexPath];
        headerView.title.text = [_viewModel.datasource headerForSection:indexPath.section].title;
    }
    return headerView;
}

#pragma mark - utilites methods

- (void) bindViewModel
{
    _isUpdating = NO;
    __weak typeof(self) weakSelf = self;
    [self.viewModel.signalUpdate subscribeNext:^(id value) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if (blockSelf.isUpdating) {
            return ;
        }
        blockSelf.isUpdating = YES;
        blockSelf.contentOffset = blockSelf.collectionView.contentOffset;
        [UIView performWithoutAnimation:^{
            if (@available(iOS 10.0, *))
            {
                [blockSelf.collectionView.collectionViewLayout invalidateLayout];

                [blockSelf.collectionView performBatchUpdates:^{
                    [blockSelf.collectionView setContentOffset:blockSelf.contentOffset];
                } completion:^(BOOL finished) {
                    blockSelf.isUpdating = NO;
                }];
            }else{
                [blockSelf.collectionView performBatchUpdates:nil completion:^(BOOL finished) {
                    [blockSelf.collectionView.collectionViewLayout invalidateLayout];
                    blockSelf.isUpdating = NO;
                }];
            }
        }];
    }];
//
   [[_viewModel.signalErrorDescription deliverOnMainThread]  subscribeNext:^(id  _Nullable message) {
        __strong typeof(weakSelf) blockSelf = weakSelf;

        if (!message)
        {
            return;
        }

        [blockSelf showAlertWithTitle:@"Ошибка" message:message];
     }];
//
    [[_viewModel.signalUploadDescription deliverOnMainThread] subscribeNext:^(id  _Nullable message) {
        __strong typeof(weakSelf) blockSelf = weakSelf;

        if (!message)
        {
            return;
        }

        blockSelf.circleLoadActivity.titleText = [message coreStringValue];
    }];
//
    [[_viewModel.signalDirectionSendStatus deliverOnMainThread] subscribeNext:^(id  _Nullable flag) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        [blockSelf showLoadActivityWithStatus:[flag coreIntegerValue]];
    }];
//
    [[_viewModel.signalTermOfUserURL deliverOnMainThread] subscribeNext:^(id  _Nullable message) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if (!message)
        {
            return;
        }
        [blockSelf showWebViewControllerWithURLString:message];
    }];
    
}



#pragma mark - UIImagePickerController delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
#pragma unused(info)
    
    TCoreImagePickerViewController *tempPicker = [picker coreAsClass:[TCoreImagePickerViewController class]];
    NSDictionary *userInfo = tempPicker.userInfo;
    NSString *key = [userInfo valueForKey:kImagePickerMementoKey];
    UIImage *image = [[info valueForKey:UIImagePickerControllerOriginalImage] coreImageUpOrientation];
   
    [picker dismissViewControllerAnimated:YES completion:^{
        [self.viewModel provideImage:image forKey:key];
    }];
    
}

#pragma mark - ButtonCollectionViewCellOutput

-(void)pressButton
{
    [self.viewModel actionContinue];    
}

#pragma mark AddPhotoCollectionViewCellOutput

-(void)pressAddPhotoWithKey:(NSString *)key
{
    _contentOffset = _collectionView.contentOffset;
#if TARGET_IPHONE_SIMULATOR
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary fromSender:key];
#else
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusDenied)
    {
        UIAlertController *alertController =
        [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Unable to access the Camera", @"")
                                            message:NSLocalizedString(@"To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app.", @"")
                                     preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
        UIAlertAction *openSettings = [UIAlertAction actionWithTitle:NSLocalizedString(@"Settings", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        #pragma unused(action)
            
            UIApplication *sharedApplication = [UIApplication sharedApplication];
            NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            
            if (url)
            {
                if ([sharedApplication canOpenURL:url])
                {
                    [sharedApplication openURL:url];
                }
            }
        }];
        
        [alertController addAction:ok];
        [alertController addAction:openSettings];
        
        [self presentViewController:alertController animated:YES completion:nil];
    }
    else if (authStatus == AVAuthorizationStatusNotDetermined)
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^( BOOL granted ) {
            if (granted)
            {
                 [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera fromSender:key];
            }
        }];
    else
    {
        [self showImagePickerForSourceType:UIImagePickerControllerSourceTypeCamera fromSender:key];
    }
#endif
}

-(NSData *)getPhotoWithKey:(NSString *)key
{
   return [_viewModel getPhotoWithKey:key];
}

#pragma mark - pages cell delegate

- (void)cellTextFieldDidBeginEditing:(UITextField *)textField withRowKey:(NSString *)kRowKey
{
//    self.lastActiveKey = kRowKey;
    self.activeTextField = textField;
}

- (void)cellTextFieldDidEndEditing:(UITextField *)textField withRowKey:(NSString *)kRowKey
{
    [self.viewModel textDidEndEditing:textField.text key:kRowKey];
}

- (void)cellTextFieldNextAction:(NSDictionary *)rowDict
{
////    self.row = [self.viewModel nextRow:rowDict];
//    NSIndexPath *indexPath = [self.viewModel indexPathAtRow:self.row];
//    UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
//    if (cell == nil)
//    {
//        self.isShowCell = YES;
//        [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
//    }
//    else
//    {
//        [self showKeyboardCell];
//    }
}

- (void)cellTextFieldDoneAction:(NSDictionary *)rowDict
{
#pragma unused(rowDict)
    [self.activeTextField resignFirstResponder];
}
#pragma mark LabelCollectionViewCellOutput
-(void)didPressText
{
    [self.viewModel actionShowTermsOfUser];
}


#pragma mark - utils

- (void) showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType fromSender:(id)sender
{
    __weak typeof(self) weakSelf = self;
    void (^Picker)(void) = ^{
            __strong typeof(weakSelf) blockSelf = weakSelf;
        TCoreImagePickerViewController *picker = [[TCoreImagePickerViewController alloc] initWithUserInfo:@{kImagePickerMementoKey:sender}];
        picker.sourceType = sourceType;
        picker.delegate = blockSelf;
        picker.modalPresentationStyle = UIModalPresentationFullScreen;
        
        [blockSelf presentViewController:picker animated:YES completion:nil];
    };
    
    if ([NSThread isMainThread])
    {
        Picker();
    }else
    {
        [[NSOperationQueue mainQueue] addOperationWithBlock:Picker];
    }
    
}

- (void) showLoadActivityWithStatus:(kUserDirectionSendStatus)status
{
    CoreNavigationController *navController = [self.navigationController coreAsClass:[CoreNavigationController class]];
    TCoreCircleLoadActivity *circleLoadActivity = self.circleLoadActivity;

    if (!navController)
    {
        return;
    }

    switch (status) {
        case kUserDirectionSendStatus_Start:
          //  navController.coreSwipeEnabled = NO;
            [self.circleLoadActivity coreAnimatedShowWithDuration:0.4f alpha:1.0f];
            [circleLoadActivity startAnimation];
            break;
        case kUserDirectionSendStatus_Finish:
          //  navController.coreSwipeEnabled = YES;
            [self stopLoadActivity:circleLoadActivity AndHideView:navController];
            break;
        case kUserDirectionSendStatus_Error:
         //   navController.coreSwipeEnabled = YES;
            [self.circleLoadActivity coreAnimatedHideWithDuration:0.4f];
            break;
        default:
            break;
    }
    
}

- (void) stopLoadActivity:(TCoreCircleLoadActivity *)circleLoadActivity AndHideView:(CoreNavigationController *)navController {
#pragma unused(navController)
    __weak typeof(self) weakSelf = self;
    [circleLoadActivity stopAnimationWithCompletion:^{
        __strong typeof(weakSelf) blockSelf = weakSelf;
        [blockSelf.circleLoadActivity coreAnimatedHideWithDuration:0.4];
    }];
}

- (void) showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:action];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


- (void) showWebViewControllerWithURLString:(NSString *)urlString
{
    TCoreWebViewController *webViewController = [[TCoreWebViewController alloc] initWithURLString:urlString];
//    webViewController.barBackgroundColor = COLOR_TEMP_VIEW_BACKGROUND;
//    webViewController.barTintColor = COLOR_TEMP_VIEW_BACKGROUND;
//    webViewController.tintColor = [UIColor whiteColor];
    webViewController.backButtonImage = [UIImage imageNamed:@"ic_arrow_back_white"];
    
    [self presentViewController:webViewController animated:YES completion:nil];
}

@end
