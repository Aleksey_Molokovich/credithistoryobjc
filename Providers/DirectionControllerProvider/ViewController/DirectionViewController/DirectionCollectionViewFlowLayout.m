//
//  DirectionCollectionViewFlowLayout.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 11.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "DirectionCollectionViewFlowLayout.h"
#import "CollectionViewCellsDefinitions.h"

@implementation DirectionCollectionViewFlowLayout
- (instancetype)init
{
    if ( (self = [super init]) == nil )
    {
        return nil;
    }
    [self configure];
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]) == nil)
    {
        return  nil;
    }
    [self configure];
    return self;
}

-(void)configure
{
    self.minimumInteritemSpacing = kCollectionViewInteritemSpacing;
    self.minimumLineSpacing = kCollectionViewMinimumLineSpacing;
    self.sectionInset = kCollectionViewInsets;
    self.estimatedItemSize = CGSizeMake(1,1);
    self.headerReferenceSize =CGSizeMake([UIScreen mainScreen].bounds.size.width,
                                         kCollectionViewSectionHeight);
}



- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    CGRect oldBounds = self.collectionView.bounds;
    if (!CGSizeEqualToSize(oldBounds.size, newBounds.size)) {
        return YES;
    }
    return NO;
}
@end

