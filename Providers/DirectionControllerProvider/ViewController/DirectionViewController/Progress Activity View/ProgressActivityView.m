//
//  ProgressActivityView.m
//  CreditCalendar
//
//  Created by ios on 28.04.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ProgressActivityView.h"
#import "ActivityIndicatorView.h"

#import "UIColor+CoreCustomColors.h"
#import "UIVIew+CoreAnimations.h"

@interface ProgressActivityView()

@property (nonatomic, strong) IBOutlet ActivityIndicatorView *activityIndicatorView;

@end

@implementation ProgressActivityView

- (instancetype) init
{
    self = [[NSBundle mainBundle] loadNibNamed:@"ProgressActivityView" owner:nil options:nil].firstObject;
    
    if (!self)
    {
        return nil;
    }
    
    self.activityIndicatorView.strokeColor = [UIColor colorWithHex:@"16A245" alpha:1.0f];
    self.activityIndicatorView.lineWidth = 3;
    
    return self;
    
}

- (void) hideAnimated:(BOOL)animated
{
    if (animated)
    {
        __weak typeof(self) weakSelf = self;
        [weakSelf coreAnimatedHideWithDuration:0.4 withCompletionBlock:^{
            __strong typeof(weakSelf) blockSelf = weakSelf;
            [blockSelf.activityIndicatorView startAnimation];
        }];
        return;
    }
    self.alpha = 0.0f;
    [self.activityIndicatorView stopAnimation];
    
}

- (void) showAnimated:(BOOL)animated
{
    
    if (animated)
    {
        __weak typeof(self) weakSelf = self;
        [self coreAnimatedShowWithDuration:0.4 alpha:1.0f withCompletionBlock:^{
            __strong typeof(weakSelf) blockSelf = weakSelf;
            [blockSelf.activityIndicatorView startAnimation];
        }];
        return;
    }
    
    self.alpha = 1.0f;
    [self.activityIndicatorView startAnimation];
    
}

@end
