//
//  ProgressActivityView.h
//  CreditCalendar
//
//  Created by ios on 28.04.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProgressActivityView : UIView

- (void) showAnimated:(BOOL)animated;

- (void) hideAnimated:(BOOL)animated;

@end
