//
//  PassportView.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 13.09.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "PassportView.h"
#import "DirectionViewModel.h"

#import "PagesHeaderView.h"
#import "PagesCellDelegate.h"

#import "TextFieldCollectionViewCell.h"
#import "MultipleTextFieldCollectionViewCell.h"
#import "AddPhotoCollectionViewCell.h"
#import "DescriptionCollectionViewCell.h"
#import "ConditionsCollectionViewCell.h"


static NSString *const kPassportCollectionViewTFCellID = @"TextFieldCollectionViewCell";
static NSString *const kPassportCollectionViewMultipleTFCellID = @"multipleTextFieldCellId";
static NSString *const kPassportCollectionViewAddPhotoCellID = @"addPhotoCellId";
static NSString *const kPassportCollectionViewDescriptionCellID = @"descriptionCellId";
static NSString *const kPassportCollectionViewConditionsCellID = @"conditionsCellId";
static NSString *const kPassportCollectionViewHeaderID = @"headerId";

static UIEdgeInsets const kPassportCollectionViewInsets = {26.0f, 20.0f, 0.0f, 20.0f};

static CGFloat const kPassportCollectionViewMinimumLineSpacing = 0.0f;

static CGFloat const kPassportCollectionViewInteritemSpacing = 8.0f;

static CGFloat const kPassportCollectionViewTFCellHeight = 83.0f;
static CGFloat const kPassportCollectionViewMultipleTFCellHeight = 217.0f;
static CGFloat const kPassportCollectionViewdDescriptionCellHeight = 37.0f;
static CGFloat const kPassportCollectionViewConditionsCellHeight = 145.0f;

@interface PassportView() <UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,PagesCellDelegate >

@property (weak, nonatomic) id<DirectionViewModelType> viewModel;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (strong, nonatomic) UITextField *textField;

@property (strong, nonatomic) NSIndexPath *indexPathSelectedCell;

@property (strong, nonatomic) NSDictionary *row;
@property (assign, nonatomic) BOOL isShowCell;
@end


@implementation PassportView

#pragma mark - init methods

- (instancetype) initWithViewModel:(id)viewModel
{
    self = [[NSBundle mainBundle] loadNibNamed:@"PassportView" owner:nil options:nil].firstObject;
    if ( self == nil)
    {
        return nil;
    }
    self.viewModel = viewModel;
    id<DirectionViewModelOutputs> outputs = self.viewModel.outputs;

    @weakify(self);
    [outputs.signalImageUpdate subscribeNext:^(id x) {
        @strongify(self);
#pragma unused(x)
        UICollectionViewCell *cell= [self.collectionView cellForItemAtIndexPath:self.indexPathSelectedCell];
        [(AddPhotoCollectionViewCell *)cell updateImage:x];
    }];
    
    [outputs.signalInvalideDataPassport subscribeNext:^(id x) {
        @strongify(self);
#pragma unused(x)
        [self setupErrors];
    }];
    return self;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    UINib *nibTFCell = [UINib nibWithNibName:@"TextFieldCollectionViewCell" bundle:nil];
    UINib *nibMultipleTFCell = [UINib nibWithNibName:@"MultipleTextFieldCollectionViewCell" bundle:nil];
    UINib *nibAddPhotoCell = [UINib nibWithNibName:@"AddPhotoCollectionViewCell" bundle:nil];
    UINib *nibDescriptionCell = [UINib nibWithNibName:@"DescriptionCollectionViewCell" bundle:nil];
    UINib *nibConditionsCell = [UINib nibWithNibName:@"ConditionsCollectionViewCell" bundle:nil];
    UINib *nibHeader = [UINib nibWithNibName:@"PagesHeaderView" bundle:nil];
    [self.collectionView registerNib:nibHeader forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kPassportCollectionViewHeaderID];
    [self.collectionView registerNib:nibTFCell forCellWithReuseIdentifier:kPassportCollectionViewTFCellID];
    [self.collectionView registerNib:nibMultipleTFCell forCellWithReuseIdentifier:kPassportCollectionViewMultipleTFCellID];
    [self.collectionView registerNib:nibAddPhotoCell forCellWithReuseIdentifier:kPassportCollectionViewAddPhotoCellID];
    [self.collectionView registerNib:nibDescriptionCell forCellWithReuseIdentifier:kPassportCollectionViewDescriptionCellID];
    [self.collectionView registerNib:nibConditionsCell forCellWithReuseIdentifier:kPassportCollectionViewConditionsCellID];
}

-(void)dealloc
{
     NSLog(@"%@ controller deallocated",NSStringFromClass(self.class));
}

- (void)viewWillLayoutSubviews {
    [self.collectionView.collectionViewLayout invalidateLayout];
}

- (void) didMoveToSuperview
{
    if (!self.superview)
    {
        return;
    }
    
    [self.collectionView reloadData];
}

#pragma mark - UICollectionView delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
#pragma unused(collectionView)
    [self.textField resignFirstResponder];
    NSDictionary *section = self.viewModel.outputs.dataSource[indexPath.section];
    NSDictionary *row = [section[kDirectionSectionRowsKey] objectAtIndex:indexPath.row];
    if ([row[kDirectionRowCell] isEqualToString:kDirectionCellAddPhoto])
    {
        [self resignFirstResponder];
        self.indexPathSelectedCell = indexPath;
        [self showDialogController:row[kDirectionRowKey]];
    }
}

#pragma mark - UICollectionView data source

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell = nil;
    NSDictionary *section = self.viewModel.outputs.dataSource[indexPath.section];
    NSDictionary *row = [section[kDirectionSectionRowsKey] objectAtIndex:indexPath.row];
    BOOL isLastCell = indexPath.row == [section[kDirectionSectionRowsKey] count] - 1 ? YES : NO;
    if ([row[kDirectionRowCell] isEqualToString:kDirectionCellTF])
    {
        TextFieldCollectionViewCell *cellTF = [collectionView dequeueReusableCellWithReuseIdentifier:kPassportCollectionViewTFCellID forIndexPath:indexPath];
        cellTF.isLastCell = isLastCell;
        [cellTF configWithDict:row];
        cellTF.delegate = self;
        cellTF.value = [self.viewModel.outputs textForKey:row[kDirectionRowKey]];
        cell = cellTF;
    }
    else if ([row[kDirectionRowCell] isEqualToString:kDirectionCellMultipleTF])
    {
        MultipleTextFieldCollectionViewCell *cellMultipleTF = [collectionView dequeueReusableCellWithReuseIdentifier:kPassportCollectionViewMultipleTFCellID forIndexPath:indexPath];
        cellMultipleTF.isLastCell = isLastCell;
        [cellMultipleTF configWithDict:row andViewModel:self.viewModel];
        cellMultipleTF.delegate = self;
        cell = cellMultipleTF;
    }
    else if ([row[kDirectionRowCell] isEqualToString:kDirectionCellAddPhoto])
    {
        AddPhotoCollectionViewCell *cellAddPhoto = [collectionView dequeueReusableCellWithReuseIdentifier:kPassportCollectionViewAddPhotoCellID forIndexPath:indexPath];
        if ([row[kDirectionRowKey] isEqualToString:@"photoClient"]) {
            
            [cellAddPhoto configWithDict:row andImage:self.viewModel.outputs.photoClient];
        }
        else if ([row[kDirectionRowKey] isEqualToString:@"photoPassport"])
        {
            [cellAddPhoto configWithDict:row andImage:self.viewModel.outputs.photoPassport];
        }
        cell = cellAddPhoto;
    }
    else if ([row[kDirectionRowCell] isEqualToString:kDirectionCellDescription])
    {
        DescriptionCollectionViewCell *cellDescription = [collectionView dequeueReusableCellWithReuseIdentifier:kPassportCollectionViewDescriptionCellID forIndexPath:indexPath];
        
        cell = cellDescription;
        
    }
    else if ([row[kDirectionRowCell] isEqualToString:kDirectionCellConditions])
    {
        ConditionsCollectionViewCell *cellConditions = [collectionView dequeueReusableCellWithReuseIdentifier:kPassportCollectionViewConditionsCellID forIndexPath:indexPath];
        cellConditions.delegate = self;
        id <DirectionViewModelType> viewModel = self.viewModel;
        [cellConditions configWithViewModel:viewModel];
        cell = cellConditions;
        
    }
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
#pragma unused(collectionView)
    return self.viewModel.outputs.dataSource.count;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
#pragma unused(collectionView)
    NSDictionary *sectionDict = self.viewModel.outputs.dataSource[section];
    NSDictionary *rows = sectionDict[kDirectionSectionRowsKey];
    return rows.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    
    UICollectionReusableView *reusableView = nil;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        NSDictionary *section = self.viewModel.outputs.dataSource[indexPath.section];
        PagesHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kPassportCollectionViewHeaderID forIndexPath:indexPath];
        [headerView configWithName:section[kDirectionSectionNameKey]];
        reusableView = headerView;
    }
    return reusableView;
}

#pragma mark - UICollectionViewDelegateFlowLayout methods

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
#pragma unused(collectionView, collectionViewLayout)
    CGSize size;
    NSDictionary *section = self.viewModel.outputs.dataSource[indexPath.section];
    NSDictionary *row = [section[kDirectionSectionRowsKey] objectAtIndex:indexPath.row];
    if ([row[kDirectionRowSize] isEqualToString:kDirectionFullSize]) {
        size = CGSizeMake(CGRectGetWidth(self.collectionView.bounds) - (kPassportCollectionViewInsets.left + kPassportCollectionViewInsets.right), kPassportCollectionViewTFCellHeight);
    }
    else if ([row[kDirectionRowSize] isEqualToString:kDirectionHalfSize])
    {
        size = CGSizeMake((CGRectGetWidth(self.collectionView.bounds) - kPassportCollectionViewInsets.right - kPassportCollectionViewInsets.left - kPassportCollectionViewInteritemSpacing) / 2, kPassportCollectionViewTFCellHeight);
    }
    else if ([row[kDirectionRowSize] isEqualToString:kDirectionMultipleTFSize])
    {
        size = CGSizeMake(CGRectGetWidth(self.collectionView.bounds) - (kPassportCollectionViewInsets.left + kPassportCollectionViewInsets.right), kPassportCollectionViewMultipleTFCellHeight);
    }
    else if ([row[kDirectionRowSize] isEqualToString:kDirectionPhotoSize])
    {
        float width = (CGRectGetWidth(self.collectionView.bounds) - kPassportCollectionViewInsets.right - kPassportCollectionViewInsets.left - kPassportCollectionViewInteritemSpacing) / 2;
        size = CGSizeMake(width, width);
        
    }
    else if ([row[kDirectionRowSize] isEqualToString:kDirectionDescriptionSize])
    {
        size = CGSizeMake(CGRectGetWidth(self.collectionView.bounds) - (kPassportCollectionViewInsets.left + kPassportCollectionViewInsets.right), kPassportCollectionViewdDescriptionCellHeight);
    }
    else if ([row[kDirectionRowSize] isEqualToString:kDirectionConditionsSize])
    {
        size = CGSizeMake(CGRectGetWidth(self.collectionView.bounds) - (kPassportCollectionViewInsets.left + kPassportCollectionViewInsets.right), kPassportCollectionViewConditionsCellHeight);
    }
    return size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
#pragma unused(collectionView, collectionViewLayout, section)
    return kPassportCollectionViewInsets;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
#pragma unused(collectionView, collectionViewLayout, section)
    return kPassportCollectionViewMinimumLineSpacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
#pragma unused(collectionView, collectionViewLayout, section)
    return kPassportCollectionViewInteritemSpacing;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
#pragma unused(collectionView, collectionViewLayout, section)
   
    CGSize size = CGSizeMake(CGRectGetWidth(self.collectionView.bounds), 35.0f);
    return size;
}
#pragma mark - utilites methods

- (void)setupErrors
{
    id <DirectionViewModelType> viewModel = self.viewModel;
    id <DirectionViewModelOutputs> outputs = viewModel.outputs;
    for (NSDictionary *row in outputs.invalidePassportRows) {
        NSIndexPath *indexPath = [outputs indexPathAtRow:row];
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
        if ([row[kDirectionRowCell] isEqualToString:kDirectionCellTF]) {
            [((TextFieldCollectionViewCell *)cell) showError:row[kDirectionRowError]];
        }
        else if ([row[kDirectionRowCell] isEqualToString:kDirectionCellMultipleTF]) {
             [((MultipleTextFieldCollectionViewCell *)cell) showError:row[kDirectionRowError] key:row[kDirectionRowKey]];
        }
        else if ([row[kDirectionRowCell] isEqualToString:kDirectionCellAddPhoto]) {
            [((AddPhotoCollectionViewCell *)cell) showError:row[kDirectionRowError]];
        }
        else if ([row[kDirectionRowCell] isEqualToString:kDirectionCellConditions]) {
            [((ConditionsCollectionViewCell *)cell) showError:row[kDirectionRowError]];
        }
    }
}



- (void)showDialogController:(NSString *)key
{
    self.lastActiveKey = key;
    [self sendActionsForControlEvents:UIControlEventTouchUpInside];
}

-(void)showKeyboardCell
{
    NSIndexPath *indexPath = [self.viewModel.outputs indexPathAtRow:self.row];
    UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    if (self.row[kDirectionRowCell] == kDirectionCellTF) {
        [((TextFieldCollectionViewCell *)cell) becomeFirstResponder];
    }
    else if (self.row[kDirectionRowCell] == kDirectionCellMultipleTF)
    {
        [((MultipleTextFieldCollectionViewCell *)cell) becomeFirstResponder];
    }
    }

#pragma mark - UIScrollView delegate

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
#pragma unused(scrollView)
    if (self.isShowCell) {
        [self showKeyboardCell];
        self.isShowCell = NO;
    }
}


#pragma mark - pages cell delegate

- (void)cellTextFieldDidBeginEditing:(UITextField *)textField withRowKey:(NSString *)kRowKey
{
    self.lastActiveKey = kRowKey;
    self.textField = textField;
}

- (void)cellTextFieldDidEndEditing:(UITextField *)textField withRowKey:(NSString *)kRowKey
{
    [self.viewModel.inputs textDidEndEditing:textField.text key:kRowKey];
}

- (void)cellTextFieldNextAction:(NSDictionary *)rowDict
{
    self.row = [self.viewModel.outputs nextRow:rowDict];
    NSIndexPath *indexPath = [self.viewModel.outputs indexPathAtRow:self.row];
    UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    if (cell == nil)
    {
        self.isShowCell = YES;
        [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
    }
    else
    {
        [self showKeyboardCell];
    }
}

- (void)cellTextFieldDoneAction:(NSDictionary *)rowDict
{
#pragma unused(rowDict)
    [self.textField resignFirstResponder];
}

-(void)showTermOfUse
{
    id <DirectionViewModelType> viewModel = self.viewModel;
    id <DirectionViewModelInputs> inputs = viewModel.inputs;
    [inputs actionShowTermsOfUser];
}


@end
