//
//  PassportView.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 13.09.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PassportView : UIControl

- (instancetype) initWithViewModel:(id)viewModel;

@property (nonatomic, strong) NSString *lastActiveKey;

@end
