//
//  DescriptionCollectionViewCell.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 07.02.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionCollectionViewCell : UICollectionViewCell
- (void)configWithDict:(NSDictionary *)dict;
@end
