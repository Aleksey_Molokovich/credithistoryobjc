//
//  SelectedCollectionViewCell.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 09.02.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DirectionViewModel.h"
#import "PagesCellDelegate.h"
@interface SelectedCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) id <PagesCellDelegate> delegate;

- (void)configWithViewModel:(id <DirectionViewModelType>)viewModel;

@end
