//
//  MultipleTextFieldCollectionViewCell.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 26.10.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PagesCellDelegate;
@protocol DirectionViewModelType;

@interface MultipleTextFieldCollectionViewCell : UICollectionViewCell

@property (assign, nonatomic) BOOL isLastCell;
@property (weak, nonatomic) id <PagesCellDelegate> delegate;

-(void)configWithDict:(NSDictionary *)dict andViewModel:(id<DirectionViewModelType>)viewModel;
- (void)showError:(NSString *)error key:(NSString *)key;

@end
