//
//  DescriptionTableViewCell.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 07.02.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "DescriptionCollectionViewCell.h"

@interface DescriptionCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;

@end

@implementation DescriptionCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)configWithDict:(NSDictionary *)dict
{
    #pragma unused(dict)
    
}

@end
