//
//  MultipleTextFieldCollectionViewCell.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 26.10.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import "MultipleTextFieldCollectionViewCell.h"
#import "UIColor+DesignGuidelines.h"
#import "PagesCellDelegate.h"
#import "DirectionViewModel.h"
#import "UIColor+CoreCustomColors.h"

static NSString *const kRowNameKey = @"row_name";
static NSString *const kRowTypeKey = @"row_type";
static NSString *const kRowKey = @"row_key";
static NSString *const kRowSize = @"row_size";
static NSString *const kRowPlaceholderKey = @"row_placeholder";

static NSString *const TFStringType = @"tf_string";
static NSString *const TFNumberType = @"tf_number";
static NSString *const TFSeriasNumberType = @"tf_seriasNumber";
static NSString *const TFNumberPassportType = @"tf_numberPassport";
static NSString *const TFSubvisionType = @"tf_subvision";

@interface MultipleTextFieldCollectionViewCell () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *view1;
@property (weak, nonatomic) IBOutlet UIView *view2;
@property (weak, nonatomic) IBOutlet UIView *view3;
@property (weak, nonatomic) IBOutlet UIView *view4;
@property (weak, nonatomic) IBOutlet UIView *view5;
@property (weak, nonatomic) IBOutlet UIView *view6;
@property (weak, nonatomic) IBOutlet UIView *viewError;

@property (weak, nonatomic) IBOutlet UILabel *labelName1;
@property (weak, nonatomic) IBOutlet UILabel *labelName2;
@property (weak, nonatomic) IBOutlet UILabel *labelName3;
@property (weak, nonatomic) IBOutlet UILabel *labelName4;
@property (weak, nonatomic) IBOutlet UILabel *labelName5;
@property (weak, nonatomic) IBOutlet UILabel *labelName6;

@property (weak, nonatomic) IBOutlet UITextField *textField1;
@property (weak, nonatomic) IBOutlet UITextField *textField2;
@property (weak, nonatomic) IBOutlet UITextField *textField3;
@property (weak, nonatomic) IBOutlet UITextField *textField4;
@property (weak, nonatomic) IBOutlet UITextField *textField5;
@property (weak, nonatomic) IBOutlet UITextField *textField6;

@property (weak, nonatomic) IBOutlet UIImageView *imageViewError;
@property (weak, nonatomic) IBOutlet UILabel *labelError;

@property (strong, nonatomic) NSString *key1;
@property (strong, nonatomic) NSString *key2;
@property (strong, nonatomic) NSString *key3;
@property (strong, nonatomic) NSString *key4;
@property (strong, nonatomic) NSString *key5;
@property (strong, nonatomic) NSString *key6;

@property (strong, nonatomic) NSString *keyError;

@property (strong, nonatomic) NSString *type1;
@property (strong, nonatomic) NSString *type2;
@property (strong, nonatomic) NSString *type3;
@property (strong, nonatomic) NSString *type4;
@property (strong, nonatomic) NSString *type5;
@property (strong, nonatomic) NSString *type6;

@property (strong, nonatomic) NSDictionary *dict;
@property (strong, nonatomic) NSNumberFormatter *formatter;
@property (strong, nonatomic) NSString *mask;
@property (assign, nonatomic) NSInteger maxLenght;
@property (assign, nonatomic) NSInteger minLenght;

@end

@implementation MultipleTextFieldCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.textField1.delegate = self;
    self.textField2.delegate = self;
    self.textField3.delegate = self;
    self.textField4.delegate = self;
    self.textField5.delegate = self;
    self.textField6.delegate = self;
    
    self.view1.clipsToBounds = YES;
    self.view1.backgroundColor = [UIColor coreDarkGreyBlueColor];
    
    self.view2.clipsToBounds = YES;
    self.view2.backgroundColor = [UIColor coreDarkGreyBlueColor];
    
    self.view3.clipsToBounds = YES;
    self.view3.backgroundColor = [UIColor coreDarkGreyBlueColor];
    
    self.view4.clipsToBounds = YES;
    self.view4.backgroundColor = [UIColor coreDarkGreyBlueColor];
    
    self.view5.clipsToBounds = YES;
    self.view5.backgroundColor = [UIColor coreDarkGreyBlueColor];
    
    self.view6.clipsToBounds = YES;
    self.view6.backgroundColor = [UIColor coreDarkGreyBlueColor];
    
    self.view1.layer.borderWidth = 0.0;
    self.view2.layer.borderWidth = 0.0;
    self.view3.layer.borderWidth = 0.0;
    self.view4.layer.borderWidth = 0.0;
    self.view5.layer.borderWidth = 0.0;
    self.view6.layer.borderWidth = 0.0;
    
    self.viewError.backgroundColor = [UIColor clearColor];
    self.labelError.hidden = YES;
    self.imageViewError.hidden = YES;
  
    // Initialization code
}

- (void)showError:(NSString *)error key:(NSString *)key;
{
    if ([key isEqualToString:self.key1]) {
        self.view1.layer.borderWidth = 1.0;
        self.view1.layer.borderColor = [UIColor coreOrangeYellowColor].CGColor;
    }
    else if ([key isEqualToString:self.key2])
    {
        self.view2.layer.borderWidth = 1.0;
        self.view2.layer.borderColor = [UIColor coreOrangeYellowColor].CGColor;
    }
    else if ([key isEqualToString:self.key3])
    {
        self.view3.layer.borderWidth = 1.0;
        self.view3.layer.borderColor = [UIColor coreOrangeYellowColor].CGColor;
    }
    else if ([key isEqualToString:self.key4])
    {
        self.view4.layer.borderWidth = 1.0;
        self.view4.layer.borderColor = [UIColor coreOrangeYellowColor].CGColor;
    }
    else if ([key isEqualToString:self.key5])
    {
        self.view5.layer.borderWidth = 1.0;
        self.view5.layer.borderColor = [UIColor coreOrangeYellowColor].CGColor;
    }
    else if ([key isEqualToString:self.key6])
    {
        self.view6.layer.borderWidth = 1.0;
        self.view6.layer.borderColor = [UIColor coreOrangeYellowColor].CGColor;
    }
    self.keyError = key;
    self.labelError.text = error;
    self.labelError.hidden = NO;
    self.imageViewError.hidden = NO;

}

- (void)hideError:(UITextField *)textField
{
    self.labelError.hidden = YES;
    self.imageViewError.hidden = YES;
    if (textField == self.textField1) {
        self.view1.layer.borderWidth = 0.0;
    }
    if (textField == self.textField2) {
        self.view2.layer.borderWidth = 0.0;
    }
    if (textField == self.textField3) {
        self.view3.layer.borderWidth = 0.0;
    }
    if (textField == self.textField4) {
        self.view4.layer.borderWidth = 0.0;
    }
    if (textField == self.textField5) {
        self.view5.layer.borderWidth = 0.0;
    }
    if (textField == self.textField6) {
        self.view6.layer.borderWidth = 0.0;
    }
}

-(void)configWithDict:(NSDictionary *)dict andViewModel:(id<DirectionViewModelType> )viewModel
{
    id<DirectionViewModelOutputs> outputs = viewModel.outputs;
    self.textField4.returnKeyType = self.isLastCell ? UIReturnKeyDone : UIReturnKeyNext;
    self.dict = dict;
    NSArray *names = dict[kRowNameKey];
    self.labelName1.text = names[0];
    self.labelName2.text = names[1];
    self.labelName3.text = names[2];
    self.labelName4.text = names[3];
    self.labelName5.text = names[4];
    self.labelName6.text = names[5];
    
    NSArray *keys = dict[kRowKey];
    self.key1 = keys[0];
    self.key2 = keys[1];
    self.key3 = keys[2];
    self.key4 = keys[3];
    self.key5 = keys[4];
    self.key6 = keys[5];
    
    self.textField1.text = [outputs textForKey:self.key1];
    self.textField2.text = [outputs textForKey:self.key2];
    self.textField3.text = [outputs textForKey:self.key3];
    self.textField4.text = [outputs textForKey:self.key4];
    self.textField5.text = [outputs textForKey:self.key5];
    self.textField6.text = [outputs textForKey:self.key6];
    
    NSArray *placeholders = dict[kRowPlaceholderKey];
    self.textField1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholders[0] attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHex:@"#5f6076" alpha:1.0]}];
    self.textField2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholders[1] attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHex:@"#5f6076" alpha:1.0]}];
    self.textField3.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholders[2] attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHex:@"#5f6076" alpha:1.0]}];
    self.textField4.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholders[3] attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHex:@"#5f6076" alpha:1.0]}];
    self.textField5.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholders[3] attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHex:@"#5f6076" alpha:1.0]}];
    self.textField6.attributedPlaceholder = [[NSAttributedString alloc] initWithString:placeholders[3] attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHex:@"#5f6076" alpha:1.0]}];
    
    [self setupTextFields];
    
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    id<PagesCellDelegate> delegate = self.delegate;
    if (textField == self.textField1) {
        self.labelName1.textColor = [UIColor coreOrangeYellowColor];
        [delegate cellTextFieldDidBeginEditing:textField withRowKey:self.key1];
    }
    else if (textField == self.textField2) {
        self.labelName2.textColor = [UIColor coreOrangeYellowColor];
        [delegate cellTextFieldDidBeginEditing:textField withRowKey:self.key2];
    }
    else if (textField == self.textField3) {
        self.labelName3.textColor = [UIColor coreOrangeYellowColor];
        [delegate cellTextFieldDidBeginEditing:textField withRowKey:self.key3];
    }
    else if (textField == self.textField4) {
        self.labelName4.textColor = [UIColor coreOrangeYellowColor];
        [delegate cellTextFieldDidBeginEditing:textField withRowKey:self.key4];
    }
    else if (textField == self.textField5) {
        self.labelName5.textColor = [UIColor coreOrangeYellowColor];
        [delegate cellTextFieldDidBeginEditing:textField withRowKey:self.key5];
    }
    else if (textField == self.textField6) {
        self.labelName6.textColor = [UIColor coreOrangeYellowColor];
        [delegate cellTextFieldDidBeginEditing:textField withRowKey:self.key6];
    }
    [self hideError:textField];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
     id<PagesCellDelegate> delegate = self.delegate;
    if (textField == self.textField1) {
        self.labelName1.textColor = [UIColor lightGrayColor];
        [delegate cellTextFieldDidEndEditing:textField withRowKey:self.key1];
    }
    else if (textField == self.textField2) {
        self.labelName2.textColor = [UIColor lightGrayColor];
        [delegate cellTextFieldDidEndEditing:textField withRowKey:self.key2];
    }
    else if (textField == self.textField3) {
        self.labelName3.textColor = [UIColor lightGrayColor];
        [delegate cellTextFieldDidEndEditing:textField withRowKey:self.key3];
    }
    else if (textField == self.textField4) {
        self.labelName4.textColor = [UIColor lightGrayColor];
        [delegate cellTextFieldDidEndEditing:textField withRowKey:self.key4];
    }
    else if (textField == self.textField5) {
        self.labelName5.textColor = [UIColor lightGrayColor];
        [delegate cellTextFieldDidEndEditing:textField withRowKey:self.key5];
    }
    else if (textField == self.textField6) {
        self.labelName6.textColor = [UIColor lightGrayColor];
        [delegate cellTextFieldDidEndEditing:textField withRowKey:self.key6];
    }
    [self hideError:textField];
    
}

- (void)setupTextFields
{
    self.textField1.keyboardType = UIKeyboardTypeNumberPad;
    self.textField2.keyboardType = UIKeyboardTypeDefault;
    self.textField3.keyboardType = UIKeyboardTypeDefault;
    self.textField4.keyboardType = UIKeyboardTypeDefault;
    self.textField5.keyboardType = UIKeyboardTypeDefault;
    self.textField6.keyboardType = UIKeyboardTypeDefault;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([textField isEqual:self.textField1]) {
        self.maxLenght = 6;
    }
    else {
        self.maxLenght = 10;
    }
    NSMutableString *text = [textField.text mutableCopy];
    range.length == 1 ? [text deleteCharactersInRange:range] : [text appendString:string];
    BOOL check = YES;
    
    
    if (_maxLenght > 0 && textField.text.length + 1 > _maxLenght && range.length != 1 && textField != self.textField3 && textField != self.textField2)
    {
        return NO;
    }
    
    if (check)
    {
        check = [self updateToCurrentFromatterText:textField.text withString:string range:range textField:textField];
        
        if (self.formatter != nil && text.length > 0)
        {
            text = [[text stringByReplacingOccurrencesOfString:self.formatter.groupingSeparator withString:@""]  mutableCopy];
            textField.text = [self.formatter stringFromNumber:@([text doubleValue])];
            return NO;
        }
    }
    if (_maxLenght > 0 && text.length == _maxLenght && range.length != 1 && textField != self.textField3 && textField != self.textField2) {
        [self performSelector:@selector(textFieldShouldReturn:) withObject:textField afterDelay:0.2];
        return check;
    }
    return check;
}

- (BOOL) updateToCurrentFromatterText:(NSString *)text withString:(NSString *)string range:(NSRange)range textField:(UITextField *)textField
{
    if (self.mask == nil)
    {
        return YES;
    }
    
    NSString *separator = [self.mask stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if (separator.length > 1) {
        separator = [separator substringToIndex:1];;
    }
    
    if (range.length == 0)
    {
        NSString *str = [NSString stringWithFormat:@"%@%@",text,string];
        NSString *mask = [self.mask substringFromIndex:str.length];
        if (separator.length == 0)
        {
            return YES;
        }
        
        if ([mask rangeOfString:separator].location == 0 && [mask rangeOfString:separator].length == 1)
        {
            NSLog(@"%@ Append separator",NSStringFromClass(self.class));
            textField.text = [str stringByAppendingString:separator];
            return NO;
        }
    }
    
    if (range.length == 1)
    {
        if (separator.length == 0)
        {
            return YES;
        }
        
        NSString *string = [text substringWithRange:range];
        
        if ([string isEqualToString:separator])
        {
            NSLog(@"%@ Deleted separator",NSStringFromClass(self.class));
            textField.text = [text substringToIndex:range.location -1];
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    id<PagesCellDelegate> delegate = self.delegate;
    if (textField == self.textField1) {
        [self.textField2 becomeFirstResponder];
    }
    else if (textField == self.textField2) {
        [self.textField3 becomeFirstResponder];
    }
    else if (textField == self.textField3) {
        [self.textField4 becomeFirstResponder];
    }
    else if (textField == self.textField4) {
        [self.textField5 becomeFirstResponder];
    }
    else if (textField == self.textField5) {
        [self.textField6 becomeFirstResponder];
    }
    else if (textField == self.textField6) {
        if (self.isLastCell) {
           [delegate cellTextFieldDoneAction:self.dict];
        }
        else {
            if ([delegate respondsToSelector:@selector(cellTextFieldNextAction:)]) {
                [delegate cellTextFieldNextAction:self.dict];
            }
        }
    }
    return YES;
}

-(BOOL)becomeFirstResponder
{
    [self.textField1 becomeFirstResponder];
    return [super becomeFirstResponder];
}


@end
