//
//  TextFieldCollectionViewCell.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 18.10.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PagesCellDelegate;

@interface TextFieldCollectionViewCell : UICollectionViewCell

@property (assign, nonatomic) BOOL isLastCell;
@property (weak, nonatomic) id <PagesCellDelegate> delegate;
@property (strong, nonatomic) NSString *value;
@property (assign, nonatomic) BOOL showMessageError;
- (void)configWithDict:(NSDictionary *)dict;
- (void)showError:(NSString *)error;
- (void)hideError;
@end
