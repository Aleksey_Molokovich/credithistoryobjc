//
//  PagesCellDelegate.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 13.09.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>


@protocol PagesCellDelegate <NSObject>



@optional

- (void)cellTextFieldDidBeginEditing:(UITextField *)textField withRowKey:(NSString *)kRowKey;
- (void)cellTextFieldDidEndEditing:(UITextField *)textField withRowKey:(NSString *)kRowKey;
- (void)cellTextFieldNextAction:(NSDictionary *)rowDict;
- (void)cellTextFieldDoneAction:(NSDictionary *)rowDict;
- (void)showTermOfUse;
- (void)selectedIndexChanged:(NSInteger)index;
@end
