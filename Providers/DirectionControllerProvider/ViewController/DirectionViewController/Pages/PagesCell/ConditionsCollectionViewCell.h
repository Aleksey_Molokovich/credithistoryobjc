//
//  ConditionsCollectionViewCell.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 07.02.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DirectionViewModel.h"
#import "PagesCellDelegate.h"

@interface ConditionsCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) id <PagesCellDelegate> delegate;

- (void)configWithViewModel:(id <DirectionViewModelType>)viewModel;
- (void)showError:(NSString *)error;

@end
