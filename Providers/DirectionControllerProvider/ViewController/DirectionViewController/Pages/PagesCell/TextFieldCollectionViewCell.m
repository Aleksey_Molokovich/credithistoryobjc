//
//  TextFieldCollectionViewCell.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 18.10.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import "TextFieldCollectionViewCell.h"
#import "PagesCellDelegate.h"
#import "UIColor+CoreCustomColors.h"
#import "UIColor+DesignGuidelines.h"

static NSString *const kRowNameKey = @"row_name";
static NSString *const kRowTypeKey = @"row_type";
static NSString *const kRowKey = @"row_key";
static NSString *const kRowSize = @"row_size";
static NSString *const kRowIdKey = @"row_idkey";
static NSString *const kRowCell = @"row_cell";
static NSString *const kRowPlaceholderKey = @"row_placeholder";

static NSString *const TFStringType = @"tf_string";
static NSString *const TFNumberType = @"tf_number";
static NSString *const TFDateType = @"tf_date";
static NSString *const TFUnlimitadeDateType = @"tf_unlimitade_date";
static NSString *const TFSeriasNumberType = @"tf_seriasNumber";
static NSString *const TFNumberPassportType = @"tf_numberPassport";
static NSString *const TFSubvisionType = @"tf_subvision";
static NSString *const TFMoneyType = @"tf_money";
static NSString *const TFDayType = @"tf_day";
static NSString *const TFSNILSType = @"tf_snils";

@interface TextFieldCollectionViewCell () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIView *viewMain;
@property (weak, nonatomic) IBOutlet UIView *viewError;
@property (weak, nonatomic) IBOutlet UILabel *labelError;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewError;

@property (strong, nonatomic) NSDictionary *dict;
@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSNumberFormatter *formatter;
@property (strong, nonatomic) NSString *mask;
@property (assign, nonatomic) NSInteger maxLenght;
@property (assign, nonatomic) NSInteger minLenght;
@property (strong, nonatomic) NSString *placeholder;
@end

@implementation TextFieldCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.textField.delegate = self;
    self.backgroundColor = [UIColor clearColor];
    self.viewMain.clipsToBounds = YES;
    self.viewMain.backgroundColor = [UIColor coreDarkGreyBlueColor];
    self.viewError.backgroundColor = [UIColor clearColor];
    self.showMessageError = YES;
    [self hideError];
    // Initialization code
}

- (void)configWithDict:(NSDictionary *)dict
{
    self.dict = dict;
    self.labelName.text = dict[kRowNameKey];
    self.type = dict[kRowTypeKey];
    self.key = dict[kRowKey];
    self.maxLenght = -1;
    self.formatter = nil;
    self.mask = nil;
    self.placeholder = dict[kRowPlaceholderKey];
    if (self.placeholder) {
        self.textField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.placeholder attributes:@{NSForegroundColorAttributeName: [UIColor colorWithHex:@"#5f6076" alpha:1.0]}];
    }
    self.textField.returnKeyType = self.isLastCell ? UIReturnKeyDone : UIReturnKeyNext;
    [self setupTextFieldWithType:self.type];
}

-(void)setValue:(NSString *)value
{
    self.textField.text = value;
}

- (void)showError:(NSString *)error
{
    if (self.showMessageError) {
        self.labelError.text = error;
        self.labelError.hidden = NO;
        self.imageViewError.hidden = NO;
    }
    self.viewMain.layer.borderWidth = 1.0;
    self.viewMain.layer.borderColor = [UIColor coreOrangeYellowColor].CGColor;
}

- (void)hideError
{
    self.labelError.hidden = YES;
    self.imageViewError.hidden = YES;
    self.viewMain.layer.borderWidth = 0.0;
}

- (BOOL)becomeFirstResponder
{
    [self.textField becomeFirstResponder];
    return [super becomeFirstResponder];
}

- (void)setupTextFieldWithType:(NSString *)type
{
    if ([type isEqualToString:TFStringType]) {
        self.textField.keyboardType = UIKeyboardTypeDefault;
        self.minLenght = 3;
    }
    else if ([type isEqualToString:TFNumberType])
    {
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
        self.maxLenght = 10;
    }
    else if ([type isEqualToString:TFDateType])
    {
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
        self.minLenght = 10;
        self.maxLenght = 10;
        self.mask = @"##.##.####";
    }
    else if ([type isEqualToString:TFDayType])
    {
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
        self.minLenght = 1;
        self.maxLenght = 2;
    }
    else if ([type isEqualToString:TFSeriasNumberType])
    {
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
        self.maxLenght = 4;
        self.minLenght = 4;
    }
    else if ([type isEqualToString:TFNumberPassportType])
    {
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
        self.maxLenght = 6;
        self.minLenght = 6;
    }
    else if ([type isEqualToString:TFSubvisionType])
    {
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
        self.maxLenght = 7;
        self.minLenght = 7;
        self.mask = @"###-###";
    }
    else if ([type isEqualToString:TFMoneyType])
    {
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
        self.maxLenght = 10;
    }
    else if ([type isEqualToString:TFUnlimitadeDateType])
    {
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
        self.minLenght = 10;
        self.maxLenght = 10;
        self.mask = @"##.##.####";
    }
    else if ([type isEqualToString:TFSNILSType])
    {
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
        self.minLenght = 14;
        self.maxLenght = 14;
        self.mask = @"###-###-### ##";
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([self.textField.text isEqualToString:@"0"]) {
        self.textField.text = @"";
    }
    self.labelName.textColor = [UIColor coreOrangeYellowColor];
    [self hideError];
    [self.delegate cellTextFieldDidBeginEditing:textField withRowKey:self.key];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self hideError];
    self.labelName.textColor = [UIColor lightGrayColor];
    [self.delegate cellTextFieldDidEndEditing:textField withRowKey:self.key];
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSMutableString *text = [textField.text mutableCopy];
    range.length == 1 ? [text deleteCharactersInRange:range] : [text appendString:string];
    BOOL check = YES;
    
    
    if (_maxLenght > 0 && textField.text.length + 1 > _maxLenght && range.length != 1)
    {
        return NO;
    }
    
    if (self.type == TFDateType || self.type == TFUnlimitadeDateType) {
        check = [self validateDateText:textField.text withString:string range:range];
    }
    if (self.type == TFDayType) {
        check = [self validateDayText:textField.text withString:string range:range];
    }
    
    if (check)
    {
        check = [self updateToCurrentFromatterText:textField.text withString:string range:range];
        
        if (self.formatter != nil && text.length > 0)
        {
            text = [[text stringByReplacingOccurrencesOfString:self.formatter.groupingSeparator withString:@""]  mutableCopy];
            self.textField.text = [self.formatter stringFromNumber:@([text doubleValue])];
            return NO;
        }
    }
    if (_maxLenght > 0 && text.length == _maxLenght && range.length != 1) {
        [self performSelector:@selector(textFieldShouldReturn:) withObject:textField afterDelay:0.2];
        return check;
    }
    return check;
}

- (BOOL) updateToCurrentFromatterText:(NSString *)text withString:(NSString *)string range:(NSRange)range
{
    if (self.mask == nil)
    {
        return YES;
    }
    
    NSString *separator = [self.mask stringByReplacingOccurrencesOfString:@"#" withString:@""];
    if (separator.length > 1) {
        double indexD = (double)text.length/(double)separator.length;
        NSInteger index = (NSInteger)round(indexD) == 0 ? (NSInteger)round(indexD) : (NSInteger)round(indexD) - 1;
        if (index >= separator.length) {
            index = separator.length - 1;
        }
        NSRange myRange = NSMakeRange(index, 1);
        separator = [separator substringWithRange:myRange];
}
    
    if (range.length == 0)
    {
        NSString *str = [NSString stringWithFormat:@"%@%@",text,string];
        NSString *mask = [self.mask substringFromIndex:str.length];
        if (separator.length == 0)
        {
            return YES;
        }
        
        if ([mask rangeOfString:separator].location == 0 && [mask rangeOfString:separator].length == 1)
        {
            self.textField.text = [str stringByAppendingString:separator];
            return NO;
        }
    }
    
    if (range.length == 1)
    {
        if (separator.length == 0)
        {
            return YES;
        }
        
        NSString *string = [text substringWithRange:range];
        
        if ([string isEqualToString:separator])
        {
            self.textField.text = [text substringToIndex:range.location -1];
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
#pragma unused(textField)
    [self.textField resignFirstResponder];
    id<PagesCellDelegate> delegate = self.delegate;
    if (self.isLastCell) {
        if ([delegate respondsToSelector:@selector(cellTextFieldDoneAction:)]) {
            [delegate cellTextFieldDoneAction:self.dict];
        }
     }
    else {
        if ([delegate respondsToSelector:@selector(cellTextFieldNextAction:)]) {
        [delegate cellTextFieldNextAction:self.dict];
        }
    }
    return YES;
}

#pragma mark - Helpers Methods

- (BOOL) validateDateText:(NSString *)text withString:(NSString *)string range:(NSRange)range
{
    if ([string isEqualToString:@""]) {
        return YES;
    }
    NSInteger value = [string integerValue];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *yearString = [formatter stringFromDate:[NSDate date]];
    switch (range.location) {
        case 0:
            if (value > 3) return NO;
            break;
        case 1:
        {
            if ([self value:text atIndex:0] == 3) {
                if (value > 1) return NO;
            }
            if ([self value:text atIndex:0] == 0) {
                if (value < 1) return NO;
            }
        }
            break;
        case 3:
            if (value > 1) return NO;
            break;
        case 4:
        {
            if ([self value:text atIndex:3] == 1) {
                if (value > 2) return NO;
            }
            if ([self value:text atIndex:3] == 0) {
                if (value < 1) return NO;
            }
        }
            break;
        case 6:
        {
            if (value < 1 || value > [self value:yearString atIndex:0]) {
                return NO;
            }
        }
            break;
        case 7:
        {
            if ([self value:text atIndex:6] == [self value:yearString atIndex:0]) {
                if (value > [self value:yearString atIndex:1]) {
                    return NO;
                }
            }
            if ([self value:text atIndex:6] == 1) {
                if (value < 9) {
                    return NO;
                }
            }
        }
            break;
        case 8:
        {
            if ([self.type isEqualToString:TFUnlimitadeDateType]) {
                return YES;
            }
            if ([self value:text atIndex:7] == [self value:yearString atIndex:1]) {
                if (value > [self value:yearString atIndex:2]) {
                    return NO;
                }
            }
        }
            break;
        case 9:
        {
            if ([self.type isEqualToString:TFUnlimitadeDateType]) {
                return YES;
            }
            if ([self value:text atIndex:8] == [self value:yearString atIndex:2]) {
                if (value > [self value:yearString atIndex:3]) {
                    return NO;
                }
            }
        }
            break;
        default:
            return YES;
    }
    return YES;
}

- (BOOL) validateDayText:(NSString *)text withString:(NSString *)string range:(NSRange)range
{
    if ([string isEqualToString:@""]) {
        return YES;
    }
    NSInteger value = [string integerValue];
    switch (range.location) {
        case 0:
            if (value < 1) return NO;
            break;
        case 1:
        {
            if ([self value:text atIndex:0] == 3) {
                if (value > 1) return NO;
            }
            if ([self value:text atIndex:0] > 3) {
                return NO;
            }
        }
            break;
        default:
            return YES;
    }
    return YES;
}

- (NSInteger)value:(NSString *)text atIndex:(NSInteger)index
{
    return [[text substringWithRange:NSMakeRange(index, 1)] integerValue];
}

@end
