//
//  SelectedCollectionViewCell.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 09.02.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "SelectedCollectionViewCell.h"
#import "UIColor+DesignGuidelines.h"


@interface SelectedCollectionViewCell ()

@property (weak, nonatomic) IBOutlet UIButton *firstSegmentButton;
@property (weak, nonatomic) IBOutlet UIButton *secondSegmentButton;

@property (strong, nonatomic) NSArray *dataSource;

@end


@implementation SelectedCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.firstSegmentButton setBackgroundColor:[UIColor coreDarkGreyBlueColor] forState:UIControlStateNormal];
    [self.firstSegmentButton setBackgroundColor:[UIColor clearColor] forState:UIControlStateSelected];
    [self.firstSegmentButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.firstSegmentButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    
    [self.secondSegmentButton setBackgroundColor:[UIColor coreDarkGreyBlueColor] forState:UIControlStateNormal];
    [self.secondSegmentButton setBackgroundColor:[UIColor clearColor] forState:UIControlStateSelected];
    [self.secondSegmentButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.secondSegmentButton setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    
    self.firstSegmentButton.selected = YES;
    self.secondSegmentButton.selected = NO;
    
    self.firstSegmentButton.tag = 0;
    self.secondSegmentButton.tag = 1;
    // Initialization code
}
- (IBAction)selectedChange:(UIButton *)sender {
    if (sender.selected) {
        return;
    }
    [self selectedSegmentChangedIndex:sender.tag];
}

- (void)configWithViewModel:(id <DirectionViewModelType>)viewModel
{
    id <DirectionViewModelType> vm = viewModel;
    id <DirectionViewModelOutputs> outputs = vm.outputs;
    [self selectedSegmentChangedIndex:outputs.selectedSegmentIndex];
    [self.firstSegmentButton setTitle:outputs.selectedSegmentDataSource[0] forState:UIControlStateNormal];
    [self.secondSegmentButton setTitle:outputs.selectedSegmentDataSource[1] forState:UIControlStateNormal];
}

- (void)selectedSegmentChangedIndex:(NSInteger)index
{
    [self.delegate selectedIndexChanged:index];
    switch (index) {
        case 0:
        {
            self.firstSegmentButton.selected = YES;
            self.secondSegmentButton.selected = NO;
            [self setupSelectedButton:self.firstSegmentButton];
            [self setupDeselectedButton:self.secondSegmentButton];
        }
            break;
        case 1:
        {
            self.firstSegmentButton.selected = NO;
            self.secondSegmentButton.selected = YES;
            [self setupSelectedButton:self.secondSegmentButton];
            [self setupDeselectedButton:self.firstSegmentButton];
        }
            break;
        default:
            break;
    }
}

- (void)setupSelectedButton:(UIButton *)button
{
    button.layer.borderWidth = 2.0;
    button.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)setupDeselectedButton:(UIButton *)button
{
    button.layer.borderWidth = 0.0;
}

@end
