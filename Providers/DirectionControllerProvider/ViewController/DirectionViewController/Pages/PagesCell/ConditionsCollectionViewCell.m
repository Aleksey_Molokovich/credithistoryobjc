//
//  ConditionsCollectionViewCell.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 07.02.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "ConditionsCollectionViewCell.h"

#import "UIColor+DesignGuidelines.h"

static NSString *const kRowKey = @"row_key";

@interface ConditionsCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIButton *actualDataCheckedButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmConditionsCheckedButton;
@property (weak, nonatomic) id <DirectionViewModelOutputs> outputs;
@end

@implementation ConditionsCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    UIImage *checkedImage = [UIImage imageNamed:@"ic_check_white"];
    [self.actualDataCheckedButton setImage:checkedImage forState:UIControlStateSelected];
    [self.actualDataCheckedButton setImage:nil forState:UIControlStateNormal];
    self.actualDataCheckedButton.selected = NO;
    self.actualDataCheckedButton.clipsToBounds = YES;
    
    [self.confirmConditionsCheckedButton setImage:checkedImage forState:UIControlStateSelected];
    [self.confirmConditionsCheckedButton setImage:nil forState:UIControlStateNormal];
    self.confirmConditionsCheckedButton.selected = NO;
    self.confirmConditionsCheckedButton.clipsToBounds = YES;
}

- (void)configWithViewModel:(id <DirectionViewModelType>)viewModel
{
    self.outputs = viewModel.outputs;
    id <DirectionViewModelOutputs> outputs = self.outputs;
    self.actualDataCheckedButton.selected = outputs.actualData;
    self.confirmConditionsCheckedButton.selected = outputs.confirmConditions ;
}

- (IBAction)checkedButtonAction:(UIButton *)sender {
    [self hideError:sender];
    sender.selected = !sender.selected;
    id <DirectionViewModelOutputs> outputs = self.outputs;
    if ([sender isEqual:self.actualDataCheckedButton]) {
        outputs.actualData = !outputs.actualData;
    }
    if ([sender isEqual:self.confirmConditionsCheckedButton]) {
        outputs.confirmConditions = !outputs.confirmConditions;
    }
}
- (IBAction)termsOfUseAction:(id)sender {
    #pragma unused(sender)
    [self.delegate showTermOfUse];
}

-(void)showError:(NSString *)error
{
    #pragma unused(error)
    id <DirectionViewModelOutputs> outputs = self.outputs;
    if (!outputs.actualData) {
        self.actualDataCheckedButton.layer.borderWidth = 1.0;
        self.actualDataCheckedButton.layer.borderColor = [UIColor coreOrangeYellowColor].CGColor;
    }
    if (!outputs.confirmConditions) {
        self.confirmConditionsCheckedButton.layer.borderWidth = 1.0;
        self.confirmConditionsCheckedButton.layer.borderColor = [UIColor coreOrangeYellowColor].CGColor;
    }
}

-(void)hideError:(UIButton *)button
{
    button.layer.borderWidth = 0.0;
}

@end
