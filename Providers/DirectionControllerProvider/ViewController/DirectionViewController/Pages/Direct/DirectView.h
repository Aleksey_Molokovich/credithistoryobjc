//
//  DirectView.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 13.09.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DirectView : UIControl

- (instancetype) initWithViewModel:(id)viewModel;

@end
