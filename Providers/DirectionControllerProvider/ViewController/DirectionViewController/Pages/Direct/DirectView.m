//
//  DirectView.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 13.09.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "DirectView.h"
#import "DirectionViewModel.h"

#import "PagesHeaderView.h"
#import "PagesCellDelegate.h"
#import "TextFieldCollectionViewCell.h"
#import "SelectedCollectionViewCell.h"

static NSString *const kDirectCollectionViewTFCellID = @"TextFieldCollectionViewCell";
static NSString *const kDirectCollectionViewSelectedCellID = @"selectedCellId";
static NSString *const kDirectCollectionViewHeaderID = @"headerId";


static UIEdgeInsets const kDirectCollectionViewInsets = {26.0f, 20.0f, 0.0f, 20.0f};
static CGFloat const kDirectCollectionViewMinimumLineSpacing = 0.0f;
static CGFloat const kDirectCollectionViewInteritemSpacing = 8.0f;
static CGFloat const kDirectCollectionViewTFCellHeight = 83.0f;

@interface DirectView()<UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, PagesCellDelegate>

@property (weak, nonatomic) id<DirectionViewModelType> viewModel;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) UITextField *textField;
@property (strong, nonatomic) NSDictionary *row;
@property (assign, nonatomic) BOOL isShowCell;
@end


@implementation DirectView

#pragma mark - init methods

- (instancetype) initWithViewModel:(DirectionViewModel *)viewModel
{
    self = [[NSBundle mainBundle] loadNibNamed:@"DirectView" owner:nil options:nil].firstObject;
    if ( self == nil)
    {
        return nil;
    }
    self.viewModel = viewModel;
    @weakify(self);
    [viewModel.outputs.signalInvalideDataInfo subscribeNext:^(id x) {
        @strongify(self);
#pragma unused(x)
        [self setupErrors];
    }];
    return self;
}

- (void) awakeFromNib
{
    [super awakeFromNib];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    
    UINib *nibTF = [UINib nibWithNibName:@"TextFieldCollectionViewCell" bundle:[NSBundle mainBundle]];
    UINib *nibSelected = [UINib nibWithNibName:@"SelectedCollectionViewCell" bundle:[NSBundle mainBundle]];
    UINib *nibHeader = [UINib nibWithNibName:@"PagesHeaderView" bundle:nil];
    [self.collectionView registerNib:nibHeader forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kDirectCollectionViewHeaderID];
    [self.collectionView registerNib:nibTF forCellWithReuseIdentifier:kDirectCollectionViewTFCellID];
    [self.collectionView registerNib:nibSelected forCellWithReuseIdentifier:kDirectCollectionViewSelectedCellID];
}

-(void)dealloc
{
    NSLog(@"%@ controller deallocated",NSStringFromClass(self.class));
}

- (void)viewWillLayoutSubviews {
    [self.collectionView.collectionViewLayout invalidateLayout];
}

- (void) didMoveToSuperview
{
    if (!self.superview)
    {
        return;
    }
    [self.collectionView reloadData];
}

#pragma mark - UICollectionView delegate

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
#pragma unused(collectionView, indexPath)

    [self.textField resignFirstResponder];
}

#pragma mark - UICollectionView data source

- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    id<DirectionViewModelType> viewModel = self.viewModel;
    id<DirectionViewModelOutputs> outputs = viewModel.outputs;
    UICollectionViewCell *cell = nil;
    NSDictionary *section = outputs.dataSource[indexPath.section];
    NSDictionary *row = [section[kDirectionSectionRowsKey] objectAtIndex:indexPath.row];
    if ([row[kDirectionRowCell] isEqualToString:kDirectionCellTF])
    {
        TextFieldCollectionViewCell *cellTF = [collectionView dequeueReusableCellWithReuseIdentifier:kDirectCollectionViewTFCellID forIndexPath:indexPath];
        
        if (indexPath.row == [section[kDirectionSectionRowsKey] count] - 1) {
            cellTF.isLastCell = YES;
        }
        
        cellTF.delegate = self;
        [cellTF configWithDict:row];
        cellTF.value = [outputs textForKey:row[kDirectionRowKey]];
        cell = cellTF;
    }
    if ([row[kDirectionRowCell] isEqualToString:kDirectionCellSelected])
    {
        SelectedCollectionViewCell *cellSelected = [collectionView dequeueReusableCellWithReuseIdentifier:kDirectCollectionViewSelectedCellID forIndexPath:indexPath];
        cellSelected.delegate = self;
        [cellSelected configWithViewModel:viewModel];
        cell = cellSelected;
    }
    return cell;
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
#pragma unused(collectionView)
     return self.viewModel.outputs.dataSource.count;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
#pragma unused(collectionView)
    NSDictionary *sectionDict = self.viewModel.outputs.dataSource[section];
    NSDictionary *rows = sectionDict[kDirectionSectionRowsKey];
    return rows.count;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableView = nil;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        NSDictionary *section = self.viewModel.outputs.dataSource[indexPath.section];
        PagesHeaderView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:kDirectCollectionViewHeaderID forIndexPath:indexPath];
        [headerView configWithName:section[kDirectionSectionNameKey]];
        reusableView = headerView;
    }
    return reusableView;
}

#pragma mark - UICollectionViewDelegateFlowLayout methods

- (CGSize) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
#pragma unused(collectionView, collectionViewLayout)
    CGSize size;
    NSDictionary *section = self.viewModel.outputs.dataSource[indexPath.section];
    NSDictionary *row = [section[kDirectionSectionRowsKey] objectAtIndex:indexPath.row];
    if ([row[kDirectionRowSize] isEqualToString:kDirectionFullSize]) {
        size = CGSizeMake(CGRectGetWidth(self.collectionView.bounds) - (kDirectCollectionViewInsets.left + kDirectCollectionViewInsets.right), kDirectCollectionViewTFCellHeight);
    }
    else if ([row[kDirectionRowSize] isEqualToString:kDirectionHalfSize])
    {
        size = CGSizeMake(CGRectGetWidth(self.collectionView.bounds) / 2 - kDirectCollectionViewInsets.right - kDirectCollectionViewInteritemSpacing/2, kDirectCollectionViewTFCellHeight);
    }
    return size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
#pragma unused(collectionView, collectionViewLayout, section)
    return kDirectCollectionViewInsets;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
#pragma unused(collectionView, collectionViewLayout, section)
    return kDirectCollectionViewMinimumLineSpacing;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
#pragma unused(collectionView, collectionViewLayout, section)
    return kDirectCollectionViewInteritemSpacing;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section
{
#pragma unused(collectionView, collectionViewLayout, section)
    CGSize size = CGSizeMake(CGRectGetWidth(self.collectionView.bounds), 35.0f);
    return size;
}


#pragma mark - utilites methods

- (void)setupErrors
{
    id<DirectionViewModelType> viewModel = self.viewModel;
    id<DirectionViewModelOutputs> outputs = viewModel.outputs;
    for (NSDictionary *row in outputs.invalideInfoRows)
    {
        NSIndexPath *indexPath = [outputs indexPathAtRow:row];
        UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
        [((TextFieldCollectionViewCell *)cell) showError:row[kDirectionRowError]];
    }
}

-(void)showKeyboardCell
{
    NSIndexPath *indexPath = [self.viewModel.outputs indexPathAtRow:self.row];
    UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    [((TextFieldCollectionViewCell *)cell) becomeFirstResponder];
}

#pragma mark - UIScrollView delegate

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView
{
#pragma unused(scrollView)
    if (self.isShowCell) {
        [self showKeyboardCell];
        self.isShowCell = NO;
    }
}

#pragma mark - pages cell delegate

- (void)cellTextFieldDidBeginEditing:(UITextField *)textField withRowKey:(NSString *)kRowKey
{
#pragma unused(kRowKey)
    self.textField = textField;
}

- (void)cellTextFieldDidEndEditing:(UITextField *)textField withRowKey:(NSString *)kRowKey
{
    [self.viewModel.inputs textDidEndEditing:textField.text key:kRowKey];
}

- (void)cellTextFieldNextAction:(NSDictionary *)rowDict
{
    id<DirectionViewModelType> viewModel = self.viewModel;
    id<DirectionViewModelOutputs> outputs = viewModel.outputs;
    
    self.row = [outputs nextRow:rowDict];
    NSIndexPath *indexPath = [outputs indexPathAtRow:self.row];
    UICollectionViewCell *cell = [self.collectionView cellForItemAtIndexPath:indexPath];
    if (cell == nil)
    {
        self.isShowCell = YES;
        [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
    }
    else
    {
        [self showKeyboardCell];
    }
    
}

- (void)cellTextFieldDoneAction:(NSDictionary *)rowDict
{
#pragma unused(rowDict)
    [self.viewModel.inputs actionContinue];
}

- (void)selectedIndexChanged:(NSInteger)index
{
    id <DirectionViewModelType> viewModel = self.viewModel;
    id <DirectionViewModelInputs> inputs = viewModel.inputs;
    [inputs selectedIndexChanged:index];
}


@end
