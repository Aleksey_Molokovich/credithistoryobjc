//
//  PagesHeaderView.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 13.09.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PagesHeaderView.h"

#import "UIColor+DesignGuidelines.h"

@interface PagesHeaderView()

@property (weak, nonatomic) IBOutlet UILabel *name;

@end


@implementation PagesHeaderView

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.backgroundColor = [UIColor coreDarkGreyBlueColor];
}

-(void)configWithName:(NSString *)name
{
    self.name.text = name;
}
@end
