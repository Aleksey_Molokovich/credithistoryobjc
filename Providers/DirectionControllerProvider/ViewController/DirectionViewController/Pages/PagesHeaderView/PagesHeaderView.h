//
//  PagesHeaderView.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 13.09.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PagesHeaderView : UICollectionReusableView

-(void)configWithName:(NSString *)name;

@end
