//
//  RegistrationViewController.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 04.10.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DirectionViewModel;

@interface DirectionViewController : UIViewController

- (instancetype) initWithViewModel:(DirectionViewModel *)viewModel;

@end
