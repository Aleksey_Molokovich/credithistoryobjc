//
//  DirectionCollectionViewFlowLayout.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 11.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface DirectionCollectionViewFlowLayout : UICollectionViewFlowLayout

-(void)configure;

@end
