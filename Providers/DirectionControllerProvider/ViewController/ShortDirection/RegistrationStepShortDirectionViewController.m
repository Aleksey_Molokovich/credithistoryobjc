//
//  RegistrationStepShortDirectionViewController.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 26.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "RegistrationStepShortDirectionViewController.h"
#import "RegistrationStepShortDirectionViewModel.h"

#import "CoreNavigationController.h"

#import "ShortDirectionCollectionViewFlowLayout.h"

#import "CollectionViewCellsDefinitions.h"
#import "BaseCollectionViewCell.h"
#import "BlueButtonCollectionViewCell.h"
#import "ButtonCollectionViewCellOutput.h"
#import "LabelCollectionViewCellOutput.h"
#import "TextFieldCollectionViewOutput.h"
#import "CellManager.h"

#import "TCoreColorList.h"
#import "TCoreCircleLoadActivity.h"
#import "TCoreWebViewController.h"

#import "NSLayoutConstraint+CoreUtils.h"
#import "NSObject+CoreUtils.h"
#import "UIVIew+CoreAnimations.h"

@interface RegistrationStepShortDirectionViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, ButtonCollectionViewCellOutput, LabelCollectionViewCellOutput, TextFieldCollectionViewOutput>

@property (strong, nonatomic) RegistrationStepShortDirectionViewModel *viewModel;

@property (strong, nonatomic) ShortDirectionCollectionViewFlowLayout *collectionViewLayout;

@property (nonatomic, strong) TCoreCircleLoadActivity *circleLoadActivity;

@property (strong, nonatomic) UITextField *activeTextField;

#pragma mark - IBOutlets

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewBottomConstraint;

@end

@implementation RegistrationStepShortDirectionViewController

- (instancetype)initWithViewModel:(RegistrationStepShortDirectionViewModel *)viewModel
{
    if ( (self = [super init]) == nil)
    {
        return nil;
    }
    self.viewModel = viewModel;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShownNotification:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillBeHiddenNotification:) name:UIKeyboardWillHideNotification object:nil];
    
    return self;
}

- (void)dealloc
{
    [self.circleLoadActivity removeFromSuperview];
    self.circleLoadActivity = nil;
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"%@ deallocated",NSStringFromClass(self.class));
}

#pragma mark - View Controller Lyfecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureViewController];
    [self bindViewModel];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setToolbarHidden:YES];
}

-(void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];
    if ([self iPhonePlusDeviceAndiOS10]) [self.collectionView.collectionViewLayout invalidateLayout];
}

-(BOOL)iPhonePlusDeviceAndiOS10
{
    if (UI_USER_INTERFACE_IDIOM() != UIUserInterfaceIdiomPhone) return NO;
    if (!(SYSTEM_VERSION_LESS_THAN(@"11.0") && SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0"))) return NO;
    if ([UIScreen mainScreen].scale > 2.9) return YES;   // Scale is only 3 when not in scaled mode for iPhone 6 Plus
    return NO;
}
#pragma mark - UICollectionView Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    #pragma unused(collectionView)
    return self.viewModel.datasource.countSections;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    #pragma unused(collectionView)
    return [self.viewModel.datasource countCellInSection:section];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BaseCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[self.viewModel.datasource cellIdAtIndexPath:indexPath] forIndexPath:indexPath];
    
    cell.delegate = self;
    NSString *key = [_viewModel.datasource cellConfigAtIndexPath:indexPath].key;
    
    [cell configureCell:[self.viewModel.datasource cellConfigAtIndexPath:indexPath]];
    [cell fillWithData:[self.viewModel dataForKey:key]];
    
    if ([cell isKindOfClass:[BlueButtonCollectionViewCell class]])
    {
        [(BlueButtonCollectionViewCell *)cell button].rac_command = self.viewModel.commandSendDirection;
    }
    
    return cell;
}

#pragma mark - UICollectionView Delegate Flow Layout

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    #pragma unused(collectionView, collectionViewLayout, section)
    return 0.f;
}

#pragma mark - ButtonCollectionViewCellOutput

//- (void)pressButton
//{
//    [self.viewModel sendDirection];
//}

#pragma mark - LabelCollectionViewCellOutput

- (void)didPressText
{
    [self.viewModel showTerms];
}

#pragma mark - TextFieldCollectionViewOutput

- (void)cellTextDidChange:(NSString *)text withRowKey:(NSString *)kRowKey
{
//    NSLog(@"%@ cellTextDidChange: %@ key: %@", [self class], text, kRowKey);
    [self.viewModel setValue:text forDirectionKey:kRowKey];
}

- (void)cellTextFieldDidBeginEditing:(UITextField *)textField withRowKey:(NSString *)kRowKey
{
    #pragma unused(kRowKey)
    self.activeTextField = textField;
}

- (void)cellTextFieldDidEndEditing:(UITextField *)textField withRowKey:(NSString *)kRowKey
{
//    [self.viewModel textDidEndEditing:textField.text key:kRowKey];
    [self.viewModel setValue:textField.text forDirectionKey:kRowKey];
}

- (void)cellTextFieldDoneAction:(NSDictionary *)rowDict
{
    #pragma unused(rowDict)
    [self.activeTextField resignFirstResponder];
}

#pragma mark - Utilities

- (void) showLoadActivityWithStatus:(NSInteger)status
{
    CoreNavigationController *navController = [self.navigationController coreAsClass:[CoreNavigationController class]];
    TCoreCircleLoadActivity *circleLoadActivity = self.circleLoadActivity;
    
    if (!navController)
    {
        return;
    }
    
    switch (status) {
        case 1:
        //    navController.coreSwipeEnabled = NO;
            [self.circleLoadActivity coreAnimatedShowWithDuration:0.4f alpha:1.0f];
            [circleLoadActivity startAnimation];
            break;
        case 2:
        //    navController.coreSwipeEnabled = YES;
            [self stopLoadActivity:circleLoadActivity AndHideView:navController];
            break;
        case 3:
     //       navController.coreSwipeEnabled = YES;
            [self.circleLoadActivity coreAnimatedHideWithDuration:0.4f];
            break;
        default:
            break;
    }
}

- (void) stopLoadActivity:(TCoreCircleLoadActivity *)circleLoadActivity AndHideView:(CoreNavigationController *)navController {
#pragma unused(navController)
    __weak typeof(self) weakSelf = self;
    [circleLoadActivity stopAnimationWithCompletion:^{
        __strong typeof(weakSelf) blockSelf = weakSelf;
        [blockSelf.circleLoadActivity coreAnimatedHideWithDuration:0.4];
    }];
}

- (void) showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
    [alertController addAction:action];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void) showWebViewControllerWithURLString:(NSString *)urlString
{
    TCoreWebViewController *webViewController = [[TCoreWebViewController alloc] initWithURLString:urlString];
    webViewController.backButtonImage = [UIImage imageNamed:@"ic_arrow_back_white"];
//    webViewController.barBackgroundColor = COLOR_TEMP_VIEW_BACKGROUND;
//    webViewController.barTintColor = COLOR_TEMP_VIEW_BACKGROUND;
//    webViewController.tintColor = [UIColor whiteColor];
    
    [self presentViewController:webViewController animated:YES completion:nil];
}

- (void) configureViewController
{
    [self.navigationItem setTitle:@"Анкета"];
    
    _collectionViewLayout = [ShortDirectionCollectionViewFlowLayout new];
    _collectionView.collectionViewLayout = _collectionViewLayout;
    _collectionView.dataSource = self;
    _collectionView.delegate = self;
    _collectionView.backgroundColor = [UIColor clearColor];
    
    [CellManager registerCellsWithId:@[kCollectionViewWarningCell,
                                       kCollectionViewTextFieldCell,
                                       kCollectionViewSelectableCell,
                                       kBlueButtonCollectionViewCell,
                                       kCommentCollectionViewCell,
                                       kLabelCollectionViewCell] colectionView:_collectionView];
    
    TCoreCircleLoadActivity *circleLoadActivity = [[TCoreCircleLoadActivity alloc] init];
    circleLoadActivity.titleText = @"Ваша заявка отправляется…";
    circleLoadActivity.translatesAutoresizingMaskIntoConstraints = NO;
    circleLoadActivity.alpha = 0.0f;
    _circleLoadActivity = circleLoadActivity;
    
    [self.view addSubview:_circleLoadActivity];
    [NSLayoutConstraint activateConstraints:[NSLayoutConstraint standartBoxConstratinsFrom:self.view to:_circleLoadActivity constant:0.f]];
    [self.view bringSubviewToFront:_circleLoadActivity];
}

- (void) bindViewModel
{
    __weak typeof(self) weakSelf = self;

    [[_viewModel.signalErrorDescription deliverOnMainThread] subscribeNext:^(id  _Nullable message) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        
        if (!message)
        {
            NSString *errStr = (NSString *)message;
            if (errStr.length == 0)
            {
                return;
            }
        }
        
        [blockSelf showAlertWithTitle:@"Ошибка" message:message];
    }];
    
    [[_viewModel.signalTermOfUserURL deliverOnMainThread] subscribeNext:^(id  _Nullable url) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if (!url)
        {
            NSString *urlStr = (NSString *)url;
            if (urlStr.length == 0)
            {
                return;
            }
        }
        [blockSelf showWebViewControllerWithURLString:url];
    }];
}

#pragma mark - Keyboard Notification Handling

- (void)keyboardWillShownNotification:(NSNotification *)aNotification
{
    NSDictionary *info = [aNotification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    
    self.collectionViewBottomConstraint.constant = kbSize.height;
}

- (void)keyboardWillBeHiddenNotification:(NSNotification *)aNotification
{
    #pragma unused(aNotification)
    self.collectionViewBottomConstraint.constant = 0.f;
}

@end
