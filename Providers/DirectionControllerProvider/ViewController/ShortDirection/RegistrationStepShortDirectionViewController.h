//
//  RegistrationStepShortDirectionViewController.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 26.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RegistrationStepShortDirectionViewModel;

@interface RegistrationStepShortDirectionViewController : UIViewController

- (instancetype) initWithViewModel:(RegistrationStepShortDirectionViewModel *)viewModel;

@end
