
//
//  DirectionControllerProvider.h
//  CreditCalendar
//
//  Created by Алексей Молокович  on 13.09.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "CoreProvider.h"

@interface DirectionControllerProvider : NSObject<CoreProvider>

+ (id) sharedInstance;

@end
