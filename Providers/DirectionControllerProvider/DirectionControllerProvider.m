//
//  DirectionControllerProvider.m
//  CreditCalendar
//
//  Created by Алексей Молокович  on 13.09.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DirectionControllerProvider.h"

#import "CoreResourceContext.h"
#import "CoreResourcesManager.h"
#import "CoreAction.h"

#import "DirectionViewModel.h"
//#import "ShortDirectionViewModel.h"
#import "RegistrationStepShortDirectionViewModel.h"

#import "DirectionViewController.h"
//#import "ShortDirectionViewController.h"
#import "RegistrationStepShortDirectionViewController.h"

#import "PresentationNavigationGroup.h"
#import "ShortNavigationGroup.h"

#import "NSString+CoreUtils.h"

#import "DebugAsserts.h"


@implementation DirectionControllerProvider

#pragma mark - init methods

+ (void) load
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [[CoreResourcesManager sharedInstance] coreRegistrateProvider:[DirectionControllerProvider sharedInstance]];
    });
}

+ (id) sharedInstance
{
    static DirectionControllerProvider *_sharedInstance = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[DirectionControllerProvider alloc] init];
    });
    
    return _sharedInstance;
}

#pragma mark - CoreProvider protocol methods

- (void) acquireResourceWithContext:(CoreResourceContext *)context withCompletionBlock:(CoreProviderCompletionBlock)completionBlock
{
    DEBUG_ASSERTS_VALID([CoreResourceContext class], context);
    
    if (!context)
    {
        if (completionBlock)
        {
            completionBlock(nil);
        }
        return;
    }
    
    UIViewController *vc = nil;
    
    if (context.action)
    {
        vc = [self acquireViewControllerByActionWithResourceContext:context];
    }
    
    if (context.resourceClass)
    {
        vc = [self acquireViewControllerByResourceClassWithResourceContext:context];
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (completionBlock)
        {
            completionBlock(vc);
        }
    });
}

- (NSArray *) providedResourceClasses
{
//    return @[[DirectionViewModel class],[ShortDirectionViewModel class],[ShortDirectionViewController class],[DirectionViewController class]];
    return @[[DirectionViewModel class],[DirectionViewController class],[RegistrationStepShortDirectionViewModel class],[RegistrationStepShortDirectionViewController class]];
}

- (NSArray *) navigationGroupClasses
{
    return @[[ShortNavigationGroup class]];
}

- (NSArray *) actionKeys
{
    return @[[NSString stringWithUTF8String:PUSH.SHORT_DIRECTION.key],[NSString stringWithUTF8String:PUSH.DIRECTION.key]];
}

#pragma mark - utils methods

- (UIViewController *) acquireViewControllerByResourceClassWithResourceContext:(CoreResourceContext *)context
{
    Class resourceClass = context.resourceClass;
    
    if (![[self providedResourceClasses] containsObject:resourceClass])
    {
        return nil;
    }
    
    NSString *resourceClassName = [self prefixClass:resourceClass];
    DEBUG_ASSERTS_VALID([NSString class], resourceClassName);
    
    NSArray *avClasses = [self providedResourceClasses];
    NSPredicate *predicate = [self predicateForResourceClassName:resourceClassName];
    NSArray *result = [avClasses filteredArrayUsingPredicate:predicate];
    
    UIViewController *vc = nil;
    id viewModel = nil;
    
    for (Class cls in result)
    {
        if ([cls isSubclassOfClass:[CoreViewModel class]])
        {
            viewModel = [[cls alloc] initWithServices:context.services];
            continue;
        }
        
        vc = [[cls alloc] initWithViewModel:viewModel];
    }
    
    return vc;
}

- (UIViewController *) acquireViewControllerByActionWithResourceContext:(CoreResourceContext *)context
{
    CoreAction *action = context.action;
    
    NSString *key = action.key;
    
    if (![[self actionKeys] containsObject:key])
    {
        return nil;
    }
    
    UIViewController *viewConrtoller = nil;
    Class vmCls = nil;
    Class vcCls = nil;
    
    if ([key isEqualToUTF8String:PUSH.SHORT_DIRECTION.key])
    {
        vmCls = [RegistrationStepShortDirectionViewModel class];
        vcCls = [RegistrationStepShortDirectionViewController class];
//        vmCls = [ShortDirectionViewModel class];
//        vcCls = [ShortDirectionViewController class];
    }
    
    if ([key isEqualToUTF8String:PUSH.DIRECTION.key])
    {
        vmCls = [DirectionViewModel class];
        vcCls = [DirectionViewController class];
    }
    
    id viewModel = [[vmCls alloc] initWithServices:context.services];
    DEBUG_ASSERTS_VALID([CoreViewModel class], viewModel);
    
    viewConrtoller = [[vcCls alloc] initWithViewModel:viewModel];
    DEBUG_ASSERTS_VALID([UIViewController class], viewConrtoller);
    
    return viewConrtoller;
}


- (NSPredicate *) predicateForResourceClassName:(NSString *)clsName
{
    return [NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
#pragma unused(bindings)
        
        Class cls = evaluatedObject;
        NSString *clsPrefix = [self prefixClass:cls];
        
        return [clsPrefix isEqualToString:clsName];
    }];
}

- (NSString *) prefixClass:(Class)cls
{
    NSString *resourceClassName = [NSStringFromClass(cls) stringByReplacingOccurrencesOfString:@"ViewController" withString:@""];
    
    if ([cls isSubclassOfClass:[CoreViewModel class]])
    {
        resourceClassName = [resourceClassName stringByReplacingOccurrencesOfString:@"ViewModel" withString:@""];
    }
    
    return resourceClassName;
}


@end
