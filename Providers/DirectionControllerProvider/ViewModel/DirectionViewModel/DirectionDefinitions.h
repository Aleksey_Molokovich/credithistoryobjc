//
//  DirectionDefinitions.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 11.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#ifndef DirectionDefinitions_h
#define DirectionDefinitions_h

typedef NS_ENUM(NSInteger, DirectionCellType)
{
    DirectionCellSurname,
    DirectionCellName,
    DirectionCellPatronomic,
    DirectionCellBirthday,
    DirectionCellPlaceOfBirthday,
    DirectionCellGender,
    DirectionCellEmail,
    
    DirectionCellPassportSeries,
    DirectionCellPassportNumber,
    DirectionCellComment,
    DirectionCellIssueDate,
    DirectionCellCodeSubdivision,
    DirectionCellIssuedPlace,
    DirectionCellRegIndex,
    DirectionCellRegCity,
    DirectionCellRegCityFullWidth,
    DirectionCellRegAddress,
    DirectionCellRegHouse,
    DirectionCellRegBuilding,
    DirectionCellRegFlat,
    
    DirectionCellIncome,
    DirectionCellPhotoClient,
    DirectionCellPhotoPassport,
    DirectionCellActualData,
    DirectionCellConfirmationConditions,
    
    DirectionCellCallbackMethod,
    DirectionCellWarningMessage,
    DirectionCellContinueButton,
    DirectionCellAgreement
};

#endif /* DirectionDefinitions_h */
