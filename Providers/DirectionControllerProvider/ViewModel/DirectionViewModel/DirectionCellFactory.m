//
//  DirectionCellFactory.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 11.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "DirectionCellFactory.h"
#import "TextFieldCollectionCellDefinition.h"
#import "NSMutableAttributedString+CoreUtil.h"

#import "UIColor+DesignGuidelines.h"


@implementation DirectionCellFactory

-(CollectionCellConfigModel *)createCell:(DirectionCellType)type
                                editable:(BOOL)editable
{
    
    CollectionCellConfigModel *cell = [CollectionCellConfigModel new];
    cell.cellType = type;
    cell.editable = editable;
    cell.placeholder = @"";
    cell.widthType = CollectionCellWidthFull;
    cell.identifier = kCollectionViewTextFieldCell;
    
    switch (type) {
        case DirectionCellSurname:
            cell.title = @"Фамилия";
            cell.subType = kTextFieldCollectionCellSubTypeString;
            cell.key = @"surname";
            break;
            
        case DirectionCellName:
            cell.title = @"Имя";
            cell.subType = kTextFieldCollectionCellSubTypeString;
            cell.key = @"name";
            break;
            
        case DirectionCellPatronomic:
            cell.title = @"Отчество";
            cell.subType = kTextFieldCollectionCellSubTypeString;
            cell.key = @"patronymic";
            break;
            
        case DirectionCellBirthday:
            cell.title = @"Дата рождения";
            cell.subType = kTextFieldCollectionCellSubTypeDate;
            cell.key = @"birthday";
            cell.placeholder = @"дд.мм.гггг";
            break;
            
        case DirectionCellPlaceOfBirthday:
            cell.title = @"Место рождения";
            cell.subType = kTextFieldCollectionCellSubTypeString;
            cell.key = @"placeOfBirth";
            break;
            
            //        case DirectionCellGender:
            //            cell.widthType = CollectionCellWidthHalf;
            //            cell.title = @"";
            //            cell.identifier = kCollectionViewTextFieldCell;
            //            cell.subType = kTextFieldCollectionCellSubTypeString;
            //            cell.key = @"gender";
            //            break;
        case DirectionCellEmail:
            cell.title = @"Адрес электронной почты";
            cell.subType = kTextFieldCollectionCellSubTypeString;
            cell.key = @"email";
            break;
            
        case DirectionCellPassportSeries:
            cell.widthType = CollectionCellWidthHalf;
            cell.title = @"Серия паспорта";
            cell.subType = kTextFieldCollectionCellSubTypeSeriasPassport;
            cell.key = @"passportSeries";
            break;
            
        case DirectionCellPassportNumber:
            cell.widthType = CollectionCellWidthHalf;
            cell.title = @"Номер паспорта";
            cell.subType = kTextFieldCollectionCellSubTypeNumberPassport;
            cell.key = @"passportNumber";
            break;
            
        case DirectionCellComment:
            cell.widthType = CollectionCellWidthFull;
            cell.title = @"Данные необходимы для получения информации из Бюро кредитных историй";
            cell.identifier = kCommentCollectionViewCell;
            break;
            
        case DirectionCellIssueDate:
            cell.widthType = CollectionCellWidthHalf;
            cell.title = @"Дата выдачи";
            cell.subType = kTextFieldCollectionCellSubTypeDate;
            cell.key = @"issuedDate";
            break;
            
        case DirectionCellCodeSubdivision:
            cell.widthType = CollectionCellWidthHalf;
            cell.title = @"Код подразделения";
            cell.subType = kTextFieldCollectionCellSubTypeSubvisionPassport;
            cell.key = @"codeSubdivision";
            break;
            
        case DirectionCellIssuedPlace:
            cell.title = @"Выдан";
            cell.subType = kTextFieldCollectionCellSubTypeString;
            cell.key = @"issuedPlace";
            break;
            
        case DirectionCellRegIndex:
            cell.widthType = CollectionCellWidthHalf;
            cell.title = @"Индекс";
            cell.identifier = kCollectionViewTextFieldCell;
            cell.subType = kTextFieldCollectionCellSubTypeIndex;
            cell.key = @"registrationIndex";
            break;
            
        case DirectionCellRegCity:
            cell.widthType = CollectionCellWidthHalf;
            cell.title = @"Город регистрации";
            cell.identifier = kCollectionViewTextFieldCell;
            cell.subType = kTextFieldCollectionCellSubTypeString;
            cell.key = @"registrationCity";
            break;
            
        case DirectionCellRegCityFullWidth:
            cell.title = @"Город регистрации";
            cell.identifier = kCollectionViewTextFieldCell;
            cell.subType = kTextFieldCollectionCellSubTypeString;
            cell.key = @"registrationCity";
            break;
            
        case DirectionCellRegAddress:
            cell.widthType = CollectionCellWidthFull;
            cell.title = @"Адрес постоянной регистрации";
            cell.identifier = kCollectionViewTextFieldCell;
            cell.subType = kTextFieldCollectionCellSubTypeString;
            cell.key = @"registrationAddress";
            break;
            
        case DirectionCellRegHouse:
            cell.widthType = CollectionCellWidthOneThird;
            cell.title = @"Дом";
            cell.subType = kTextFieldCollectionCellSubTypeString;
            cell.key = @"house";
            break;
            
        case DirectionCellRegBuilding:
            cell.widthType = CollectionCellWidthOneThird;
            cell.title = @"Корпус";
            cell.subType = kTextFieldCollectionCellSubTypeString;
            cell.key = @"building";
            break;
            
        case DirectionCellRegFlat:
            cell.widthType = CollectionCellWidthOneThird;
            cell.title = @"Кв.";
            cell.subType = kTextFieldCollectionCellSubTypeNumber;
            cell.key = @"flat";
            break;
            
        case DirectionCellIncome:
            cell.widthType = CollectionCellWidthFull;
            cell.title = @"Доход (RUB)*";
            cell.identifier = kCollectionViewTextFieldCell;
            cell.subType = kTextFieldCollectionCellSubTypeMoney;
            cell.key = @"income";
            break;
            
        case DirectionCellPhotoClient:
            cell.widthType = CollectionCellWidthHalf;
            cell.title = @"Ваша фотография";
            cell.identifier = kAddPhotoCollectionViewCell;
            cell.key = @"photoClient";
            break;
            
        case DirectionCellPhotoPassport:
            cell.widthType = CollectionCellWidthHalf;
            cell.title = @"Фото вашего паспорта (2 и 3 стр. целиком)";
            cell.identifier = kAddPhotoCollectionViewCell;
            cell.key = @"photoPassport";
            break;
            
        case DirectionCellActualData:
            cell.widthType = CollectionCellWidthHalf;
            cell.title = @"";
            cell.identifier = kCollectionViewTextFieldCell;
            cell.subType = kTextFieldCollectionCellSubTypeDate;
            cell.key = @"actualData";
            break;
            
        case DirectionCellConfirmationConditions:
            cell.widthType = CollectionCellWidthHalf;
            cell.title = @"";
            cell.identifier = kCollectionViewTextFieldCell;
            cell.subType = kTextFieldCollectionCellSubTypeDate;
            cell.key = @"confirmConditions";
            break;
            
        case DirectionCellCallbackMethod:
            cell.widthType = CollectionCellWidthFull;
            cell.title = @"Выберите способ связи";
            cell.identifier = kCollectionViewSelectableCell;
            cell.key = @"callBackMethod";
            break;
            
        case DirectionCellWarningMessage:
            cell.widthType = CollectionCellWidthFull;
            cell.title = @"Ошибка в данных по кредиту.";
            cell.identifier = kCollectionViewWarningCell;
            break;
            
        case DirectionCellContinueButton:
            cell.widthType = CollectionCellWidthFull;
            cell.title = @"Отправить анкету";
            cell.identifier = kBlueButtonCollectionViewCell;
            break;
            
        case DirectionCellAgreement:
        {
            cell.widthType = CollectionCellWidthFull;
            NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"Нажимая кнопку «Зарегистрироваться», вы принимаете условия Пользовательского соглашения и Обработки персональных данных"];
            [string setColorForText:@"Пользовательского соглашения" withColor:[UIColor coreDarkGreyBlueColor]];
            [string setColorForText:@"Обработки персональных данных" withColor:[UIColor coreDarkGreyBlueColor]];
            cell.attributedTitle = string;
            
            cell.identifier = kLabelCollectionViewCell;
        }
            break;
        default:
            break;
    }
    
    return cell;
}

@end
