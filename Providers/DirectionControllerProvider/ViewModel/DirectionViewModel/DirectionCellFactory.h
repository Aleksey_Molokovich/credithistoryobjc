//
//  DirectionCellFactory.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 11.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollectionCellConfigModel.h"
#import "DirectionDefinitions.h"

@interface DirectionCellFactory : NSObject

- (CollectionCellConfigModel *)createCell:(DirectionCellType)type
                              editable:(BOOL)editable;

@end
