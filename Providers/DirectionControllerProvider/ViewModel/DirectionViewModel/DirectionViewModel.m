//
//  RegistrationViewModel.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 04.10.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import "ViewModelServices.h"

#import "DirectionViewModel.h"
#import "CoreViewModel_Private.h"
#import "DirectionVerificationViewModel.h"

#import "AccountService.h"

#import "AnalyticsManager.h"
#import "SpotlightManager+Popup.h"

#import "Direction.h"
#import "DirectionImpl.h"
#import "UserImpl.h"

#import "CoreAction.h"

#import "NSError+CoreUtils.h"
#import "UIImage+CoreUtils.h"

#import "CollectionCellDataModel.h"
#import "AddPhotoCollectionViewCellModel.h"

NSString *const kDirectionURLTermsOfUser = @"https://sovcombank.ru/apply/PD_Guide_2016.html";

NSInteger const maxPage = 2;

@interface DirectionViewModel ()

@property (strong, nonatomic) DirectionImpl *direction;

@property (strong, nonatomic) RACSignal *signalUpdate;
@property (strong, nonatomic) RACSignal *signalImageUpdate;
@property (strong, nonatomic) RACSignal *signalInvalideDataInfo;
@property (strong, nonatomic) RACSignal *signalInvalideDataPassport;
@property (strong, nonatomic) RACSignal *signalDirectionSendStatus;
@property (strong, nonatomic) RACSignal *signalUploadDescription;
@property (strong, nonatomic) RACSignal *signalErrorDescription;
@property (strong, nonatomic) RACSignal *signalTermOfUserURL;

@property (strong, nonatomic) DirectionDatasource *datasource;
@property (strong, nonatomic) DirectionValidator *validator;
@property (strong, nonatomic) NSArray *invalideInfoRows;
@property (strong, nonatomic) NSArray *invalidePassportRows;

@property (strong, nonatomic) NSNumber *page;
@property (strong, nonatomic) NSData *photoClient;
@property (strong, nonatomic) NSData *photoPassport;
@property (strong, nonatomic) NSData *imageData;
@property (strong, nonatomic) NSString *key;

@property (strong, nonatomic) NSString *uploadDesc;
@property (strong, nonatomic) NSString *errorDesc;
@property (assign, nonatomic) kUserDirectionSendStatus sendSatus;
@property (strong, nonatomic) NSString *termOfUserURLString;

@end

@implementation DirectionViewModel

@synthesize actualData = _actualData;
@synthesize confirmConditions = _confirmConditions;
@synthesize selectedSegmentDataSource = _selectedSegmentDataSource;
@synthesize selectedSegmentIndex = _selectedSegmentIndex;

#pragma mark - init methods

- (instancetype) initWithServices:(id<ViewModelServices>)services
{
    if ( (self = [super initWithServices:services]) == nil)
    {
        return nil;
    }
    
    UserImpl *user = [[self.services accountService] account];
    self.direction = user.direction;
    self.termOfUserURLString = nil;
    self.sendSatus = kUserDirectionSendStatus_None;
    self.services = services;
    self.key = @"";
    self.invalideInfoRows = [NSArray array];
    self.invalidePassportRows = [NSArray array];
    self.selectedSegmentDataSource = @[@"Женский",@"Мужской"];
    self.selectedSegmentIndex = 1;
    
    self.datasource = [DirectionDatasource new];
    [self.datasource prepare];
    self.validator = [DirectionValidator new];
    
    if (!self.direction)
    {
        self.direction = [[DirectionImpl alloc] init];
    }
    
    self.direction.name = user.name;
    self.direction.surname = user.surname;
    self.direction.patronymic = user.patronymic;
    self.direction.passportSeries = user.passportSeries;
    self.direction.passportNumber = user.passportNumber;
    
#if DEBUG
    self.direction.name = self.direction.name?self.direction.name: @"Игорь";
    self.direction.surname = self.direction.surname?self.direction.surname : @"Мовчан";
    self.direction.patronymic = self.direction.patronymic? self.direction.patronymic:@"Андреевич";
    NSDateFormatter *dateFormater = [[NSDateFormatter alloc] init];
    dateFormater.dateFormat = @"dd.MM.yyyy";
     [dateFormater setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    self.direction.birthday = [dateFormater dateFromString:@"23.06.1982"];
    self.direction.placeOfBirth = @"Москва";
    self.direction.passportSeries = self.direction.passportSeries?self.direction.passportSeries:@"4509";
    self.direction.passportNumber = self.direction.passportNumber?self.direction.passportNumber:@"146769";
    self.direction.issuedPlace = @"ОВД Ясеневой";
    self.direction.issuedDate = [dateFormater dateFromString:@"28.05.2007"];
    self.direction.codeSubdivision = @"770-125";
    self.direction.registrationCity = @"Москва";
    self.direction.registrationAddress = @"Литовская";
    self.direction.house = @"3";
    self.direction.registrationIndex = @"420032";
    self.direction.snils = @"123-123-123 33";
    self.direction.income = 150000;
#endif
    
    [self initialize];
    return self;
}

- (void)initialize
{
    self.signalUpdate = RACObserve(self, direction);
    self.signalInvalideDataInfo = RACObserve(self, invalideInfoRows);
    self.signalInvalideDataPassport = RACObserve(self, invalidePassportRows);
    self.signalImageUpdate = RACObserve(self, imageData);
    self.signalDirectionSendStatus = RACObserve(self, sendSatus);
    self.signalUploadDescription = RACObserve(self, uploadDesc);
    self.signalErrorDescription = RACObserve(self, errorDesc);
    self.signalTermOfUserURL = RACObserve(self, termOfUserURLString);

}

#pragma mark - Getters/Setters

-(NSData *)getPhotoWithKey:(NSString *)key
{
    if ([key isEqualToString:@"photoClient"]) {
        return [self photoClient];
    }else if ([key isEqualToString:@"photoPassport"]) {
        return [self photoPassport];
    }
    return nil;
}

- (NSData *)photoClient
{    
    if (_photoClient == nil && _direction.photoClient != nil)
    {
        AccountService *accountService = [self.services accountService];
        _photoClient = [accountService accountPhotoAtPath:_direction.photoClient];
    }
    return _photoClient;
}

- (NSData *)photoPassport
{
    if (_photoPassport == nil && _direction.photoPassport != nil)
    {
        AccountService *accountService = [self.services accountService];
        _photoPassport = [accountService accountPhotoAtPath:_direction.photoPassport];
    }
    return _photoPassport;
}

#pragma mark - DirectionViewModelInputs methods

- (void)actionContinue
{

    if (![_validator validate:_direction]) {
        self.direction = self.direction;
        return;
    }

    id<ViewModelServices> services = self.services;
    AccountService *accountService = [services accountService];
//    AnalyticsManager *analyticsManager = [AnalyticsManager sharedInstance];
//    [analyticsManager logFillFirstPageDirection];
    
    //        [analyticsManager logFillSecondPageDirection];
    self.sendSatus = kUserDirectionSendStatus_Start;
    __weak typeof(self) weakSelf = self;
    [accountService accountSendDirection:self.direction progressBlock:^(NSString * _Nonnull progressDescription) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        blockSelf.uploadDesc = progressDescription;
    } withCompletionBlock:^(NSError * _Nullable error)
     {
         __strong typeof(weakSelf) blockSelf = weakSelf;
         
         if (error)
         {
             blockSelf.sendSatus = kUserDirectionSendStatus_Error;
             blockSelf.errorDesc = [error coreMessage];
             return;
         }
         
         blockSelf.sendSatus = kUserDirectionSendStatus_Finish;
         [accountService account].status = UserRegistrationFullVereficationStatus;
         CoreAction *action = [CoreAction actionFromActionDescription:PUSH.SCORE];
         [services dispatchAction:action];
         
         [SpotlightManager insertInfoPopupWithTitle:@"Поздравляем!" message:@"После обработки данных, указанных в анкете Вам будут доступны основные функции в приложении" actions:nil style:kSpotlightItemPopupSStyleUltraWidth];
      }];
}

- (void)actionBack
{
    NSInteger value = [self.page integerValue];
    value = value == 0 ? value : value-1;
    self.page = @(value);
    
}

- (void) provideImage:(UIImage *)image forKey:(NSString *)key
{
    if (!image || !key)
    {
        return;
    }
    
    AccountService *accountService = [self.services accountService];
    NSString *path = [accountService accountSavePhoto:UIImageJPEGRepresentation(image, 0.3) withName:key];
    
    if (!path)
        return;
    
    
    image = [image coreImageRenderWithSize:CGSizeMake(image.size.width/10, image.size.height/10) andCorenderRadius:0];
    
    if ([key isEqualToString:@"photoClient"])
    {
        self.photoClient = UIImageJPEGRepresentation(image, 0.5) ;
    } else {
        self.photoPassport = UIImageJPEGRepresentation(image, 0.5);
    }
    
    [self.direction setValue:path forKey:key];
    
    self.direction = self.direction;
}

- (void)textDidEndEditing:(NSString *)text key:(NSString *)key
{
    [self.direction setValue:text forKey:key];
}

- (void)actionShowTermsOfUser
{
//    id<ViewModelServices> services = self.services;
    self.termOfUserURLString = kDirectionURLTermsOfUser;
}

- (void)selectedIndexChanged:(NSInteger)index
{
    self.selectedSegmentIndex = index;
    self.direction.gender = self.selectedSegmentDataSource[index];
}

- (CollectionCellDataModel *)dataForKey:(NSString *)key
{
        
    if (!key) 
        return nil;
    

    CollectionCellDataModel *data=[CollectionCellDataModel new];

    data.stateInfo = [_validator validateByKey:key];
    
    id value = [self.direction valueForKey:key];
    
    if ([key isEqualToString:@"photoPassport"] ||
        [key isEqualToString:@"photoClient"]) {
        AddPhotoCollectionViewCellModel *customData=[AddPhotoCollectionViewCellModel new];
        customData.path = value;
        data.customData = customData;
        return data;
    }
    
    
    if ([value isKindOfClass:[NSString class]]) {
        data.text = value;
    }
    else if ([value isKindOfClass:[NSNumber class]])
    {
        data.text=[NSString stringWithFormat:@"%@",value];
    }
    else if ([value isKindOfClass:[NSDate class]])
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        dateFormatter.dateFormat = @"dd.MM.YYYY";
        data.text = [dateFormatter stringFromDate:value];
    }
    
    return data;
}

@end
