//
//  DirectionValidator.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 15.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "DirectionValidator.h"
#import "DirectionImpl.h"
#import "CollectionCellValidateModel.h"

@implementation DirectionValidator

-(BOOL)validate:(DirectionImpl *)model
{
    
    NSMutableArray *invalideRows = [NSMutableArray array];
    
    if (!model.surname.length)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"surname" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (!model.name.length)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"name" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (!model.patronymic.length)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"patronymic" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (![self validateDate:model.birthday])
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"birthday" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (!model.placeOfBirth.length)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"placeOfBirth" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (model.income<5000)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"income" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (model.passportSeries.length != 4)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"passportSeries" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (model.passportNumber.length != 6)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"passportNumber" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (!model.issuedPlace.length)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"issuedPlace" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (![self validateDate: model.issuedDate])
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"issuedDate" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (!model.codeSubdivision.length)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"codeSubdivision" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (!model.registrationAddress.length)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"registrationAddress" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (!model.house.length)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"house" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (!model.photoClient.length)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"photoClient" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (!model.photoPassport.length)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"photoPassport" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    self.invalideRows = invalideRows;
    return ([invalideRows count]==0)?YES:NO;
}

- (CollectionCellValidateModel*) validateByKey:(NSString*)key
{
    for (CollectionCellValidateModel *err in self.invalideRows)
    {
        if ([err.key isEqual:key])
        {
            return err;
        }
    }
    return nil;
}

- (BOOL)validateDate:(NSDate*)checkDate
{
    if (!checkDate) {
        return NO;
    }
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:checkDate];
    
    NSDateComponents *current = [[NSCalendar currentCalendar] components: NSCalendarUnitYear fromDate:[NSDate date]];
    
    
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    NSInteger currentYear = [current year];
    
    if (day >0 && month>0 &&
        year<=currentYear )
    {
        return YES;
    }
    return NO;
}

@end
