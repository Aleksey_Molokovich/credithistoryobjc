//
//  RegistrationViewModel.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 04.10.16.
//  Copyright © 2016 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import "Direction.h"
#import "CoreViewModel.h"
#import "DirectionDatasource.h"
#import "DirectionValidator.h"
@class CollectionCellDataModel;
typedef NS_ENUM(NSInteger,kUserInfoFormPage)
{
    kUserInfoFormDirectPage = 0,
    kUserInfoFormPassportPage
};

typedef NS_ENUM(NSInteger, kUserDirectionSendStatus)
{
    kUserDirectionSendStatus_None = 0,
    kUserDirectionSendStatus_Start,
    kUserDirectionSendStatus_Finish,
    kUserDirectionSendStatus_Error
};

@interface DirectionViewModel : CoreViewModel

- (instancetype) initWithServices:(id<ViewModelServices>)services;


//Вызывается при переходе на следующую страницу и отправке анкеты
- (void)actionContinue;

//Вызывается при переходе к предыдущей странице
- (void)actionBack;

//Вызывается при окончании изменения текста в UITextField
- (void)textDidEndEditing:(NSString *)text key:(NSString *)key;

- (void)provideImage:(UIImage *)image forKey:(NSString *)key;

//Вызывает при открытие правил
- (void)actionShowTermsOfUser;

//Вызывает при изменении выбора пола
- (void)selectedIndexChanged:(NSInteger)index;

- (NSData *)getPhotoWithKey:(NSString *)key;

@property (nonatomic) UIImage *image;

//Сигнал при смене страницы
@property (strong, readonly, nonatomic) RACSignal *signalUpdate;

//Сигнал при выборе фотографии из галерии
@property (strong, readonly, nonatomic) RACSignal *signalImageUpdate;

//Сигнал при неправильно заполненных данных на первой странице
@property (strong, readonly, nonatomic) RACSignal *signalInvalideDataInfo;

//Сигнал при неправильно заполненных данных на второй странице
@property (strong, readonly, nonatomic) RACSignal *signalInvalideDataPassport;

@property (strong, readonly, nonatomic) RACSignal *signalDirectionSendStatus;
@property (strong, readonly, nonatomic) RACSignal *signalUploadDescription;
@property (strong, readonly, nonatomic) RACSignal *signalErrorDescription;
@property (strong, readonly, nonatomic) RACSignal *signalTermOfUserURL;

//Данные для collectionView
@property (strong, readonly, nonatomic) DirectionDatasource *datasource;

@property (strong, readonly, nonatomic) DirectionValidator *validator;

////Данные о неправильных ячейках на первой странице
//@property (strong, readonly, nonatomic) NSArray *invalideInfoRows;
//
////Данные о неправильных ячейках на второй странице
//@property (strong, readonly, nonatomic) NSArray *invalidePassportRows;

////Фото клиента
//@property (strong, readonly, nonatomic) NSData *photoClient;
//
////Фото паспорта
//@property (strong, readonly, nonatomic) NSData *photoPassport;



@property (assign, nonatomic) NSInteger selectedSegmentIndex;
@property (strong, nonatomic) NSArray *selectedSegmentDataSource;
@property (assign, nonatomic) BOOL actualData;
@property (assign, nonatomic) BOOL confirmConditions;

//Возвращает текст для вывода на экран для опеределенного свойства по ключу
- (CollectionCellDataModel *)dataForKey:(NSString *)key;
//Возвращает indexPath для данных определенной ячейки
- (NSIndexPath *)indexPathAtRow:(NSDictionary *)rowDict;
//Возвращает данные следующей ячейки в зависимостри от приходящей ячейки
- (NSDictionary *)nextRow:(NSDictionary *)rowDict;

@end
