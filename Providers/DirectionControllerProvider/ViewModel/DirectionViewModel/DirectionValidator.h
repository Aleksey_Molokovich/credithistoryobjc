//
//  DirectionValidator.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 15.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CollectionCellValidateModel;
@class DirectionImpl;

@interface DirectionValidator : NSObject

@property (strong, nonatomic) NSArray *invalideRows;

- (BOOL) validate:(DirectionImpl*)model;

- (CollectionCellValidateModel*) validateByKey:(NSString*)key;
@end
