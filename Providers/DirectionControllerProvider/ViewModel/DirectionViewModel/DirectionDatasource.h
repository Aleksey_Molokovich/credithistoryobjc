//
//  DirectionDatasource.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 11.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class  CollectionCellConfigModel;
@class  CollectionCellHeaderModel;
@interface DirectionDatasource : NSObject
- (void)prepare;

@property (nonatomic, assign) NSInteger countSections;

- (NSString*)cellIdAtIndexPath:(NSIndexPath*)path;

- (CollectionCellConfigModel *)cellConfigAtIndexPath:(NSIndexPath *)path;

- (NSInteger) countCellInSection:(NSInteger)section;

- (CollectionCellHeaderModel*)headerForSection:(NSInteger)section;

- (CGFloat)hightForHeaderSection:(NSInteger)section;
@end
