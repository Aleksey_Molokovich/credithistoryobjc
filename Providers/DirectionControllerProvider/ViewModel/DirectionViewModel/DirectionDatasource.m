//
//  DirectionDatasource.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 11.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "DirectionDatasource.h"
#import "CollectionCellHeaderModel.h"
#import "CollectionCellConfigModel.h"
#import "DirectionCellFactory.h"

@interface DirectionDatasource()

@property (nonatomic, strong) DirectionCellFactory *factoryCell;
@property (nonatomic, strong) NSArray<CollectionCellConfigModel*> *cells;
@property (nonatomic, strong) NSArray<CollectionCellHeaderModel*> *sections;

@end

@implementation DirectionDatasource

- (NSInteger)countSections
{
    return _countSections;
}


- (void)prepare;
{
    if (!_factoryCell)
    {
        _factoryCell = [DirectionCellFactory new];
    }
    NSMutableArray *mSections = [NSMutableArray new];
    
    CollectionCellHeaderModel *headerWarning = [CollectionCellHeaderModel new];
    headerWarning.height = 0;
    headerWarning.cells = @[[_factoryCell createCell:DirectionCellWarningMessage editable:NO],];
    

    
    BOOL editable = YES;
    
    
    
    CollectionCellHeaderModel *personalData = [CollectionCellHeaderModel new];
    personalData.height = kCollectionViewSectionHeight;
    personalData.title = @"ПЕРСОНАЛЬНЫЕ ДАННЫЕ";
    personalData.cells = @[
                           [_factoryCell createCell:DirectionCellSurname editable:editable],
                           [_factoryCell createCell:DirectionCellName editable:editable],
                           [_factoryCell createCell:DirectionCellPatronomic editable:editable],
                           [_factoryCell createCell:DirectionCellBirthday editable:editable],
                           [_factoryCell createCell:DirectionCellPlaceOfBirthday editable:editable],
                           [_factoryCell createCell:DirectionCellIncome editable:editable]];
    [mSections addObject:personalData];
    
    CollectionCellHeaderModel *passportData = [CollectionCellHeaderModel new];
    passportData.height = kCollectionViewSectionHeight;
    passportData.title = @"ПАСПОРТНЫЕ ДАННЫЕ";
    passportData.cells = @[[_factoryCell createCell:DirectionCellPassportSeries editable:editable],
                           [_factoryCell createCell:DirectionCellPassportNumber editable:editable],
                           [_factoryCell createCell:DirectionCellIssuedPlace editable:editable],
                           [_factoryCell createCell:DirectionCellIssueDate editable:editable],
                           [_factoryCell createCell:DirectionCellCodeSubdivision editable:editable],
                           [_factoryCell createCell:DirectionCellRegIndex editable:editable],
                           [_factoryCell createCell:DirectionCellRegCity editable:editable],
                           [_factoryCell createCell:DirectionCellRegAddress editable:editable],
                           [_factoryCell createCell:DirectionCellRegHouse editable:editable],
                           [_factoryCell createCell:DirectionCellRegBuilding editable:editable],
                           [_factoryCell createCell:DirectionCellRegFlat editable:editable]];
    
    [mSections addObject:passportData];

    CollectionCellHeaderModel *photoData = [CollectionCellHeaderModel new];
    photoData.height = kCollectionViewSectionHeight;
    photoData.title = @"ЗАГРУЗКА ФОТОГРАФИЙ";
    photoData.cells = @[[_factoryCell createCell:DirectionCellPhotoClient editable:editable],
                        [_factoryCell createCell:DirectionCellPhotoPassport editable:editable],
                        [_factoryCell createCell:DirectionCellAgreement editable:editable],
                        [_factoryCell createCell:DirectionCellContinueButton editable:editable]
                        ];
    
    [mSections addObject:photoData];
    
    self.sections = [mSections copy];
    self.countSections = [self.sections count];
}


- (NSInteger) countCellInSection:(NSInteger)section
{
    return [self.sections[section].cells count];
}

-(CollectionCellConfigModel *)cellConfigAtIndexPath:(NSIndexPath *)path
{
    return self.sections[path.section].cells[path.item];
}

-(NSString *)cellIdAtIndexPath:(NSIndexPath *)path
{
    return self.sections[path.section].cells[path.item].identifier;
}

-(CollectionCellHeaderModel *)headerForSection:(NSInteger)section
{
    return self.sections[section];
}

-(CGFloat)hightForHeaderSection:(NSInteger)section
{
    return self.sections[section].height;
}
@end
