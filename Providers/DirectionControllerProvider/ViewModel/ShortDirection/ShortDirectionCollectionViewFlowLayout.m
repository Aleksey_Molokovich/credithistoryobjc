//
//  ShortDirectionCollectionViewFlowLayout.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 26.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "ShortDirectionCollectionViewFlowLayout.h"

@implementation ShortDirectionCollectionViewFlowLayout


-(void)configure
{
    [super configure];
//    self.minimumInteritemSpacing = 20.f;
//    self.minimumLineSpacing = 0.f;
    self.sectionInset = UIEdgeInsetsMake(0.f, 16.f, 0.f, 16.f);
    
//    self.estimatedItemSize = CGSizeMake(1,1);
    self.headerReferenceSize = CGSizeMake(self.collectionView.frame.size.width,
                                             0);
}

//    self.minimumInteritemSpacing = kCollectionViewInteritemSpacing;
//    self.minimumLineSpacing = kCollectionViewMinimumLineSpacing;
//    self.sectionInset = kCollectionViewInsets;
//    self.estimatedItemSize = CGSizeMake(1,1);
//    self.headerReferenceSize =CGSizeMake(self.collectionView.frame.size.width,
//                                         kCollectionViewSectionHeight);
//}
@end
