//
//  ShortDirectionValidator.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 26.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "ShortDirectionValidator.h"
#import "ShortDirectionImpl.h"
#import "CollectionCellValidateModel.h"

@implementation ShortDirectionValidator

-(BOOL)validate:(ShortDirectionImpl *)model
{
    NSMutableArray *invalideRows = [NSMutableArray array];
    
    if (!model.surname.length)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"surname" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (!model.name.length)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"name" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (!model.patronymic.length)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"patronymic" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (model.passportSeries.length != 4)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"passportSeries" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    if (model.passportNumber.length != 6)
        [invalideRows addObject:[CollectionCellValidateModel initWithKey:@"passportNumber" error:@"Укажите фамилию" state:CollectionCellDataModelStateUnvalid]];
    
    self.invalideRows = invalideRows;
    return ([invalideRows count]==0)?YES:NO;
}

@end
