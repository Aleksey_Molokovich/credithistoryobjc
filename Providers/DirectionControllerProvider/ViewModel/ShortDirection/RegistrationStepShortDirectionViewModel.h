//
//  RegistrationStepShortDirectionViewModel.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 26.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveObjC/ReactiveObjC.h>

#import "CoreViewModel.h"
#import "RegistrationStepViewModelProtocol.h"

#import "ShortDirectionDatasource.h"
#import "ShortDirectionValidator.h"

@class CollectionCellDataModel;

@interface RegistrationStepShortDirectionViewModel : CoreViewModel <RegistrationStepViewModelProtocol>

@property (nonatomic, strong, readonly) ShortDirectionDatasource *datasource;
@property (nonatomic, strong, readonly) ShortDirectionValidator *validator;

@property (nonatomic, strong, readonly) RACSignal *signalErrorDescription;
@property (nonatomic, strong, readonly) RACSignal *signalTermOfUserURL;
@property (strong, nonatomic, readonly) RACCommand *commandSendDirection;

- (CollectionCellDataModel *)dataForKey:(NSString *)key;

- (void)setValue:(NSString *)value forDirectionKey:(NSString *)key;

- (void)showTerms;

- (void)sendDirection;


@end
