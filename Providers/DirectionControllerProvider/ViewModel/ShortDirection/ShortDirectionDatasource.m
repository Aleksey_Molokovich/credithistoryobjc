//
//  ShortDirectionDatasource.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 26.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "ShortDirectionDatasource.h"
#import "DirectionCellFactory.h"
#import "CollectionCellHeaderModel.h"
#import "CollectionCellConfigModel.h"

@interface ShortDirectionDatasource()

@property (nonatomic, strong) DirectionCellFactory *factoryCell;
@property (nonatomic, strong) NSArray<CollectionCellConfigModel*> *cells;
@property (nonatomic, strong) NSArray<CollectionCellHeaderModel*> *sections;

@end

@implementation ShortDirectionDatasource

- (void)prepare
{
    if (!_factoryCell) {
        _factoryCell = [DirectionCellFactory new];
    }
    
    BOOL editable = YES;
    
    CollectionCellHeaderModel *shortDirection = [CollectionCellHeaderModel new];
    shortDirection.height = 0;
    shortDirection.cells = @[[_factoryCell createCell:DirectionCellSurname editable:editable],
                             [_factoryCell createCell:DirectionCellName editable:editable],
                             [_factoryCell createCell:DirectionCellPatronomic editable:editable],
                             [_factoryCell createCell:DirectionCellBirthday editable:editable],
                             [_factoryCell createCell:DirectionCellEmail editable:editable],
                             [_factoryCell createCell:DirectionCellPassportSeries editable:editable],
                             [_factoryCell createCell:DirectionCellPassportNumber editable:editable],
                             [_factoryCell createCell:DirectionCellComment editable:editable],
                             [_factoryCell createCell:DirectionCellAgreement editable:editable],
                             [_factoryCell createCell:DirectionCellContinueButton editable:editable]];
    
    self.sections = @[shortDirection];
    self.countSections = [_sections count];
}

@end
