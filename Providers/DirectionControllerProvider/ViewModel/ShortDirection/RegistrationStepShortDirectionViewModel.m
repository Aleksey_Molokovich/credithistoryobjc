//
//  RegistrationStepShortDirectionViewModel.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 26.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "RegistrationStepShortDirectionViewModel.h"
#import "RegistrationViewModel.h"
#import "CoreViewModel_Private.h"

#import "AccountService.h"
#import "ViewModelServices.h"
#import "HTTPSynchronizationService.h"

#import "CoreLoading.h"

#import "ShortDirectionDatasource.h"
#import "DirectionValidator.h"

#import "CollectionCellDataModel.h"

#import "ShortDirectionImpl.h"

#import "CoreAction.h"

#import "NSError+CoreUtils.h"
#import "NSObject+CoreUtils.h"
#import "NSDate+CoreUtils.h"

#import "DebugAsserts.h"

NSString *const kTermsURL = @"https://sovcombank.ru/apply/PD_Guide_2016.html";

@interface RegistrationStepShortDirectionViewModel()

@property (nonatomic, strong) ShortDirectionDatasource *datasource;
@property (nonatomic, strong) ShortDirectionValidator *validator;

@property (nonatomic, strong) RACSignal *signalErrorDescription;
@property (nonatomic, strong) RACSignal *signalTermOfUserURL;

@property (nonatomic, strong) ShortDirectionImpl *direction;

@property (strong, nonatomic) NSArray *invalidRows;
@property (strong, nonatomic) NSString *errorDesc;
@property (strong, nonatomic) NSString *termOfUserURLString;

@end


@implementation RegistrationStepShortDirectionViewModel

@synthesize mainRegViewModel;

#pragma mark - init methods

- (instancetype)initWithServices:(id<ViewModelServices>)services
{
    if ( (self = [super initWithServices:services]) == nil)
    {
        return nil;
    }
    
    _validator = [ShortDirectionValidator new];
    _datasource = [ShortDirectionDatasource new];
    [_datasource prepare];
    
    AccountService *accountService = [self.services accountService];
    self.direction = [accountService accountShortDirection];

#if DEBUG
    self.direction.name = @"Игорь";
    self.direction.surname = @"Мовчан";
    self.direction.patronymic = @"Андреевич";
    self.direction.email = @"email@email.ru";
    self.direction.passportSeries = @"4509";
    self.direction.passportNumber = @"146769";
    self.direction.birthday = [NSDate coreDateFromUndefinedObject:@"23.06.1982"];
#endif
    
    _invalidRows = [NSArray array];
    
    self.signalErrorDescription = RACObserve(self, errorDesc);
    self.signalTermOfUserURL = RACObserve(self, termOfUserURLString);
    
    NSNotificationCenter *notifCenter = [NSNotificationCenter defaultCenter];
    [notifCenter addObserver:self selector:@selector(notificationWillTerminatenotification:) name:UIApplicationWillTerminateNotification object:nil];
    
    
    return self;
}

- (void) dealloc
{
    NSLog(@"%@ deallocated",NSStringFromClass(self.class));
    NSNotificationCenter *notifCenter = [NSNotificationCenter defaultCenter];
    [notifCenter removeObserver:self name:UIApplicationWillTerminateNotification object:nil];
}

#pragma mark - Notifications handler methods

- (void) notificationWillTerminatenotification:(NSNotification *)notif
{
    #pragma unused(notif)
    AccountService *accountService = [self.services accountService];
    [accountService accountSaveShortDirection:self.direction];
}

- (CollectionCellDataModel *)dataForKey:(NSString *)key
{
    if (!key)
    {
        return nil;
    }
    
    CollectionCellDataModel *data= [CollectionCellDataModel new];
    
    data.stateInfo = [_validator validateByKey:key];
    
    id value = [self.direction valueForKey:key];
    
    if ([value isKindOfClass:[NSString class]])
    {
        data.text = value;
    }
    else if ([value isKindOfClass:[NSNumber class]])
    {
        data.text=[NSString stringWithFormat:@"%@",value];
    }
    else if ([value isKindOfClass:[NSDate class]])
    {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        dateFormatter.dateFormat = @"dd.MM.YYYY";
        data.text = [dateFormatter stringFromDate:value];
    }
    
    return data;
}

- (void)setValue:(id)value forDirectionKey:(NSString *)key
{
    [self.direction setValue:value forKey:key];
}

- (void)showTerms
{
    self.termOfUserURLString = kTermsURL;
}


- (RACCommand *)commandSendDirection
{
    __weak typeof(self) weakSelf = self;
    RACSignal *signalShortDirectionValidation = [RACSignal combineLatest:@[RACObserve(self.direction, name),
                                                                RACObserve(self.direction, surname),
                                                                RACObserve(self.direction, patronymic),
                                                                RACObserve(self.direction, email),
                                                                RACObserve(self.direction, passportSeries),
                                                                RACObserve(self.direction, passportNumber),
                                                                RACObserve(self.direction,  birthday)]
                                                       reduce:^id _Nullable(NSString *name, NSString *surname, NSString *patronymic, NSString *email, NSString *passportSeries, NSString *passportNumber, NSDate *birthdate){
                                                           BOOL nonEmptyCheck = name.length > 0 && surname.length > 0 && patronymic.length > 0 && email.length > 0 && passportSeries.length == 4 && passportNumber.length == 6;
                                                           BOOL emailCheck = [RegistrationStepShortDirectionViewModel validateEmailString:email];
                                                           BOOL dateCheck = [RegistrationStepShortDirectionViewModel validateDate:birthdate];
                                                           return @(nonEmptyCheck && emailCheck && dateCheck);
                                                       }];
    
    return [[RACCommand alloc] initWithEnabled:signalShortDirectionValidation signalBlock:^RACSignal * _Nonnull(id  _Nullable input) {
        #pragma unused(input)
        __strong typeof(weakSelf) blockSelf = weakSelf;
        if (!blockSelf) return [RACSignal empty];
        
        [blockSelf sendDirection];
        
        return [RACSignal empty];
    }];
}

#pragma mark - Utilities

- (void) sendDirection
{
    AccountService *accountService = [self.services accountService];
    DEBUG_ASSERTS_VALID([AccountService class], accountService);
    ShortDirectionImpl *direction = [(id)self.direction coreAsClass:[ShortDirectionImpl class]];

    __weak typeof(self) weakSelf = self;
    
    CORE_START_LOADING(CoreLoadingRainbowRingActivity_Small, @"Идет отправка анкеты", @"Не закрывайте приложение");
    [accountService accountSendShorDirection:direction withCompletionBlock:^(NSError * _Nullable error) {
        __strong typeof(weakSelf) blockSelf = weakSelf;
        
        CORE_STOP_LOADING(CoreLoadingRainbowRingActivity_Small);
       
        if (error || !blockSelf)
        {
            return;
        }
        
        if (self.mainRegViewModel)
        {
            [self.mainRegViewModel stepCompleteOnViewModel:self];
        }
        else
        {
            CoreAction *action = [CoreAction actionFromActionDescription:PUSH.SCORE];
            [blockSelf.services dispatchAction:action];
        }
    }];
}

+ (BOOL)validateEmailString:(NSString *)string
{
    NSString *validString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSPredicate *emailTest=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",validString];
    return [emailTest evaluateWithObject:string];
}

+ (BOOL)validateDate:(NSDate*)checkDate
{
    if (!checkDate) {
        return NO;
    }
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:checkDate];
    
    NSDateComponents *current = [[NSCalendar currentCalendar] components: NSCalendarUnitYear fromDate:[NSDate date]];
    
    
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    NSInteger currentYear = [current year];
    
    if (day >0 && month>0 &&
        year<=currentYear )
    {
        return YES;
    }
    return NO;
}

@end
