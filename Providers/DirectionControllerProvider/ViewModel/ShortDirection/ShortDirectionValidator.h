//
//  ShortDirectionValidator.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 26.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "DirectionValidator.h"

@class ShortDirectionImpl;

@interface ShortDirectionValidator : DirectionValidator

-(BOOL)validate:(ShortDirectionImpl *)model;


@end
