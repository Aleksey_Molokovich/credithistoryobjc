//
//  BaseCollectionViewCell.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 20.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CollectionCellDataModel.h"
#import "CollectionCellConfigModel.h"

@protocol ButtonCollectionViewCellOutput;
@protocol TextFieldCollectionViewOutput;
@protocol AddPhotoCollectionViewCellOutput;
@protocol LabelCollectionViewCellOutput;
@protocol TextViewCollectionViewOutput;

@interface BaseCollectionViewCell : UICollectionViewCell


@property (weak, nonatomic) id delegate;

@property (nonatomic, strong) CollectionCellConfigModel *config;

@property (nonatomic, strong) NSIndexPath *indexPath;

- (void) configureCell:(CollectionCellConfigModel*)model;

- (void) fillWithData:(CollectionCellDataModel*)data;

@end
