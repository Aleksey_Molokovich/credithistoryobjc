//
//  TextFieldCollectionCell.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 20.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCell.h"
#import "UIColor+CoreCustomColors.h"
#import "TextFieldCollectionCellSubTypeBase.h"
#import "TextFieldCollectionCellFactory.h"
#import "TextFieldCollectionViewOutput.h"
#import "TCoreTextFieldView.h"

@interface TextFieldCollectionCell()<TextFieldCollectionCellSubTypeBaseDelegate>

@property (weak, nonatomic) IBOutlet TCoreTextFieldView *textFieldView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImg;

@property (weak, nonatomic) IBOutlet UIView *viewMain;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewError;
@property (strong, nonatomic) TextFieldCollectionCellFactory *factory;
@property (strong, nonatomic) TextFieldCollectionCellSubTypeBase *tfObject;
@end


@implementation TextFieldCollectionCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.textFieldView.delegate = self;
    self.backgroundColor = [UIColor clearColor];
    self.viewMain.clipsToBounds = YES;
    self.viewMain.backgroundColor = [UIColor whiteColor];
    self.factory = [TextFieldCollectionCellFactory new];
    [_textFieldView setState:TCoreTextFieldViewStateNormal];
    self.tfObject = nil;
}

-(void)configureCell:(CollectionCellConfigModel *)model
{
    [super configureCell:model];
    
    self.tfObject = [_factory create:model.subType];
    self.tfObject.key = model.key;
    self.textFieldView.enabled = model.editable;
    
    if (self.tfObject.iconName) {
        self.iconImg.image =[[UIImage imageNamed: self.tfObject.iconName] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
        self.iconImg.tintColor = [UIColor colorWithHex:@"#018DB7" alpha:1];
    }else{
        self.iconImg.image = nil;
    }
    
    if (model.inputAccessoryView)
    {
        _textFieldView.inputAccessoryView = model.inputAccessoryView ;
    }else
    {
        _textFieldView.inputAccessoryView = nil;
    }
    
    if (_tfObject.inputView)
    {
        _textFieldView.textField.inputView = _tfObject.inputView ;
        _tfObject.delegate = self;
    }else{
        _textFieldView.textField.inputView = nil;
    }
    
    _textFieldView.title = model.title;

    _textFieldView.keyboardType = _tfObject.keyboardType;
    _textFieldView.textField.autocorrectionType = UITextAutocorrectionTypeNo;
    

}


-(void)fillWithData:(CollectionCellDataModel *)data
{
    _textFieldView.text = [_tfObject fillText:data.text];
    
    if (data.stateInfo.state == CollectionCellDataModelStateUnvalid) {
        [_textFieldView setState:TCoreTextFieldViewStateFail];
    }
    else
    {
        [_textFieldView setState:TCoreTextFieldViewStateNormal];
    }
    
    if ([_tfObject respondsToSelector:@selector(configureInputViewWithText:)])
    {
        [_tfObject configureInputViewWithText:data.text];
    }
}

#pragma mark TextFieldCollectionCellSubTypeBaseDelegate

-(void)textChangeByAccessoryView:(NSString *)text
{
    self.textFieldView.textField.text = text;
    if ([self.delegate respondsToSelector:@selector(cellTextDidChange:withRowKey:)]) {
        [self.delegate cellTextDidChange:text withRowKey:_tfObject.key];
    }
}

#pragma mark TextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{

    if ([self.delegate respondsToSelector:@selector(cellTextFieldIndexPath:)])
    {
        [self.delegate cellTextFieldIndexPath:self.indexPath];
    }
    
    [_tfObject textFieldDidBeginEditing:textField];
    if ([textField.text isEqualToString:@"0"])
    {
        textField.text = @"";
    }
    
    [_textFieldView setState:TCoreTextFieldViewStateNormal];
    
    if ([self.delegate respondsToSelector:@selector(cellTextFieldDidBeginEditing:withRowKey:)])
    {
        [self.delegate cellTextFieldDidBeginEditing:textField withRowKey:_tfObject.key];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [_tfObject textFieldDidEndEditing:textField];
    [_textFieldView setState:TCoreTextFieldViewStateNormal];
    if ([self.delegate respondsToSelector:@selector(cellTextFieldDidEndEditing:withRowKey:)]) {
        [self.delegate cellTextFieldDidEndEditing:textField withRowKey:_tfObject.key];
    }
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    BOOL flag = [self.tfObject textField:textField shouldChangeCharactersInRange:range replacementString:string];
    
    if ([self.delegate respondsToSelector:@selector(cellTextDidChange:withRowKey:)]) {
        [self.delegate cellTextDidChange:[self.tfObject.formatterByMask deformattedString:textField.text] withRowKey:_tfObject.key];
    }
    return flag;
}

- (CGFloat)height
{
    return kCollectionViewCellHeight;
}
@end
