//
//  TextFieldCollectionViewOutput.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 28.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#ifndef TextFieldCollectionViewOutput_h
#define TextFieldCollectionViewOutput_h
@protocol TextFieldCollectionViewOutput <NSObject>


@optional
- (void)cellTextDidChange:(NSString *)text withRowKey:(NSString *)kRowKey;
- (void)cellTextFieldDidBeginEditing:(UITextField *)textField withRowKey:(NSString *)kRowKey;
- (void)cellTextFieldDidEndEditing:(UITextField *)textField withRowKey:(NSString *)kRowKey;
- (void)cellTextFieldNextAction:(NSDictionary *)rowDict;
- (void)cellTextFieldDoneAction:(NSDictionary *)rowDict;
- (void)cellTextFieldIndexPath:(NSIndexPath*)indexpath;
- (void)cellTextFieldInputAccessorySearchAction;
@end
#endif /* TextFieldCollectionViewOutput_h */
