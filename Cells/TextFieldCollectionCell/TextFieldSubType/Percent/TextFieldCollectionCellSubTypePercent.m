//
//  TextFieldCollectionCellSubTypePercent.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 09.06.2018.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypePercent.h"

@implementation TextFieldCollectionCellSubTypePercent
- (void)configure
{
    self.keyboardType = UIKeyboardTypeDecimalPad;
    self.minLenght = 1;
    self.maxLenght = 5;
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *formattedText = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if ([formattedText intValue]>100) {
        return NO;
    }
    
    return [super textField:textField shouldChangeCharactersInRange:range replacementString:string];
    
}
@end
