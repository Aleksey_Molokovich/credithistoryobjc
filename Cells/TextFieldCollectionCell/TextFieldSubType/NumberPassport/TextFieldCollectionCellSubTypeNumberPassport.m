//
//  TextFieldCollectionCellSubTypeNumberPassport.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 22.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeNumberPassport.h"

@implementation TextFieldCollectionCellSubTypeNumberPassport


- (void)configure
{
    self.keyboardType = UIKeyboardTypeNumberPad;
    self.maxLenght = 6;
    self.minLenght = 6;
    self.mask = @"######";
}

@end
