//
//  TextFieldCollectionCellSubTypeBasePhoneNumber.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 18.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeBasePhoneNumber.h"
#import "Formatter.h"
@implementation TextFieldCollectionCellSubTypeBasePhoneNumber

- (void)configure
{
    self.keyboardType = UIKeyboardTypeNumberPad;
    self.minLenght = 10;
    self.maxLenght = 10;
    
    self.prefix = @"+7 ";
    self.mask = @"(###) ###-##-##";
    
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    textField.text = [self.formatterByMask formattedText:textField.text shouldChangeCharactersInRange:range replacementString:string];

    return NO;
}



@end
