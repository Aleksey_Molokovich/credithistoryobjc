//
//  TextFieldCollectionCellSubTypeSeriasPassport.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 22.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeSeriasPassport.h"

@implementation TextFieldCollectionCellSubTypeSeriasPassport

- (void)configure
{
    self.keyboardType = UIKeyboardTypeNumberPad;
    self.maxLenght = 4;
    self.minLenght = 4;
    self.mask = @"####";
}

@end
