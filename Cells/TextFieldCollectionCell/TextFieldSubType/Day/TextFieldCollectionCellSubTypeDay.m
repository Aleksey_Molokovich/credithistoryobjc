//
//  TextFieldCollectionCellSubTypeDay.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 22.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeDay.h"

@implementation TextFieldCollectionCellSubTypeDay

- (void)configure
{
    self.keyboardType = UIKeyboardTypeNumberPad;
    self.minLenght = 1;
    self.maxLenght = 2;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    return [self validateDayText:textField.text withString:string range:range];
}

- (BOOL) validateDayText:(NSString *)text withString:(NSString *)string range:(NSRange)range
{
    if ([string isEqualToString:@""]) {
        return YES;
    }
    NSString *formattedText = [text stringByReplacingCharactersInRange:range withString:string];
    
    NSInteger value = [formattedText integerValue];
    if (value>31 || (value==0 && [string isEqualToString:@"0"])) {
        return NO;
    }

    return YES;
}

- (NSInteger)value:(NSString *)text atIndex:(NSInteger)index
{
    return [[text substringWithRange:NSMakeRange(index, 1)] integerValue];
}
@end
