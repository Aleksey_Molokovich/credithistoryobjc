//
//  TextFieldCollectionCellSubTypeIndex.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 06.02.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeIndex.h"

@implementation TextFieldCollectionCellSubTypeIndex
- (void)configure
{
    self.keyboardType = UIKeyboardTypeNumberPad;
    self.minLenght = 6;
    self.maxLenght = 6;
    self.mask = @"######";
}
@end
