//
//  TextFieldCollectionCellSubTypeString.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 22.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeString.h"

@implementation TextFieldCollectionCellSubTypeString


-(void)configure
{
    self.keyboardType = UIKeyboardTypeDefault;
    self.minLenght = 3;
}

@end
