//
//  TextFieldCollectionCellSubTypeBase.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 22.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TextFieldCollectionCellDefinition.h"
#import "Formatter.h"


@import UIKit;

@protocol TextFieldCollectionCellSubTypeBaseDelegate<NSObject>
- (void)textChangeByAccessoryView:(NSString*)text;

@end

@interface TextFieldCollectionCellSubTypeBase : NSObject

@property (nonatomic) Formatter *formatterByMask;

@property (assign, nonatomic) NSInteger maxLenght;
@property (assign, nonatomic) NSInteger minLenght;
@property (strong, nonatomic) NSString *placeholder;
@property (strong, nonatomic) NSString *prefix;
@property (strong, nonatomic) NSString *mask;
@property (strong, nonatomic) NSString *key;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSNumberFormatter *formatter;
@property (assign, nonatomic) UIKeyboardType keyboardType;
@property (strong, nonatomic) UIView *inputView;
@property (strong, nonatomic) NSString *iconName;

@property (weak, nonatomic) id<TextFieldCollectionCellSubTypeBaseDelegate> delegate;

- (void)configure;

- (void)configureInputViewWithText:(NSString*)text;

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range
 replacementString:(NSString *)string;

- (void)textFieldDidEndEditing:(UITextField *)textField;
- (void)textFieldDidBeginEditing:(UITextField *)textField;


- (NSString*)fillText:(NSString*)text;
@end
