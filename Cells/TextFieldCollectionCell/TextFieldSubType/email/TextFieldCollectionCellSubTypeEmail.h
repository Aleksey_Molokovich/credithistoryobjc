//
//  TextFieldCollectionCellSubTypeEmail.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 06.02.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeBase.h"

@interface TextFieldCollectionCellSubTypeEmail : TextFieldCollectionCellSubTypeBase

@end
