//
//  TextFieldCollectionCellSubTypeEmail.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 06.02.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeEmail.h"

@implementation TextFieldCollectionCellSubTypeEmail

- (void)configure
{
    self.keyboardType = UIKeyboardTypeEmailAddress;
    
}
@end
