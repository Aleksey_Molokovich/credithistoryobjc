//
//  TextFieldCollectionCellSubTypeNumber.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 22.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeNumber.h"

@implementation TextFieldCollectionCellSubTypeNumber

- (void)configure
{
    self.keyboardType = UIKeyboardTypeDecimalPad;
//    self.maxLenght = 4;
//    self.minLenght = 4;
}

- (NSString *)fillText:(NSString *)text
{
    if ([text isEqualToString:@"0"]) {
        return @"";
    }
    return text;
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    NSString *txt = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if ([txt isEqualToString:@"0"]) {
        textField.text = @"";
        return NO;
    }
    return YES;
}
@end
