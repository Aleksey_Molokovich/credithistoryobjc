//
//  TextFieldCollectionCellSubTypeSNILS.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 22.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeSNILS.h"

@implementation TextFieldCollectionCellSubTypeSNILS

- (void)configure
{
    self.keyboardType = UIKeyboardTypeNumberPad;
    self.minLenght = 14;
    self.maxLenght = 14;
    self.mask = @"###-###-### ##";
}

@end



