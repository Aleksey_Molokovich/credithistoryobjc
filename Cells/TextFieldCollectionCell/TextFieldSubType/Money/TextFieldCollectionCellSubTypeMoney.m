//
//  TextFieldCollectionCellSubTypeMoney.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 22.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeMoney.h"

@implementation TextFieldCollectionCellSubTypeMoney
- (void)configure
{
    self.keyboardType = UIKeyboardTypeNumberPad;
    self.maxLenght = 10;
}


- (NSString *)fillText:(NSString *)text
{
    if ([text isEqualToString:@"0"]) {
        return @"";
    }
    return text;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
//    if (!textField.text.length)
//    {
//        textField.text = @"0";
//    }
}



- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    textField.text = [textField.text stringByReplacingCharactersInRange:range withString:string];
    
    if ([textField.text isEqualToString:@"0"]) {
        textField.text = @"";
    }
    return NO;
}
@end
