//
//  TextFieldCollectionCellSubTypeBase.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 22.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeBase.h"
#import "Formatter.h"

@implementation TextFieldCollectionCellSubTypeBase

-(instancetype)init
{
    if ( (self = [super init]) == nil )
    {
        return nil;
    }
    
    [self configure];
    self.formatterByMask = [[Formatter alloc] initWithPrefix:self.prefix andMask:self.mask];
    return self;
}

- (void)configure
{
    
}

-(NSString *)fillText:(NSString *)text
{
    return text;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.maxLenght > 0 &&
        textField.text.length + 1 > self.maxLenght &&
        range.length != 1)
    {
        return NO;
    }
    
    if (self.mask) {
        NSString *prevText = textField.text;
        NSString *formattedText = [self.formatterByMask formattedText:textField.text shouldChangeCharactersInRange:range replacementString:string];
        
        if ([prevText length] == [formattedText length]) {
            formattedText = [self.formatterByMask formattedString:prevText];
        }
        
        textField.text =formattedText;
        
        
        
        //correct cursor position
        if(![string isEqualToString:@""])
        {
            range.location += [formattedText length] - [prevText length];
        }
        
        if (self.formatterByMask.prefix && range.location<=[self.formatterByMask.prefix length])
        {
            range.location = [self.formatterByMask.prefix length];
        }
        
        UITextPosition *start = [textField positionFromPosition:[textField beginningOfDocument] offset:range.location];
        if (!start) {
            return NO;
        }
        UITextPosition *end = [textField positionFromPosition:start offset:0];
        [textField setSelectedTextRange:[textField textRangeFromPosition:start toPosition:end]];
        
        return NO;
    }
    
    
    return YES;
}


-(void)textFieldDidBeginEditing:(UITextField *)textField
{
#pragma unused (textField)
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
#pragma unused (textField)
}


@end
