//
//  TextFieldCollectionCellSubTypeDate.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 22.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeDate.h"

@interface TextFieldCollectionCellSubTypeDate()

@property (nonatomic,  strong) UIDatePicker *inputView;

@end

@implementation TextFieldCollectionCellSubTypeDate

@synthesize inputView = _inputView;


- (void)configure
{
    self.keyboardType = UIKeyboardTypeNumberPad;
    self.minLenght = 10;
    self.maxLenght = 10;
    self.iconName = @"ic_menu_calendar";
    self.mask = @"##.##.####";
    self.inputView = [UIDatePicker new];
    [self.inputView setDatePickerMode:UIDatePickerModeDate];
    [self.inputView addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];

}

-(void)configureInputViewWithText:(NSString *)text
{
    if (!text)
        return;
        
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    dateFormatter.dateFormat = @"dd.MM.yyyy";
    [dateFormatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
//    dateFormatter.dateStyle = NSDateIntervalFormatterShortStyle;
    self.inputView.date = [dateFormatter dateFromString:text];
}

- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker
{
    if ([self.delegate respondsToSelector:@selector(textChangeByAccessoryView:)]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        dateFormatter.dateFormat = @"dd.MM.yyyy";
        [self.delegate textChangeByAccessoryView:[dateFormatter stringFromDate:datePicker.date]];
    }
}

- (BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    BOOL check = [self validateDateText:textField.text withString:string range:range];


    return check;
}

- (BOOL) validateDateText:(NSString *)text withString:(NSString *)string range:(NSRange)range
{
    if ([string isEqualToString:@""]) {
        return YES;
    }
    NSInteger value = [string integerValue];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *yearString = [formatter stringFromDate:[NSDate date]];
    switch (range.location) {
        case 0:
            if (value > 3) return NO;
            break;
        case 1:
        {
            if ([self value:text atIndex:0] == 3) {
                if (value > 1) return NO;
            }
            if ([self value:text atIndex:0] == 0) {
                if (value < 1) return NO;
            }
        }
            break;
        case 3:
            if (value > 1) return NO;
            break;
        case 4:
        {
            if ([self value:text atIndex:3] == 1) {
                if (value > 2) return NO;
            }
            if ([self value:text atIndex:3] == 0) {
                if (value < 1) return NO;
            }
        }
            break;
        case 6:
        {
            if (value < 1 || value > [self value:yearString atIndex:0]) {
                return NO;
            }
        }
            break;
        case 7:
        {
            if ([self value:text atIndex:6] == [self value:yearString atIndex:0]) {
                if (value > [self value:yearString atIndex:1]) {
                    return NO;
                }
            }
            if ([self value:text atIndex:6] == 1) {
                if (value < 9) {
                    return NO;
                }
            }
        }
            break;
        case 8:
        {
            if ([self isKindOfClass:NSClassFromString(kTextFieldCollectionCellSubTypeUnlimitedDate)]) {
                return YES;
            }
            if ([self value:text atIndex:7] == [self value:yearString atIndex:1]) {
                if (value > [self value:yearString atIndex:2]) {
                    return NO;
                }
            }
        }
            break;
        case 9:
        {
            if ([self isKindOfClass:NSClassFromString(kTextFieldCollectionCellSubTypeUnlimitedDate)]) {
                return YES;
            }
            if ([self value:text atIndex:8] == [self value:yearString atIndex:2]) {
                if (value > [self value:yearString atIndex:3]) {
                    return NO;
                }
            }
        }
            break;
        default:
            return YES;
    }
    return YES;
}

- (NSInteger)value:(NSString *)text atIndex:(NSInteger)index
{
    return [[text substringWithRange:NSMakeRange(index, 1)] integerValue];
}
@end
