//
//  TextFieldCollectionCellSubTypeUnlimitedDate.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 20.12.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeDate.h"

@interface TextFieldCollectionCellSubTypeUnlimitedDate : TextFieldCollectionCellSubTypeDate

@end
