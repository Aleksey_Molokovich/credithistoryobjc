//
//  TextFieldCollectionCellSubTypeUnlimitedDate.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 20.12.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeUnlimitedDate.h"

@interface TextFieldCollectionCellSubTypeUnlimitedDate()

@property (nonatomic,  strong) UIDatePicker *inputView;

@end

@implementation TextFieldCollectionCellSubTypeUnlimitedDate
@synthesize inputView = _inputView;

- (void)configure
{
    self.keyboardType = UIKeyboardTypeNumberPad;
    self.minLenght = 10;
    self.maxLenght = 10;
    self.iconName = @"ic_menu_calendar";
    self.mask = @"##.##.####";
    self.inputView = [UIDatePicker new];
    [self.inputView setDatePickerMode:UIDatePickerModeDate];
    [self.inputView addTarget:self action:@selector(onDatePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    
}

- (void)onDatePickerValueChanged:(UIDatePicker *)datePicker
{
    if ([self.delegate respondsToSelector:@selector(textChangeByAccessoryView:)]) {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
        dateFormatter.dateFormat = @"dd.MM.YYYY";
        [self.delegate textChangeByAccessoryView:[dateFormatter stringFromDate:datePicker.date]];
    }
}
@end
