//
//  TextFieldCollectionCellSubTypeSubvisionPassport.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 27.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeSubvisionPassport.h"

@implementation TextFieldCollectionCellSubTypeSubvisionPassport
- (void)configure
{
    self.keyboardType = UIKeyboardTypeNumberPad;
    self.maxLenght = 7;
    self.minLenght = 7;
    self.mask = @"###-###";
}
@end
