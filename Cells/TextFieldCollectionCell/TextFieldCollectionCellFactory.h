//
//  TextFieldCollectionCellFactory.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 22.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCellSubTypeBase.h"

@interface TextFieldCollectionCellFactory : NSObject

- (TextFieldCollectionCellSubTypeBase *) create:(NSString*)type;

@end
