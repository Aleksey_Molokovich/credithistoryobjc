//
//  TextFieldCollectionCellFactory.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 22.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "TextFieldCollectionCellFactory.h"
#import "TextFieldCollectionCellSubTypeString.h"
#import "TextFieldCollectionCellDefinition.h"
#import "DebugAsserts.h"



@implementation TextFieldCollectionCellFactory


-(TextFieldCollectionCellSubTypeBase *)create:(NSString *)type
{
    id object = [[NSClassFromString(type) alloc] init];
    
    DEBUG_ASSERTS_VALID([TextFieldCollectionCellSubTypeBase class], object);
    
    return object;
}

@end
