//
//  TextFieldCollectionViewDefinition.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 22.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#ifndef TextFieldCollectionViewDefinition_h
#define TextFieldCollectionViewDefinition_h

static NSString *const kTextFieldCollectionCellSubTypeString = @"TextFieldCollectionCellSubTypeString";
static NSString *const kTextFieldCollectionCellSubTypeNumber = @"TextFieldCollectionCellSubTypeNumber";
static NSString *const kTextFieldCollectionCellSubTypeDate = @"TextFieldCollectionCellSubTypeDate";
static NSString *const kTextFieldCollectionCellSubTypeIndex = @"TextFieldCollectionCellSubTypeIndex";

static NSString *const kTextFieldCollectionCellSubTypeUnlimitedDate = @"TextFieldCollectionCellSubTypeUnlimitedDate";
static NSString *const kTextFieldCollectionCellSubTypeSeriasPassport = @"TextFieldCollectionCellSubTypeSeriasPassport";
static NSString *const kTextFieldCollectionCellSubTypeNumberPassport = @"TextFieldCollectionCellSubTypeNumberPassport";
static NSString *const kTextFieldCollectionCellSubTypeSubvisionPassport = @"TextFieldCollectionCellSubTypeSubvisionPassport";
static NSString *const kTextFieldCollectionCellSubTypeMoney = @"TextFieldCollectionCellSubTypeMoney";
static NSString *const kTextFieldCollectionCellSubTypeDay = @"TextFieldCollectionCellSubTypeDay";
static NSString *const kTextFieldCollectionCellSubTypeSNILS = @"TextFieldCollectionCellSubTypeSNILS";
static NSString *const kTextFieldCollectionCellSubTypeEmail = @"TextFieldCollectionCellSubTypeEmail";
static NSString *const kTextFieldCollectionCellSubTypePercent = @"TextFieldCollectionCellSubTypePercent";



#endif /* TextFieldCollectionViewDefinition_h */
