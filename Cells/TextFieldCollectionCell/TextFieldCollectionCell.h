//
//  TextFieldCollectionCell.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 20.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "BaseCollectionViewCell.h"
@interface TextFieldCollectionCell : BaseCollectionViewCell<UITextFieldDelegate>

@end
