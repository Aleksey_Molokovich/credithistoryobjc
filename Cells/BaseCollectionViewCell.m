//
//  BaseCollectionViewCell.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 20.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "BaseCollectionViewCell.h"

@implementation BaseCollectionViewCell


-(void)fillWithData:(CollectionCellDataModel *)data
{
#pragma unused(data)
}

-(void)configureCell:(CollectionCellConfigModel *)model
{
    self.config = model;
}


-(void)applyLayoutAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    [super applyLayoutAttributes:layoutAttributes];
    self.indexPath = layoutAttributes.indexPath;
}

-(UICollectionViewLayoutAttributes *)preferredLayoutAttributesFittingAttributes:(UICollectionViewLayoutAttributes *)layoutAttributes
{
    
    UICollectionViewLayoutAttributes *attributes = [super preferredLayoutAttributesFittingAttributes:layoutAttributes];

    CGSize cellSize;
    CGSize parentSize = self.bounds.size;
    parentSize.width = [UIScreen mainScreen].bounds.size.width;
    
    
    if (self.config.widthType == CollectionCellWidthFull) {
        cellSize = CGSizeMake(parentSize.width - (kCollectionViewInsets.left + kCollectionViewInsets.right), [self height]);
    }
    else if (self.config.widthType == CollectionCellWidthOneThird)
    {
        cellSize = CGSizeMake((int)(parentSize.width / 3 - kCollectionViewInsets.right - kCollectionViewInteritemSpacing/2), [self height]);
    }
    else
    {
        cellSize = CGSizeMake((int)(parentSize.width / 2 - kCollectionViewInsets.right - kCollectionViewInteritemSpacing/2), [self height]);
    }
    
    if (IS_IPHONE_5_OR_4) {
        cellSize = CGSizeMake(parentSize.width - (kCollectionViewInsets.left + kCollectionViewInsets.right), [self height]);
    }

    CGRect frame = attributes.frame;
    frame.size = cellSize;
    attributes.frame = frame;
    

    return attributes;
}

- (CGFloat)height
{
    return kCollectionViewCellHeight;
}

@end
