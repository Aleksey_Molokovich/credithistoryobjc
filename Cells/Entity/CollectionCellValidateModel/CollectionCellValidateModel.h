//
//  ValidateModel.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 04.12.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollectionViewCellsDefinitions.h"

@interface CollectionCellValidateModel : NSObject
@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSString *error;
@property (nonatomic, assign) CollectionCellDataModelState state;

+ (instancetype)initWithKey:(NSString *)key
                      error:(NSString *)error
                      state:(CollectionCellDataModelState)state;
@end
