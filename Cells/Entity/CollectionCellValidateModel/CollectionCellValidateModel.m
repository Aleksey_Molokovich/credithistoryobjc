//
//  ValidateModel.m
//  CreditCalendar
//
//  Created by Алексей Молокович on 04.12.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "CollectionCellValidateModel.h"

@implementation CollectionCellValidateModel

+(instancetype)initWithKey:(NSString *)key error:(NSString *)error state:(CollectionCellDataModelState)state
{
    CollectionCellValidateModel *model = [CollectionCellValidateModel new];
    model.key = key;
    model.error = error;
    model.state = state;
    return model;
}



@end
