//
//  CollectionCellConfigModel.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 15.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollectionViewCellsDefinitions.h"
@interface CollectionCellConfigModel : NSObject
@property (nonatomic, strong) NSString *identifier;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSAttributedString *attributedTitle;
@property (nonatomic, strong) NSString *key;
@property (nonatomic, assign) BOOL isNeedInputAccessoryView;
@property (nonatomic, strong) UIView *inputAccessoryView;
@property (nonatomic, assign) BOOL editable;
@property (nonatomic, assign) CollectionCellWidthType widthType;
@property (nonatomic, strong) NSString *placeholder;
@property (nonatomic, assign) NSInteger cellType;
@property (nonatomic, strong) NSString *subType; // для фабрики TF
@end
