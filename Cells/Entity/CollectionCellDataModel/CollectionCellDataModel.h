//
//  CellCreditEditingModel.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 17.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CollectionViewCellsDefinitions.h"
#import "CollectionCellValidateModel.h"

@interface CollectionCellDataModel : NSObject

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) CollectionCellValidateModel *stateInfo;
@property (nonatomic, strong) id customData;
@end
