//
//  CollectionCellHeaderModel.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 22.11.17.
//  Copyright © 2017 Alef. All rights reserved.
//

#import "CellLayout.h"
@class CollectionCellConfigModel;
@interface CollectionCellHeaderModel : CellLayout
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSArray<CollectionCellConfigModel*> *cells;

@end
