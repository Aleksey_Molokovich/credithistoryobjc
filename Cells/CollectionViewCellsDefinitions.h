//
//  CollectionViewCellsDefinitions.h
//  CreditCalendar
//
//  Created by Алексей Молокович on 10.01.18.
//  Copyright © 2018 Alef. All rights reserved.
//

#ifndef CollectionViewCellsDefinitions_h
#define CollectionViewCellsDefinitions_h

#define IS_IPHONE_5_OR_4 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )


@import Foundation;
@import UIKit;
typedef NS_ENUM(NSInteger, CollectionCellWidthType)
{
    CollectionCellWidthFull,
    CollectionCellWidthHalf,
    CollectionCellWidthOneThird
};

typedef NS_ENUM (NSInteger,CollectionCellDataModelState)
{
    CollectionCellDataModelStateNormal,
    CollectionCellDataModelStateValid,
    CollectionCellDataModelStateUnvalid,
    CollectionCellDataModelStateWarning
};

static UIEdgeInsets const kCollectionViewInsets = {10.0f, 20.0f, 0.0f, 20.0f};
static CGFloat const kCollectionViewMinimumLineSpacing = 10.0f;
static CGFloat const kCollectionViewInteritemSpacing = 10.0f;
static CGFloat const kCollectionViewCellHeight = 70.0f;
static CGFloat const kCollectionViewSectionHeight = 40.0f;

static NSString *const kCollectionViewTextFieldCell = @"TextFieldCollectionCell";
static NSString *const kTextViewCollectionViewCell = @"TextViewCollectionViewCell";
static NSString *const kCollectionViewSelectableCell = @"SelectableCollectionViewCell";
static NSString *const kCollectionHeaderView = @"CollectionHeaderView";
static NSString *const kCollectionViewWarningCell = @"WarningMessageCollectionViewCell";
static NSString *const kAddPhotoCollectionViewCell = @"AddPhotoCollectionViewCell";
static NSString *const kBlueButtonCollectionViewCell = @"BlueButtonCollectionViewCell";
static NSString *const kLabelCollectionViewCell = @"LabelCollectionViewCell";
static NSString *const kCommentCollectionViewCell = @"CommentCollectionViewCell";


#endif /* CollectionViewCellsDefinitions_h */
